//
//  MNZip.h
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/04/05.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface MNZip : NSObject {

	
	
}

+(NSData*)zip:(id)raw; //raw is nsdata or nsfilewrapper

+(NSFileWrapper*)unzip:(NSData*)zipData;


@end
