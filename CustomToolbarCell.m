//
//  StyleToolbarCell.m
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/04/07.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "CustomToolbarCell.h"


@implementation CustomToolbarCell


- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView
{
	
	
	NSImage* image = [[[self image] copy] autorelease];




	float yohaku = (cellFrame.size.width - cellFrame.size.height)/2;
	NSRect destRect = NSMakeRect( yohaku ,0,cellFrame.size.height, cellFrame.size.height);

	NSRect imageRect = NSMakeRect(0,0,[image size].width, [image size].height);
	
	[image drawInRect:destRect fromRect:imageRect operation:NSCompositeSourceOver fraction:1.0];
	
	
	if([self isHighlighted])
	{
		[image drawInRect:destRect fromRect:imageRect operation:NSCompositeXOR fraction:0.2];
		
	}
	
	
	////NSLog(@"drawing toolbar counter %@",[self title]);

	// draw counter
	int counter = [[self title] intValue];
	if( counter  !=  0 )
	{

		
		NSImage* counterImage = [self counterImage:counter];
		
		NSSize counterSize = [counterImage size];
		NSPoint drawPoint = NSMakePoint( cellFrame.size.width - counterSize.width,
										 ( cellFrame.size.height - counterSize.height) /2.5);
		
		[counterImage drawAtPoint:drawPoint
		fromRect:NSMakeRect(0,0,counterSize.width,counterSize.height)
						operation:NSCompositeSourceOver fraction:1.0];
		
	}
		
}


-(NSImage*)counterImage:(int)counter
{
	NSDictionary* dict = [NSDictionary dictionaryWithObjectsAndKeys:
		[NSFont boldSystemFontOfSize: 9.0], NSFontAttributeName,
		[NSColor whiteColor], NSForegroundColorAttributeName,
		NULL];
	
	NSString* countPartStr = [NSString stringWithFormat:@"%d",counter];
	
	
	NSAttributedString* countPart = [[[NSAttributedString alloc] initWithString:countPartStr attributes:dict] autorelease];
	
	

	


	
	
	// caliculate attributed string centered in area
	NSRect counterStringRect = NSZeroRect;
	NSRect frame = NSZeroRect;
	
	
	counterStringRect.size = [countPart size];
	
	
	float COUNTER_RADIUS = (counterStringRect.size.height  ) /2;

	

	frame.size.height = counterStringRect.size.height ;
	frame.size.width = counterStringRect.size.width + COUNTER_RADIUS * 2;

	counterStringRect.origin.x = COUNTER_RADIUS;
	counterStringRect.origin.y = 1;
	
	
	NSImage* returnImage = [[[NSImage alloc] initWithSize:frame.size] autorelease];
	
	[returnImage lockFocus];
	// draw background -- rounded rectangle
	
	
	[[NSColor colorWithCalibratedWhite:0.4 alpha:0.90] set];
	
	NSBezierPath *path = [NSBezierPath bezierPath];
	
	[path moveToPoint:NSMakePoint( COUNTER_RADIUS, 0)];
	[path lineToPoint:NSMakePoint( frame.size.width - COUNTER_RADIUS, 0)];
	[path appendBezierPathWithArcWithCenter:NSMakePoint( frame.size.width - COUNTER_RADIUS, COUNTER_RADIUS) radius:COUNTER_RADIUS startAngle:270.0 endAngle:90.0];
	[path lineToPoint:NSMakePoint(  COUNTER_RADIUS,  frame.size.height)];
	[path appendBezierPathWithArcWithCenter:NSMakePoint(  COUNTER_RADIUS,   COUNTER_RADIUS) radius:COUNTER_RADIUS startAngle:90.0 endAngle:270.0];
	[path fill];
	
	
	
	[countPart drawAtPoint:counterStringRect.origin];
	[returnImage unlockFocus];

	return 	returnImage;

}

@end
