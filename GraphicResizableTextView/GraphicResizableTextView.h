/* GraphicResizableTextView */


/*
 バージョン２
 画像をトリムする
 
 
 */

#import <Cocoa/Cocoa.h>
#import "LinkTextView.h"
#import "AppController.h"


//#import "NSClipView(extention).h"


/*
typedef enum {
	NoKnob = 0,
	CornerKnob,
	RightKnob,
	BottomKnob
		
} DRAGMODE;

*/
@interface GraphicResizableTextView : LinkTextView
{
	//NSTextAttachment*	selectedImage;
	//NSRect				selectedImageRect;
	//unsigned			selectedImageCharIndex;
	
	//NSCursor* reszeArrowCorner;
	//NSCursor* reszeArrowRight;
	//NSCursor* reszeArrowBottom;
	
	
	NSImage* _draggingImage;

}

- (id)initWithFrame:(NSRect)frame;

-(void)awakeFromNib;

-(void)dealloc;

- (void)mouseDown:(NSEvent *)theEvent;

- (NSMenu *)menuForEvent:(NSEvent *)theEvent;

-(void)menu_selected:(id)sender;

- (void)drawRect:(NSRect)aRect;

-(void)scaleImagesInSelectedRangeBy:(float)scaleFactor; //degree must be 90 or -90

-(void)refreshSelectedRange;

-(void)rotateImagesInSelectedRangeBy90Degrees;

-(void)convertAlltextAttachments;

-(void)convertAttachmentAt:(unsigned)location;

-(void)fitGraphics;

-(void)fitGraphicsInSelectedRange;

-(void)fitGraphicsInRange:(NSRange)selRange;

-(void)trimGraphics;

-(void)trimGraphicsInSelectedRange;

-(void)trimGraphicsInRange:(NSRange)selRange;

-(void)maximizeGraphics;

-(void)maximizeGraphicsInSelectedRange;


-(void)maximizeGraphicsInRange:(NSRange)selRange;



@end
