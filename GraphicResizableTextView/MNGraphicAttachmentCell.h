//
//  NSTextAttachmentCell(GraphicExtention).h
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/05/01.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

/*
 NSString* title = [NSString stringWithFormat:@"%f, %f",scale,angle];
 
 
画像をトリムする
 
 
 */

#import <Cocoa/Cocoa.h>


@interface MNGraphicAttachmentCell : NSTextAttachmentCell
{
	
}
- (NSSize)cellSizeWithoutTrim;

-(NSSize)originalSize;

-(float)zoomScale;
-(float)angle;
-(NSRect)trimRect;
-(void)setTrimRect:(NSRect)rect;

-(void)setScale:(float)scale andAngle:(float)angle;

@end
