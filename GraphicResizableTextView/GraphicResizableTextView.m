#import "GraphicResizableTextView.h"
#import "NSTextView (coordinate extension).h"
#import "GraphicInspector.h"
#import "MNGraphicAttachmentCell.h"


#define TIMEOUT_WHILE(n) NSDate* __timeOutDate = [NSDate dateWithTimeIntervalSinceNow: n ];	while ( [__timeOutDate compare:[NSDate date]] == NSOrderedDescending )

@implementation GraphicResizableTextView

#define SKT_HANDLE_WIDTH 8
#define SKT_HALF_HANDLE_WIDTH 4


#define MINIMUM_RESIZABLE_SIZE 0


- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		
		[self awakeFromNib];
    }
    return self;
}


-(void)awakeFromNib
{
	/*
	[selectedImage release];
	selectedImage = NULL;
	
	
	NSBundle* myBundle;
	myBundle =[NSBundle bundleForClass:[self class]];
	NSString* path;
	
	path = [myBundle pathForResource:@"resizeArrowCorner" ofType:@"tif"];
	NSImage* reszeArrowCornerImage = [[[NSImage alloc] initWithContentsOfFile:path ] autorelease];
	reszeArrowCorner = [[NSCursor alloc] initWithImage:reszeArrowCornerImage hotSpot:NSMakePoint(8.0,8.0)];
	
	path = [myBundle pathForResource:@"resizeArrowBottom" ofType:@"tif"];
	NSImage* reszeArrowBottomImage = [[[NSImage alloc] initWithContentsOfFile:path ] autorelease];
	reszeArrowBottom = [[NSCursor alloc] initWithImage:reszeArrowBottomImage hotSpot:NSMakePoint(8.0,8.0)];
	
	path = [myBundle pathForResource:@"resizeArrowRight" ofType:@"tif"];
	NSImage* reszeArrowRightImage = [[[NSImage alloc] initWithContentsOfFile:path ] autorelease];
	reszeArrowRight = [[NSCursor alloc] initWithImage:reszeArrowRightImage hotSpot:NSMakePoint(8.0,8.0)];
	
	*/

	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}

	
}

-(void)dealloc
{
	//[reszeArrowCorner release];
	//[reszeArrowBottom release];
	//[reszeArrowRight release];
	
	
	[super dealloc];
}



/*
- (void)keyDown:(NSEvent *)theEvent
{
	
	////NSLog(@"keydown");
	if( selectedImage != NULL )
	{
		//NSLog(@"HERE");
		[selectedImage release];
		selectedImage = NULL;
		selectedImageRect = NSZeroRect;
		[[self enclosingScrollView] display];
		
		//reset cursor
		[self setDefaultCursorRect];
	}
	[super keyDown:theEvent];
}*/

- (void)mouseDown:(NSEvent *)theEvent
{

	
	// check if clicked on an image
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	unsigned charIndex = [self charIndexOn:mouseLoc];
	
	
	id something = NULL;
	if( charIndex != NSNotFound )
		something = [[self textStorage] attribute:NSAttachmentAttributeName
										  atIndex:charIndex effectiveRange:NULL];
	
	
	
	if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"]|| 
		[ [[something attachmentCell] className] isEqualToString:@"MNGraphicAttachmentCell"]) /// select image
	{
		//convert tiff to cell
		if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"] )
			
		{
			
			MNGraphicAttachmentCell* newCell = 
			[[[MNGraphicAttachmentCell alloc] initImageCell:
				[(MNGraphicAttachmentCell*)[something attachmentCell] image] ] autorelease];
			[something setAttachmentCell:newCell]; //convert
		}
		
		
		
		
		//check size  is over MINIMUM_RESIZABLE_SIZE
		
		NSSize size = [[something attachmentCell] cellSize];
		
		if( ( size.width > MINIMUM_RESIZABLE_SIZE ) && ( size.height > MINIMUM_RESIZABLE_SIZE ) )
		{
			
			
			[self setSelectedRange:NSMakeRange( charIndex, 1)];
			
			
			BOOL startDragging = NO;
			TIMEOUT_WHILE(30)
			{
				theEvent = [[self window] 
				nextEventMatchingMask:NSAnyEventMask
							untilDate:[NSDate dateWithTimeIntervalSinceNow:30.0]
							   inMode:NSEventTrackingRunLoopMode
							  dequeue:YES];
				
				if( theEvent == nil )
				{
					break;
				}
				
				if ([theEvent type] == NSLeftMouseUp  )
				{		
					break;
				}
				
				
				if( [theEvent type] == NSLeftMouseDragged  )
				{
					startDragging = YES;
					break;
					
				}
			}
			
			if( startDragging == YES )
			{
				NSRect contentRect;
				NSPoint mLoc = [[self window] mouseLocationOutsideOfEventStream];
				
				contentRect.origin = [[[self window] contentView] convertPoint:mLoc toView:self ];
				
				NSPasteboard* pb = [NSPasteboard pasteboardWithName:NSDragPboard];
				[pb declareTypes:[NSArray arrayWithObject:NSTIFFPboardType]  owner:self];
				
				[pb setData:[[[something attachmentCell] image] TIFFRepresentation]
					forType:NSTIFFPboardType];
				
				
				
				
				[_draggingImage release];
				_draggingImage = [[something attachmentCell] image];
				NSImage* dragimage = [[[NSImage alloc] initWithSize: [_draggingImage size]] autorelease];
				NSRect rect = NSZeroRect;
				rect.size = [_draggingImage size];
				
				[dragimage lockFocus];
				NSGraphicsContext *context = [NSGraphicsContext currentContext];
				[context saveGraphicsState];
				
				
				NSAffineTransform* affine = [NSAffineTransform transform];
				
				
				
				// move axis
				//	[affine scaleXBy:1.0 yBy:-1.0];
				
				
				//	[affine concat];	
				
				
				
				
				[dragimage setFlipped:NO];
				
				[_draggingImage drawAtPoint:NSMakePoint(0,0)
								   fromRect:rect
								  operation:NSCompositeSourceOver
								   fraction:0.5];
				//	[context restoreGraphicsState];
				
				[dragimage setFlipped:YES];
				
				[dragimage unlockFocus];
				
				
				//NSLog(@"start dragging");
				
				
				
				pb = [NSPasteboard pasteboardWithName:NSDragPboard];
				
				[pb setData:[_draggingImage TIFFRepresentation] forType:NSTIFFPboardType];
				[pb setPropertyList:[[[NSArray arrayWithObject:@"tiff" ] description] propertyList]
							forType:NSFilesPromisePboardType];
				
				
				[_draggingImage retain];
				
				
				

				
				
				[self dragImage:dragimage
							 at:contentRect.origin
						 offset:NSMakeSize(0,0)
						  event:theEvent
					 pasteboard:pb
						 source:self
					  slideBack:YES];
				/*
				 
				 NSPoint fromPoint = [self convertPoint:mouseLoc fromView:[[self window] contentView]];
				 
				 NSRect fromRect = {0,0,32,32};
				 fromRect.origin = fromPoint;
				 
				 [self  dragPromisedFilesOfTypes:[NSArray arrayWithObject:@"tiff" ] 
										fromRect:fromRect
										  source: self 
									   slideBack:YES
										   event: theEvent];
				 
				 */
				[super mouseDown:nil];
				return;
			}else //clcik
			{
				
				[[something attachmentCell] trackMouse:(NSEvent *)theEvent
												inRect:[self rectForCharRange:NSMakeRange(charIndex,1)]
												ofView:self
									  atCharacterIndex: charIndex 
										  untilMouseUp:NO];
				
			}
			
		}
		
		[super mouseDown:nil];
		return;	
	}
	
	[super mouseDown:theEvent];
	
}

- (NSArray *)namesOfPromisedFilesDroppedAtDestination:(NSURL *)dropDestination
{
	//NSLog(@"namesOfPromisedFilesDroppedAtDestination %@",[dropDestination path]);
	
	if( _draggingImage != nil )
	{
		NSString* path = [dropDestination path];
		
		NSString* fullPath = [path stringByAppendingPathComponent:@"EdgiesImage.tiff"];
		
		fullPath = [fullPath uniquePathForFolder];
		
		
		[[_draggingImage TIFFRepresentation] writeToFile:fullPath atomically:YES];
		
		[_draggingImage release];
		_draggingImage = nil;
		
		return [NSArray arrayWithObject:[fullPath lastPathComponent]];
	}
	
	
	return [NSArray array];
}



- (NSMenu *)menuForEvent:(NSEvent *)theEvent
{
	
	
	NSMenuItem* customMenuItem;
	NSMenu* aContextMenu = [super menuForEvent:theEvent];
	
	
	
	/* remove MN item */
	int piyo;
	for( piyo = [aContextMenu numberOfItems]-1 ; piyo >= 0; piyo -- )
	{
		customMenuItem = [aContextMenu itemAtIndex:piyo];
		if( [[customMenuItem representedObject] isKindOfClass:[NSString class]] )
			if( [[customMenuItem representedObject] isEqualToString:@"MN_GraphicResizableTextView"] )
			{
				[aContextMenu removeItem:customMenuItem];
			}
				
	}
		
		
		
		
		
		NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
		unsigned charIndex = [self charIndexOn:mouseLoc];
		
		
		id something = NULL;
		if( charIndex != NSNotFound )
		{
			something = [[self textStorage] attribute:NSAttachmentAttributeName
											  atIndex:charIndex effectiveRange:NULL];
			
		}
		
		
		
		if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"]|| 
			[ [[something attachmentCell] className] isEqualToString:@"MNGraphicAttachmentCell"]) /// select image
		{
			[self setSelectedRange:NSMakeRange(charIndex,1) ];
			
			
			//add separator
			customMenuItem = [NSMenuItem separatorItem];
			[customMenuItem setRepresentedObject:@"MN_GraphicResizableTextView"];
			[aContextMenu addItem:customMenuItem ];
			
			
			
			
			NSMenu* submenu = [[NSMenu alloc] init];
			//
			/*
			 customMenuItem = [[NSMenuItem alloc] initWithTitle:@"90 degree clockwise"																   action:@selector(menu_selected:)
												  keyEquivalent:@""];
			 [customMenuItem setTag:1];
			 [customMenuItem setTarget:self];
			 [submenu addItem:[customMenuItem autorelease]];
			 
			 //
			 
			 customMenuItem = [[NSMenuItem alloc] initWithTitle:@"90 degree anticlockwise"																   action:@selector(menu_selected:)
												  keyEquivalent:@""];
			 [customMenuItem setTag:2];
			 [customMenuItem setTarget:self];
			 [submenu addItem:[customMenuItem autorelease]];
			 */
			
			//
			/*
			 customMenuItem = [[NSMenuItem alloc] initWithTitle:@"Smaller"																   action:@selector(menu_selected:)
												  keyEquivalent:@""];
			 [customMenuItem setTag:3];
			 [customMenuItem setTarget:self];
			 [submenu addItem:[customMenuItem autorelease]];
			 
			 //
			 customMenuItem = [[NSMenuItem alloc] initWithTitle:@"Bigger"																   action:@selector(menu_selected:)
												  keyEquivalent:@""];
			 [customMenuItem setTag:4];
			 [customMenuItem setTarget:self];
			 [submenu addItem:[customMenuItem autorelease]];
			 */
			//
			customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Open Graphic Inspector",@"")																   action:@selector(menu_selected:)
												 keyEquivalent:@""];
			[customMenuItem setTag:5];
			[customMenuItem setTarget:self];
			[submenu addItem:[customMenuItem autorelease]];
			
			//
			customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Fit to Window",@"")																   action:@selector(menu_selected:)
												 keyEquivalent:@""];
			[customMenuItem setTag:6];
			[customMenuItem setTarget:self];
			[submenu addItem:[customMenuItem autorelease]];
			
			
			//
			customMenuItem = [[NSMenuItem alloc] initWithTitle:NSLocalizedString(@"Picture",@"")
														action:@selector(menu_selected:) keyEquivalent:@""];
			[customMenuItem setRepresentedObject:@"MN_GraphicResizableTextView"];
			[aContextMenu addItem:customMenuItem ];
			[aContextMenu setSubmenu:[submenu autorelease] forItem:customMenuItem];
			[customMenuItem autorelease];	
			
			
		}
		
		return aContextMenu;
}

-(void)menu_selected:(id)sender
{
	//NSLog(@"menu selecteed in grtv");
	if( [sender tag] == 5 )
	{
		[APP_graphicInspectorPanel() makeKeyAndOrderFront:self];
		
		
	}else if( [sender tag] == 6 )
	{
		
		[self fitGraphicsToWindowWidthInRange:[self selectedRange]];
	}
	
}

- (void)drawRect:(NSRect)aRect
{	
	[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
	
	[super drawRect:aRect ];
}
/*
- (void)drawRect:(NSRect)aRect
{	
	[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
		
	[super drawRect:aRect ];
	
	

	
	if( selectedImage != NULL )
	{
		
		//NSLog(@"Class %@",NSStringFromClass( [selectedImage class]) );
		
		
		unsigned selectedImageLocation;
		BOOL flag = NO;
		
		NSRange range; 
		for( selectedImageLocation = 0; selectedImageLocation < [[self textStorage] length]; ) //$$$$ can be faster
		{
			if( [[self textStorage] attribute:NSAttachmentAttributeName 
				atIndex:selectedImageLocation
						longestEffectiveRange:&range inRange:[self fullRange]] == selectedImage )
			{
				flag = YES;
				break;
			}
			
			selectedImageLocation = NSMaxRange(range); 
		}
			
		selectedImageCharIndex = selectedImageLocation;
		
		if( flag == YES ) // image IS selected
		{
			NSRect rect = [self rectForCharRange:NSMakeRange(selectedImageLocation,1)]; //need to be modify
			rect.size = [[[selectedImage attachmentCell] image] size];
	
			rect.origin.y += [ [self layoutManager] 
				lineFragmentRectForGlyphAtIndex:[self glyphIndexForCharIndex:selectedImageLocation]																	effectiveRange:NULL].size.height - rect.size.height;
			
			[self drawHandlesForRect:rect];
		}
	}
}
*/
/*
- (void)drawHandleAtPoint:(NSPoint)point inColor:(NSColor*)color fill:(BOOL)flag
{
    NSRect handleRect;
	
    handleRect.origin.x = point.x - SKT_HALF_HANDLE_WIDTH + 1.0;
    handleRect.origin.y = point.y - SKT_HALF_HANDLE_WIDTH + 1.0;
    handleRect.size.width = SKT_HANDLE_WIDTH - 1.0;
    handleRect.size.height = SKT_HANDLE_WIDTH - 1.0;

	
	[NSBezierPath setDefaultLineWidth:1.0];
	if( flag == NO )
	{
		[[NSColor whiteColor] set];
		[NSBezierPath fillRect:handleRect];	
	}else
	{
	[color set];
		[NSBezierPath fillRect:handleRect];	
		
	}
	[color set];
	[NSBezierPath strokeRect:handleRect];


}

- (void)drawHandlesForRect:(NSRect)rect
{
	selectedImageRect = rect;
	
	
	[[NSColor lightGrayColor] set];
	[NSBezierPath setDefaultLineWidth:2.0];
	[NSBezierPath strokeRect:rect];
	
	
	[self drawHandleAtPoint:rect.origin
					inColor:[NSColor lightGrayColor] fill:YES];
	[self drawHandleAtPoint:NSMakePoint( rect.origin.x + rect.size.width/2, rect.origin.y)
											inColor:[NSColor lightGrayColor] fill:YES];

	[self drawHandleAtPoint:NSMakePoint( rect.origin.x, rect.origin.y +  rect.size.height/2 )
					inColor:[NSColor lightGrayColor] fill:YES];

	[self drawHandleAtPoint:NSMakePoint( rect.origin.x + rect.size.width, rect.origin.y)
					inColor:[NSColor lightGrayColor]  fill:YES];
	
	[self drawHandleAtPoint:NSMakePoint( rect.origin.x, rect.origin.y +  rect.size.height )
					inColor:[NSColor lightGrayColor] fill:YES];
//
	
	
	[self drawHandleAtPoint:NSMakePoint(NSMaxX(rect), NSMaxY(rect))
					inColor:[NSColor blackColor] fill:NO];
	[self drawHandleAtPoint:NSMakePoint( rect.origin.x + rect.size.width/2, NSMaxY(rect))
					inColor:[NSColor blackColor]  fill:NO];
	
	[self drawHandleAtPoint:NSMakePoint(NSMaxX(rect), rect.origin.y +  rect.size.height/2 )
					inColor:[NSColor blackColor]  fill:NO];
	
	[self setDefaultCursorRect];
	[self resetCursorRects];

}


-(DRAGMODE)cursorOnKnob
{
	if( selectedImage == NULL ) return;
	
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];
	
	mouseLoc = [self convertPoint:mouseLoc fromView:[[self window] contentView]];
	
	NSRect rect = selectedImageRect;
	
	
	
	if( NSPointInRect(mouseLoc, NSMakeRect(NSMaxX(rect) - SKT_HANDLE_WIDTH,
										   NSMaxY(rect) - SKT_HANDLE_WIDTH,
										   SKT_HANDLE_WIDTH*2,
										   SKT_HANDLE_WIDTH*2  ) ) )
		return CornerKnob;
	
	if( NSPointInRect(mouseLoc, NSMakeRect(rect.origin.x + rect.size.width/2 - SKT_HANDLE_WIDTH,
										   NSMaxY(rect) - SKT_HANDLE_WIDTH,
										   SKT_HANDLE_WIDTH*2,
										   SKT_HANDLE_WIDTH*2  ) ) )
		return BottomKnob;
	
	if( NSPointInRect(mouseLoc, NSMakeRect(NSMaxX(rect) - SKT_HANDLE_WIDTH,
										   rect.origin.y +  rect.size.height/2 - SKT_HANDLE_WIDTH,
										   SKT_HANDLE_WIDTH *2,
										   SKT_HANDLE_WIDTH *2) ) )
		return  RightKnob;
	
	return NoKnob;
	
	
}

-(NSRect)selectedImageRect
{
	return selectedImageRect;
}*/
/*
 
-(void)trackMouse:(DRAGMODE)mode
{
	NSEvent* theEvent;
	
	NSPoint originalMouseLoc = [[self window] mouseLocationOutsideOfEventStream];
	
	NSRect originalRect = selectedImageRect;
	
	//NSRect clipBounds = [[self superview] bounds];//NSLog(@"bounds %@",NSStringFromRect(clipBounds));
	
	while (1)
	{
        theEvent = [[self window] 
            nextEventMatchingMask:
			(NSLeftMouseDraggedMask | NSLeftMouseUpMask)];

		NSPoint currentMouseLoc = [[self window] mouseLocationOutsideOfEventStream];
		
		
		int difference_width = currentMouseLoc.x - originalMouseLoc.x;
		int difference_height = - currentMouseLoc.y + originalMouseLoc.y;
	
		
		
	
		//calc new size
		NSSize newSize;
		
		if( mode == CornerKnob )
		{
			float factor;
			 if( (difference_width * difference_width) < (difference_height * difference_height) )
				 factor =  difference_height /  originalRect.size.height + 1.0;
			 else 
				 factor =  difference_width / originalRect.size.width + 1.0;
			
			 
			 
			newSize = NSMakeSize( originalRect.size.width * factor , 
									 originalRect.size.height * factor );
		
		}else if( mode == RightKnob )
		{
			newSize = NSMakeSize( originalRect.size.width + difference_width, 
								  originalRect.size.height );
	
			
		}else if( mode == BottomKnob )
		{
			newSize = NSMakeSize( originalRect.size.width , 
								  originalRect.size.height + difference_height);
			
			
		}
		
		
		////////////// Resize image. ////////////////////
		
		//limit size
		
		if( newSize.width <=  MINIMUM_RESIZABLE_SIZE )
			newSize.width = MINIMUM_RESIZABLE_SIZE +1;
		if( newSize.height <=  MINIMUM_RESIZABLE_SIZE )
			newSize.height = MINIMUM_RESIZABLE_SIZE +1;
	
			
		//
		[self scaleImageTo:(NSSize)newSize];
		
		
		if ([theEvent type] == NSLeftMouseUp)
		{		
		//NSLog(@"draggingFinished");
			
			// set cursor
			
						
			[self setDefaultCursorRect];
			[self resetCursorRects];


			[[NSCursor arrowCursor] set];

            break;
		}
			
	}
	
}*/
/*
-(void)scaleImageTo:(NSSize)newSize
{
	
	NSImage* image = [[selectedImage attachmentCell] image];
	[image setScalesWhenResized:YES];
	
	NSBitmapImageRep* imageRep = [[image representations] objectAtIndex:0];
	
	NSSize size = [imageRep size];
	[imageRep setSize:newSize];
	
	[image setSize:newSize];

	
	
	//NSImage* newImage = [[[NSImage alloc] initWithData:[imageRep TIFFRepresentation]] autorelease];
	//[newImage setSize:newSize];
	//[newImage addRepresentation:imageRep];
	
	
	///write
	
	[[selectedImage attachmentCell] setImage:image];
	
	
	
	
	
	////////////////////////
	
	
	// refresh
	[[self textStorage] addAttribute:NSAttachmentAttributeName value:selectedImage range:NSMakeRange(selectedImageCharIndex,1)];
	[[self textStorage] addAttribute:MNGraphicSizeAttributeName value:NSStringFromSize(newSize) range:NSMakeRange(selectedImageCharIndex,1)];
	
	
	//[[self superview] scrollToPoint:clipBounds.origin];
	//[[self enclosingScrollView] reflectScrolledClipView:[self superview]];
	
	
	[[self enclosingScrollView] display];	
	
}*/

/*
- (void)setSelectedRange:(NSRange)newSelectedCharRange
{

	
	NSRange oldSelectedCharRange = [self selectedRange];
	NSTextStorage* ts = [self textStorage];
	
	
	if( [ts length] == 0 || ( oldSelectedCharRange.length == 0 && newSelectedCharRange.length == 0 ) )
	{
		[super setSelectedRange:newSelectedCharRange];
		return;
	}

	
	NSAttributedString* attr_old = [ts attributedSubstringFromRange: oldSelectedCharRange];
	NSAttributedString* attr_new = [ts attributedSubstringFromRange: newSelectedCharRange];

	if( [attr_old containsAttachments] )
	{
		unsigned hoge;
		for( hoge = oldSelectedCharRange.location; hoge  < NSMaxRange(oldSelectedCharRange); hoge++ )
		{
			id attachment = [ts attribute:NSAttachmentAttributeName atIndex:hoge effectiveRange:NULL];
			
			if( [[attachment attachmentCell] isKindOfClass:[MNGraphicAttachmentCell class]] )
			{
				[ [attachment attachmentCell]  setHighlighted:NO ];	
			//NSLog(@"no highlight");
				[ts addAttribute:NSAttachmentAttributeName value:attachment range:NSMakeRange(hoge,1)];

			}
		}
		
	}
	
	if( [attr_new containsAttachments] )
	{
		unsigned hoge;
		for( hoge = newSelectedCharRange.location; hoge  < NSMaxRange(newSelectedCharRange); hoge++ )
		{
			id attachment = [ts attribute:NSAttachmentAttributeName atIndex:hoge effectiveRange:NULL];
			
			if( [[attachment attachmentCell] isKindOfClass:[MNGraphicAttachmentCell class]] )
			{
				[ [attachment attachmentCell]  setHighlighted:YES ];	
			//NSLog(@"highlight");
				[ts addAttribute:NSAttachmentAttributeName value:attachment range:NSMakeRange(hoge,1)];

			}
		}		
		
	}

	
	
	[super setSelectedRange:newSelectedCharRange];
}
*/

-(void)scaleImagesInSelectedRangeBy:(float)scaleFactor //degree must be 90 or -90
{
	
	NSRange sRange = [self selectedRange];
	
	unsigned hoge;
	for( hoge = sRange.location; hoge  < NSMaxRange(sRange); hoge++ )
	{
		
		id something = NULL;
		something = [[self textStorage] attribute:NSAttachmentAttributeName
										  atIndex:hoge effectiveRange:NULL];
		
		if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"]|| 
			[ [[something attachmentCell] className] isEqualToString:@"MNGraphicAttachmentCell"]) /// select image
		{
			//NSLog(@"class %@",[[something attachmentCell] className]);
			if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"] )
			{
				MNGraphicAttachmentCell* newCell = 
				[[[MNGraphicAttachmentCell alloc] initImageCell:[(MNGraphicAttachmentCell*)[something attachmentCell] image] ] autorelease];
				[something setAttachmentCell:newCell];
			}
			
			[(MNGraphicAttachmentCell*) [something attachmentCell] setScale:scaleFactor andAngle:[(MNGraphicAttachmentCell*) [something attachmentCell] angle]];
			
			// refresh
			[[self textStorage] addAttribute:NSAttachmentAttributeName value:something range:NSMakeRange(hoge, 1)];
			//[[self textStorage] addAttribute:MNGraphicSizeAttributeName value:NSStringFromSize(newSize) range:NSMakeRange(hoge, 1)];
			
			
			
			
		}
	}
	
	
	[[self enclosingScrollView] display];	
	
}

-(void)rotateImagesInSelectedRangeBy90Degrees
{
	
	NSRange sRange = [self selectedRange];
	
	unsigned hoge;
	for( hoge = sRange.location; hoge  < NSMaxRange(sRange); hoge++ )
	{
		
		id something = NULL;
		something = [[self textStorage] attribute:NSAttachmentAttributeName
										  atIndex:hoge effectiveRange:NULL];
		
		if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"]|| 
			[ [[something attachmentCell] className] isEqualToString:@"MNGraphicAttachmentCell"]) /// select image
		{
			if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"] )
				
			{
				MNGraphicAttachmentCell* newCell = 
				[[[MNGraphicAttachmentCell alloc] initImageCell:[(MNGraphicAttachmentCell*)[something attachmentCell] image] ] autorelease];
				[something setAttachmentCell:newCell];
			}
			
			float scale = [(MNGraphicAttachmentCell*)[something attachmentCell] zoomScale];
			float angle = [(MNGraphicAttachmentCell*)[something attachmentCell] angle];
			angle += 90;
			if( angle >= 360 ) angle -= 360;
			
			[(MNGraphicAttachmentCell*)[something attachmentCell] setScale:scale andAngle:angle];
			
			// refresh
			[[self textStorage] addAttribute:NSAttachmentAttributeName value:something range:NSMakeRange(hoge, 1)];
			//[[self textStorage] addAttribute:MNGraphicSizeAttributeName value:NSStringFromSize(newSize) range:NSMakeRange(hoge, 1)];
			
			
			
			
		}
	}
	
	
	[[self enclosingScrollView] display];	
	
}


-(void)refreshSelectedRange
{
	
	NSRange sRange = [self selectedRange];
	
	unsigned hoge;
	for( hoge = sRange.location; hoge  < NSMaxRange(sRange); hoge++ )
	{
		
		id something = NULL;
		something = [[self textStorage] attribute:NSAttachmentAttributeName
										  atIndex:hoge effectiveRange:NULL];
		
		if( something != NULL ) /// select image
		{
			if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"] )
			{
				MNGraphicAttachmentCell* newCell = 
				[[[MNGraphicAttachmentCell alloc] initImageCell:[[something attachmentCell] image] ] autorelease];
				[something setAttachmentCell:newCell];
			}
			
			
			// refresh
			[[self textStorage] addAttribute:NSAttachmentAttributeName value:something range:NSMakeRange(hoge, 1)];

			
			
		}
	}
	
	
	[[self enclosingScrollView] display];	
	
}



/*
- (void)resetCursorRects
{
	
	
	NSRect rect =  selectedImageRect;
	if( selectedImage == NULL )
	{
//	 	[[self superview] resetCursorRects];
		return;
	}
	
	NSRect rr = [self bounds];
    NSCursor* cursor = [NSCursor arrowCursor];
    [self addCursorRect:rr cursor:cursor];
	
		
	NSRect cursorRect = NSMakeRect( NSMaxX(rect), NSMaxY(rect), SKT_HANDLE_WIDTH,SKT_HANDLE_WIDTH);
	[self addCursorRect:cursorRect cursor:reszeArrowCorner];
	
	
	cursorRect = NSMakeRect( rect.origin.x + rect.size.width/2, NSMaxY(rect), SKT_HANDLE_WIDTH,SKT_HANDLE_WIDTH);
	[self addCursorRect:cursorRect  cursor:reszeArrowBottom];
	
	
	cursorRect = NSMakeRect( NSMaxX(rect), rect.origin.y +  rect.size.height/2, SKT_HANDLE_WIDTH,SKT_HANDLE_WIDTH);
	[self addCursorRect:cursorRect  cursor:reszeArrowRight];
	
	
	
}

-(void)setDefaultCursorRect
{	
	[self discardCursorRects];
	NSRect rect = [self bounds];
    NSCursor* cursor = [NSCursor IBeamCursor];
    [self addCursorRect:rect cursor:cursor];
	
	[[NSCursor IBeamCursor] set];
}
*/


-(void)convertAlltextAttachments
{
	
	NSRange sRange = [self fullRange];
	
	unsigned hoge;
	for( hoge = sRange.location; hoge  < NSMaxRange(sRange); hoge++ )
	{
		
		[self convertAttachmentAt:hoge];
	}
	
	
	[[self enclosingScrollView] display];	
	
	
}

-(void)convertAttachmentAt:(unsigned)location
{
	id something =  [[self textStorage] attribute:NSAttachmentAttributeName 
										  atIndex:location
							longestEffectiveRange:nil inRange:[self fullRange]];
	if( something != nil )
	if( [ [[something attachmentCell] className] isEqualToString:@"NSTextAttachmentCell"] )
	{
		MNGraphicAttachmentCell* newCell = 
		[[[MNGraphicAttachmentCell alloc] initImageCell:[[something attachmentCell] image] ] autorelease];
		[something setAttachmentCell:newCell];
		
		
		// refresh
		[[self textStorage] addAttribute:NSAttachmentAttributeName value:something range:NSMakeRange(location, 1)];
		
	}	
}

-(void)fitGraphics
{
	
	[self fitGraphicsInRange:[self fullRange]];
}

-(void)fitGraphicsInSelectedRange
{
	
	[self fitGraphicsInRange:[self selectedRange]];
}


-(void)fitGraphicsInRange:(NSRange)selRange
{
	float width = PREF_documentWidth();
	float height = PREF_documentHeight();
	
	NSRange _selectedRange = [self selectedRange];
	
	width -= 1.0; // threshold 
	height -= 1.0;
	
	unsigned loc;
	
	NSRange range; 
	
	for( loc = selRange.location; loc < NSMaxRange(selRange); ) 
	{
		id something =  [[self textStorage] attribute:NSAttachmentAttributeName 
											  atIndex:loc
								longestEffectiveRange:&range inRange:[self fullRange]];
		
		if( something != NULL ) // resize to fit
		{
			
			[self convertAttachmentAt:loc];
			
			[self setSelectedRange:NSMakeRange(loc, 1)];
			
			NSSize newSize = [[something attachmentCell] trimRect].size;
			
			float angle = [[something attachmentCell] angle];
			if( angle == 90 || angle == 270 )
			{
				int _va = newSize.width;
				newSize.width = newSize.height;
				newSize.height = _va;	
			}
			
			
			float widthZoom = (float)width / (float)newSize.width;
			float heightZoom = (float)height / (float)newSize.height;
			
			
			//NSLog(@"%f    %f,%f",width,widthZoom, heightZoom);
			
			//enlarge horizontaly
			if( widthZoom < 1.0  && heightZoom > 1.0 )
				[self scaleImagesInSelectedRangeBy:widthZoom ];
			
			else if( widthZoom > 1.0  && heightZoom < 1.0 )
				[self scaleImagesInSelectedRangeBy:heightZoom ];
			
			else if( widthZoom < 1.0 && heightZoom < 1.0 )
			{
				if( widthZoom < heightZoom )
					[self scaleImagesInSelectedRangeBy:widthZoom ];
				
				else
					[self scaleImagesInSelectedRangeBy:heightZoom ];
				
				
			}
			
			
			
			
			
			
		}
		
		loc = NSMaxRange(range); 
	}		
	[self setSelectedRange:_selectedRange];
	
}

-(void)trimGraphics
{
	
	[self trimGraphicsInRange:[self fullRange]];
}

-(void)trimGraphicsInSelectedRange
{
	
	[self trimGraphicsInRange:[self selectedRange]];
}

-(void)trimGraphicsInRange:(NSRange)selRange
{
	
	unsigned loc;
	
	NSRange range; 
	
	for( loc = selRange.location; loc < NSMaxRange(selRange); ) 
	{
		id something =  [[self textStorage] attribute:NSAttachmentAttributeName 
											  atIndex:loc
								longestEffectiveRange:&range inRange:[self fullRange]];
		
		if( something != NULL ) // resize to fit
		{
			[self convertAttachmentAt:loc];
			
			
			//trim
			[ [something attachmentCell]	 smartTrim];
			// refresh
			[[self textStorage] addAttribute:NSAttachmentAttributeName 
									   value:something range:NSMakeRange(loc, 1)];
			
			
			
			NSSize newSize = [[something attachmentCell] trimRect].size;
			
			
			//NSLog(@"size %@ : %f",NSStringFromSize(newSize) , width );
			
			
			
			
		}
		
		loc = NSMaxRange(range); 
	}		
	
}

-(void)maximizeGraphics
{
	
	[self maximizeGraphicsInRange:[self fullRange]];
}

-(void)maximizeGraphicsInSelectedRange
{
	
	[self maximizeGraphicsInRange:[self selectedRange]];
}


-(void)maximizeGraphicsInRange:(NSRange)selRange
{
	float width = PREF_documentWidth();
	float height = PREF_documentHeight();
	
	NSRange _selectedRange = [self selectedRange];
	
	width -= 1.0; // threshold 
	height -= 1.0;
	
	unsigned loc;
	
	NSRange range; 
	
	for( loc = selRange.location; loc < NSMaxRange(selRange); ) 
	{
		id something =  [[self textStorage] attribute:NSAttachmentAttributeName 
											  atIndex:loc
								longestEffectiveRange:&range inRange:[self fullRange]];
		
		if( something != NULL ) // resize to fit
		{
			
			[self convertAttachmentAt:loc];
			
			[self setSelectedRange:NSMakeRange(loc, 1)];
			
			NSSize newSize = [[something attachmentCell] trimRect].size;
			
			
			float angle = [[something attachmentCell] angle];
			if( angle == 90 || angle == 270 )
			{
				int _va = newSize.width;
				newSize.width = newSize.height;
				newSize.height = _va;	
			}
			
			
			float widthZoom = (float)width / (float)newSize.width;
			float heightZoom = (float)height / (float)newSize.height;
			
		//NSLog(@"%f,%f",widthZoom, heightZoom);
			
			
			//enlarge horizontaly
			if( widthZoom < heightZoom )
			{
				
				[self scaleImagesInSelectedRangeBy:widthZoom ];
				
			}else
			{
				
				[self scaleImagesInSelectedRangeBy:heightZoom ];
				
			}
			
			
			
			
			
		}
		
		loc = NSMaxRange(range); 
	}		
	[self setSelectedRange:_selectedRange];
	
}



-(void)bakeFigures
{
	
	unsigned loc;
	
	NSRange range; 
	
	for( loc = 0; loc < NSMaxRange( [self fullRange] ); ) 
	{
		id something =  [[self textStorage] attribute:NSAttachmentAttributeName 
											  atIndex:loc
								longestEffectiveRange:&range inRange:[self fullRange]];
		
		if( something != NULL ) //
		{
			if( [[something attachmentCell] isKindOfClass:[MNGraphicAttachmentCell class]] )			
			{
				//trim
				NSData* data = [ [something attachmentCell]	 getImage] ;
				
				
				
				
				
				NSFileWrapper *wrapper = [[[NSFileWrapper alloc] initRegularFileWithContents:data ] autorelease];
				//add files
				
				[something setFileWrapper:wrapper];
				
				
				[wrapper setFilename:@"fig.tiff"];
				[wrapper setPreferredFilename:[wrapper filename]];//self rename
					
					NSAttributedString* aStr = [NSAttributedString attributedStringWithAttachment:something];
					
					
					
					
					// refresh
					[[self textStorage] replaceCharactersInRange:NSMakeRange(loc, 1)
											withAttributedString:aStr];
					
					
					
					
			}
			
			
		}
		
		loc = NSMaxRange(range); 
	}		
	
}

@end
