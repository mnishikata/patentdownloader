#import "GraphicInspector.h"
#import "GraphicResizableTextView.h"
#import "MNGraphicAttachmentCell.h"

@implementation GraphicInspector

static  id app_graphicInspectorPanel;

id APP_graphicInspectorPanel()
{
	return app_graphicInspectorPanel;
}

- (void)awakeFromNib
{
	
	app_graphicInspectorPanel = panel;
	 
	//[panel setFloatingPanel:NO];
	
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(selectionChanged:) 
												 name:NSTextViewDidChangeSelectionNotification object:NULL];
	 
	
	[[NSNotificationCenter defaultCenter] addObserver:self 
											 selector:@selector(windowWillClose:) 
												 name:NSWindowWillCloseNotification object:NULL];
	
	
	
	[panel setFloatingPanel:YES];
	 
}


-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	
	[super dealloc];
}


-(void)windowWillClose:(NSNotification*)notif
{
	if( textView == nil ) return;
	if( [textView window] ==	[notif object] )
	{
		textView = nil;
		[imageView setAttachmentCell:nil];
		[[panel contentView] display];
		[panel flushWindow];
	}
	
}

-(void)selectionChanged:(NSNotification*)notif
{

	
	if( [[notif object] window] == panel )
		return;


	if( ! [[notif object] isKindOfClass:[GraphicResizableTextView class]] )
		return;
	
	textView = [notif object];
	NSRange sRange = [textView selectedRange];
	if( sRange.length == 0 )
	{
		[self clearInspector];
		return;
	}
	
	
	// check if there is an image inside selected area

	NSAttributedString* _atr = [[textView textStorage] attributedSubstringFromRange:sRange];	
	if( ! [_atr containsAttachments] )
	{
		[self clearInspector];
		return;
	}
	
	
	//
	
	
	id cell = NULL;
	unsigned hoge;
	for( hoge = sRange.location; hoge  < NSMaxRange(sRange); hoge++ )
	{
	
		id something = NULL;
		something = [[textView textStorage] attribute:NSAttachmentAttributeName
											  atIndex:hoge effectiveRange:NULL];
		
		if( [[something attachmentCell] isKindOfClass:[MNGraphicAttachmentCell class]]  ) /// select image
		{
			if( cell != NULL )
			{ 
				cell = NULL;
				break;
			}
				cell = [something attachmentCell];
		}	
	

	}//now image indicate attachmentcell image if there is one image. image is NULL when there are more than one.
	


	if( cell != NULL )
	{
		float _scale = [cell zoomScale];
		[scaleFactor setStringValue:[NSString stringWithFormat:@"%f",  _scale * 100.0 ]];
		
		
		[imageView setAttachmentCell:cell ];
		[[panel contentView] display];
		[panel flushWindow];

	}else
	{
		[self clearInspector];

	}
	
}

-(void)refresh
{
	[textView refreshSelectedRange];
	[[panel contentView] display];
	[panel flushWindow];
}

-(void)clearInspector
{
	[scaleFactor setStringValue:@""];
	[imageView setAttachmentCell:nil ];	
	[[panel contentView] display];
	[panel flushWindow];
}

- (IBAction)outletChanged:(id)sender
{
	//NSLog(@"outlet");

	if( textView == nil ) return;
	if( ! [textView isKindOfClass:[NSTextView class]] ) return;
	
	
	NSRange sRange = [textView selectedRange];
	//NSLog(@" %@",NSStringFromRange(sRange));
	// check if there is an image inside selected area
	
	NSAttributedString* _atr = [[textView textStorage] attributedSubstringFromRange:sRange];	
	if( ! [_atr containsAttachments] )
	{
		[scaleFactor setStringValue:@""];
		return;
	}
	
	
	
	
	int tag = [sender tag];
	if( tag == 0 ) //scale
	{
		
		//NSLog(@"scale");
		
		float _scale = [scaleFactor floatValue] / 100;
		[textView scaleImagesInSelectedRangeBy:_scale ];
		 
		[self refresh];
		
	}else if( tag == 1 ) // revert
	{
		
		
	}else if( tag == 10 ) //rotate
	{
		
		//NSLog(@"rotation");
		
		float _scale = [scaleFactor floatValue] / 100;
		[textView rotateImagesInSelectedRangeBy90Degrees];
		
		[self refresh];

		
	}else if( tag == 11 ) //trim
	{
		
		//NSLog(@"trim");
		
		[textView trimGraphicsInSelectedRange];
		
		[self refresh];
		
		
	}else if( tag == 12 ) //fit
	{
		
		//NSLog(@"fit");
		
		[textView fitGraphicsInSelectedRange];
		
		[self refresh];
		
		
	}else if( tag == 13 ) //fit
	{
		
		[textView maximizeGraphicsInSelectedRange];
		
		[self refresh];
		
		
	}
	
	
	
	
}

@end
