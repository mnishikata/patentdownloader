//
//  NSTextAttachmentCell(GraphicExtention).m
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/05/01.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "MNGraphicAttachmentCell.h"

#define MARGIN 0
#define WHITE_COLOR 200


@implementation MNGraphicAttachmentCell

- (NSSize)cellSize
{
	
	NSSize size = [self trimRect].size;


	float scale = [self zoomScale];
	
	size.width = (int) ( (float)size.width * scale ) + MARGIN*2;
	size.height = (int) ( (float)size.height * scale ) + MARGIN*2 ;
	
	if( [self angle] == 90 || [self angle] == 270 )
	{
		int _va = size.width;
		size.width = size.height;
		size.height = _va;
	}
	
	
	////NSLog(@"%@",NSStringFromSize(size));
	
	return size;
}





- (void)drawFullImageWithFrame:(NSRect)cellFrame inView:(NSView *)aView
{
	NSRect _tr = [self trimRect];
	float _angle = [self angle];
	float _scale = [self zoomScale];

	[self setScale:1.0 andAngle:0.0];
	[self setTrimRect:NSZeroRect];

	[self drawWithFrame:cellFrame inView:aView];
	
	[self setTrimRect:_tr];
	[self setScale:_scale andAngle:_angle];

	
}

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)aView
{
	// debug
	
	
	
	
	NSRect _tr = [self trimRect];
	//debug end
	
	
	[[self image] setFlipped:YES];
	float _scale = [self zoomScale];
	
	/*
	
	NSAffineTransform *rotateTF = [NSAffineTransform transform];
	NSPoint centerPoint = NSMakePoint(cellFrame.origin.x + cellFrame.size.width / 2,  
									  cellFrame.origin.y + cellFrame.size.height / 2);
	
	//translate the image to bring the rotation point to the center  
	//(default is 0,0 ie lower left corner)
	[rotateTF translateXBy:centerPoint.x yBy:centerPoint.y];
	[rotateTF rotateByDegrees: [self angle]];
	[rotateTF translateXBy:-centerPoint.x yBy:-centerPoint.y];
	[rotateTF scaleBy:[self zoomScale]];
	[rotateTF concat];
		
	//[[self image] drawInRect:cellFrame fromRect:myRect operation:NSCompositeSourceOver fraction:1.0];
	[[self image] drawAtPoint:cellFrame.origin fromRect:myRect operation:NSCompositeSourceOver fraction:1.0];

	[rotateTF invert];
	[rotateTF concat];
	*/
	//
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: cellFrame.origin.x + cellFrame.size.width/2
					 yBy: cellFrame.origin.y + cellFrame.size.height/2 ]; 
	
	// rotate axis
	[affine rotateByDegrees:[self angle]];
	[affine scaleBy: _scale ];
	[affine concat];	
	
	NSRect rotatedCellFrame = cellFrame;
	if( [self angle] == 90 || [self angle] == 270 )
	{
		rotatedCellFrame.size.width = cellFrame.size.height;
		rotatedCellFrame.size.height = cellFrame.size.width;
	}
	
	
	
	// draw
	
	NSPoint _o = NSMakePoint( (-rotatedCellFrame.size.width/2+ MARGIN)  / _scale , (-rotatedCellFrame.size.height/2 + MARGIN) / _scale );
	NSSize _s = _tr.size;
	
	
	[[self image] drawInRect:NSMakeRect(_o.x, _o.y, _s.width, _s.height)
					 fromRect:_tr operation:NSCompositeSourceOver fraction:1.0];
	
	//[[self image] drawAtPoint:_o
	//fromRect:_tr operation:NSCompositeSourceOver fraction:1.0];  ----- doesnot work


	//[[attr string] drawAtPoint:NSMakePoint( -attrSize.width/2 , -attrSize.height/2) withAttributes:attributes];		
	[context restoreGraphicsState];

	
	///
	if( [aView isKindOfClass:[NSTextView class]] )
	{
	NSTextStorage* ts = [ (NSTextView*)aView textStorage];
	NSRange sr = [ (NSTextView*)aView selectedRange];
	NSAttributedString* attr = [ts attributedSubstringFromRange:sr];
	if( ! [attr containsAttachments] )	return;
	
	unsigned hoge;
	for( hoge = sr.location; hoge  < NSMaxRange(sr); hoge++ )
	{
		id attachment = [ts attribute:NSAttachmentAttributeName atIndex:hoge effectiveRange:NULL];
		
		if( [attachment attachmentCell] == self )
		{
			NSColor* color = [NSColor selectedTextBackgroundColor ];
			
			[[color colorWithAlphaComponent:0.5] set];
			
			[NSBezierPath fillRect:cellFrame];
			
		}
	}		
	}
	
	 
}

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)aView characterIndex:(unsigned)charIndex
{
	[self drawWithFrame:cellFrame inView:aView];
}

- (void)drawWithFrame:(NSRect)cellFrame inView:(NSView *)controlView characterIndex:(unsigned)charIndex layoutManager:(NSLayoutManager *)layoutManager
{

	
	[self drawWithFrame:cellFrame inView:controlView];
}

/////////////

-(NSSize)originalSize
{
	return [[self image] size];
		
}

-(float)zoomScale
{
	float scale = 1.0;
	
	NSString* title = [self title];
	NSArray* array = [title componentsSeparatedByString:@"+"];
	
	if( [array count] > 0 )
		scale = [[array objectAtIndex:0] floatValue];
	
	if( scale <= 0  ) scale = 1.0;
	
	return scale;
	
}

-(float)angle
{
	int angle = 0;
	
	NSString* title = [self title];
	
	NSArray* array = [title componentsSeparatedByString:@"+ "];
	if( [array count] > 1 )
		angle = [[array objectAtIndex:1] floatValue];

	return angle;
}
-(NSRect)trimRect
{
	NSRect trimRect = NSZeroRect;
	
	NSString* title = [self title];
	
	NSArray* array = [title componentsSeparatedByString:@"+ "];
	if( [array count] > 2 )
		trimRect = NSRectFromString( [array objectAtIndex:2] );
	
	else
		trimRect.size  = [self originalSize];
			
	return trimRect;
	
}
-(void)setTrimRect:(NSRect)rect
{
	NSSize originalSize = [self originalSize];
	
	// if rect == NSZeroRect, restore original
	
	if( NSEqualRects(rect , NSZeroRect) )
	{
		rect.size  = originalSize;
		
	}
	
	rect = NSIntersectionRect( rect , NSMakeRect(0,0, originalSize.width, originalSize.height ) );

	
	NSImage* image = [[[self image] copy] autorelease];
	NSString* title = [NSString stringWithFormat:@"%f+ %f+ %@", [self zoomScale], [self angle], NSStringFromRect(rect) ];
	[self setTitle:title];
	[self setImage:image];
	
}

-(void)setScale:(float)scale andAngle:(float)angle
{
	
	NSImage* image = [[[self image] copy] autorelease];
	NSString* title = [NSString stringWithFormat:@"%f+ %f+ %@", scale, angle, NSStringFromRect( [self trimRect] ) ];
	[self setTitle:title];
	[self setImage:image];
}


-(void)smartTrim
{
	
	[self setTrimRect:NSZeroRect];
	
	NSRect originalRect = NSZeroRect;
	originalRect.size = [self originalSize];
	
//		
	NSImage* trimTarget = [[[NSImage alloc] initWithSize:NSMakeSize(200,200)] autorelease];
	/*
	NSBitmapImageRep* rep = [[[NSBitmapImageRep alloc] 
		initWithBitmapDataPlanes:nil
					  pixelsWide:[trimTarget size].width
					  pixelsHigh:[trimTarget size].height
				   bitsPerSample:8
				 samplesPerPixel:3
						hasAlpha:NO
						isPlanar:YES
				  colorSpaceName:NSDeviceRGBColorSpace
					 bytesPerRow:0
					bitsPerPixel:0

		] autorelease];
	
	[trimTarget addRepresentation:rep];
	 */
	[trimTarget lockFocus ];
	[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];

	[[self image] setFlipped:NO];

	[[self image] drawInRect:NSMakeRect(0,0,200,200) fromRect:originalRect
				   operation:NSCompositeSourceOver fraction:1.0];
 
	
	[trimTarget unlockFocus];

	NSData* tif = [trimTarget TIFFRepresentation ];
	
	NSBitmapImageRep* rep = [[[NSBitmapImageRep alloc] initWithData:tif] autorelease];
	

	
	int Bpr, spp;
	unsigned char *data;
	int x, y, w, h;
	
	int bits = [rep bitsPerSample];
	//NSLog(@"bitsPerSample %d", bits);

	if ( bits != 8) {
		
		// usually each color component is stored
		// using 8 bits, but recently 16 bits is occasionally
		// also used ... you might want to handle that here
		//return;
	}
	
	Bpr = [rep bytesPerRow];
	spp = [rep samplesPerPixel];
	data = [rep bitmapData];
	w = [rep pixelsWide];
	h = [rep pixelsHigh];
	
	//NSLog(@"bytes per row %d",Bpr);
	
	BOOL flag = NO;
	
	
	//NSLog(@"w  %d, h %d, samplesPerPixel %d",w,h, spp);
	/*
	 1
	3 4
	 2
	 */
	
	NSRect trimRect = NSZeroRect;
	
	//(1)
	unsigned char *p;
		
	for (y=0; y<  h  ; y++)
	{
		for (x=0; x<  w  ; x++)
		{
			p = data + Bpr*y + spp * x;

			
			// do your thing
			// p[0] is red, p[1] is green, p[2] is blue, p[3] can be alpha if spp==4
			
			////NSLog(@"%d,%d,%d",p[0], p[1], p[2]);
			
			if( p[0]  > WHITE_COLOR  )
			{
				
			}else
			{
				flag = YES;
				break;
			}
			
		}
		if( flag == YES )
			break;
	}
	//NSLog(@"top margin %d", y);
	trimRect.origin.y = y;
	
	
	
	//(2)
	flag = NO;
	for (y = h-1; y >= 0  ; y-- )
	{
		for (x=0; x<  w  ; x++)
		{
			p = data + Bpr*y + spp * x;

			// do your thing
			// p[0] is red, p[1] is green, p[2] is blue, p[3] can be alpha if spp==4
			
			////NSLog(@"%d,%d,%d",p[0], p[1], p[2]);
			
			if( p[0] > WHITE_COLOR )
			{
				
			}else
			{
				flag = YES;
				break;
			}
			
		}
		if( flag == YES )
			break;
	}
	//NSLog(@"bottom margin %d", y);
	trimRect.size.height = y - trimRect.origin.y;
	
	
	
	
	//(3)
	flag = NO;
	for (x=0; x<  w  ; x++)
	{
		for (y = 0; y <  h  ; y++)
		{
			p = data + Bpr*y + spp * x;

			
			// do your thing
			// p[0] is red, p[1] is green, p[2] is blue, p[3] can be alpha if spp==4
			
			////NSLog(@"%d,%d,%d",p[0], p[1], p[2]);
			
			if( p[0] > WHITE_COLOR )
			{
				
			}else
			{
				flag = YES;
				break;
			}
		}
		if( flag == YES )
			break;
	}
	//NSLog(@"left margin %d", x);
	trimRect.origin.x = x;

	
	
	//(4)
	flag = NO;
	for (x = w-1; x >= 0  ; x-- )
	{
		for (y = 0; y <  h  ; y++)
		{
			p = data + Bpr*y + spp * x;
			
			
			// do your thing
			// p[0] is red, p[1] is green, p[2] is blue, p[3] can be alpha if spp==4
			
			////NSLog(@"%d,%d,%d",p[0], p[1], p[2]);
			
			if( p[0] > WHITE_COLOR )
			{
				
			}else
			{
				flag = YES;
				break;
			}
		}
		if( flag == YES )
			break;
	}
	//NSLog(@"right margin %d", x);
	trimRect.size.width = x - trimRect.origin.x ;

	//NSLog(@"trim rect %@", NSStringFromRect(trimRect));
	
	
	trimRect.origin.x = trimRect.origin.x - 2;
	trimRect.origin.y = trimRect.origin.y -2;
	trimRect.size.width = trimRect.size.width +4;
	trimRect.size.height = trimRect.size.height +4;
	
	
	// minimum size
	if( trimRect.size.width < 10 ) trimRect.size.width = 10;
	if( trimRect.size.height < 10 ) trimRect.size.height = 10;
	
	
	// convert 200x200 image to originalSize
	
	float factorW = (float)originalRect.size.width / 200;
	float factorH = (float)originalRect.size.height / 200;
	

	trimRect.origin.x = trimRect.origin.x * factorW;
	trimRect.origin.y = trimRect.origin.y * factorH;
	trimRect.size.width = trimRect.size.width * factorW;
	trimRect.size.height = trimRect.size.height * factorH;
	

	NSIntersectionRect( trimRect , NSMakeRect(0,0, w, h ) );
	
	[self setTrimRect:trimRect];
	
	
	

}




-(NSData*)getImageFullColor
{
	NSRect rect = [self trimRect];
	float angle = [self angle];
	if( angle == 90 || angle == 270 )
	{
		float ww = rect.size.width;
		rect.size.width = rect.size.height;
		rect.size.height = ww;
	}
	
	
	
	NSBitmapImageRep* imgrep = [[self image] bestRepresentationForDevice:nil];
	
	float wrep = [imgrep pixelsWide];
	float wimg = [self originalSize].width;
	float zoom = wrep / wimg;
	float _scale = [self zoomScale];
	[self setScale:zoom andAngle:angle]; // kokode yondeirunode imgrep kowareru
	
	rect.size.width = rect.size.width * zoom;
	rect.size.height = rect.size.height * zoom;
	
	
	// seisuka
	rect.origin.x = (int)rect.origin.x;
	rect.origin.y = (int)rect.origin.y;
	rect.size.width = (int)rect.size.width;
	rect.size.height = (int)rect.size.height;

	
	NSImage* newImage = [[[NSImage alloc] initWithSize:rect.size] autorelease];
	
	//NSLog(@"rep %@",[[[self image] representations] description]);

	imgrep = [[self image] bestRepresentationForDevice:nil]; // redefine imgrep
	NSBitmapImageRep* rep = [[[NSBitmapImageRep alloc] 

		initWithBitmapDataPlanes:nil
					  pixelsWide:(int)rect.size.width
					  pixelsHigh:(int)rect.size.height
				   bitsPerSample:[imgrep bitsPerSample]
				 samplesPerPixel:[imgrep samplesPerPixel]
						hasAlpha:[imgrep hasAlpha]
						isPlanar:[imgrep isPlanar]
				  colorSpaceName:[imgrep colorSpaceName]
					 bytesPerRow:0
					bitsPerPixel:0
		
		] autorelease];
	
	[newImage addRepresentation:rep];
	


	//NSLog(@"reps %@",[[newImage representations] description] );

	[newImage setFlipped:YES];
	[newImage setCacheDepthMatchesImageDepth:YES];
	[newImage setCacheMode:NSImageCacheNever];
	[newImage setPrefersColorMatch:YES];

	rect.origin.x = 0;
	rect.origin.y = 0;
	
	[newImage lockFocusOnRepresentation:rep];

	[self drawWithFrame:rect inView:nil];
	
	[newImage unlockFocus];
	
	

/*
	//_scale
	NSSize savingSize = [self trimRect].size;
	savingSize.width *= _scale;
	savingSize.height *= _scale;
	
	
	if( angle == 90 || angle == 270 )
	{
		float ww = savingSize.width;
		savingSize.width = savingSize.height;
		savingSize.height = ww;
	}
	
	[newImage setScalesWhenResized:YES];
	[[newImage bestRepresentationForDevice:nil] setSize:savingSize];
	[newImage  setSize:savingSize];
*/
	NSData* tif = [ newImage TIFFRepresentationUsingCompression:NSTIFFCompressionLZW  factor:1.0];
	

	
	//[self setScale:_scale andAngle:angle];
	return tif;

}





-(NSData*)getImage
{
	NSRect rect = [self trimRect];
	float angle = [self angle];

	
	
	NSImageRep* imgrep = [[self image] bestRepresentationForDevice:nil];
	
	float wrep = [imgrep pixelsWide];
	float wimg = [self originalSize].width;
	float zoom = wrep / wimg;
	float _scale = [self zoomScale];
	
	rect.size.width = rect.size.width * zoom;
	rect.size.height = rect.size.height * zoom;
	
	rect.origin.x = rect.origin.x * zoom;
	rect.origin.y = rect.origin.y * zoom;


	
	// seisuka
	rect.origin.x = (int)rect.origin.x;
	rect.origin.y = (int)rect.origin.y;
	rect.size.width = (int)rect.size.width;
	rect.size.height = (int)rect.size.height;
	
	
	
	//Quantize
	/*
	rect.origin.x = (int)(rect.origin.x /8 ) * 8
	rect.origin.y = (int)(rect.origin.y /8 ) * 8
	rect.size.width = (int)(rect.size.width /8 ) * 8
	rect.size.height = (int)(rect.size.height /8 ) * 8
*/
	
	
	int newPixelsWide; 
	int newPixelsHigh; 

	if( angle == 90 || angle == 270 )
	{
		newPixelsWide = rect.size.height;
		newPixelsHigh = rect.size.width;
	}else
	{
		newPixelsHigh = rect.size.height;
		newPixelsWide = rect.size.width;
	}
	
	

	
	
	NSBitmapImageRep* rep = [[[NSBitmapImageRep alloc] 
		
		initWithBitmapDataPlanes:nil
					  pixelsWide:newPixelsWide
					  pixelsHigh:newPixelsHigh
				   bitsPerSample:[imgrep bitsPerSample]
				 samplesPerPixel:[imgrep samplesPerPixel]
						hasAlpha:[imgrep hasAlpha]
						isPlanar:[imgrep isPlanar]
				  colorSpaceName:[imgrep colorSpaceName]
					 bytesPerRow:0
					bitsPerPixel:0
		
		] autorelease];
	
	unsigned char *imgdata = [imgrep bitmapData];
	unsigned char *data = [rep bitmapData];
	unsigned int Bpr = [rep bytesPerRow];
	unsigned int spp = [rep samplesPerPixel];
	unsigned int bits = [rep bitsPerSample];
	unsigned int bitLength = bits * spp;
	unsigned int w = rect.size.width;
	unsigned int h = rect.size.height;
	unsigned int imgBpr = [imgrep bytesPerRow];

	
	//NSLog(@"bit length  %d",bitLength);
	
	unsigned char *imgp;
	unsigned char *p;

		
	unsigned int x,y;
	unsigned int xOffset;
	unsigned int xByte;
	
	
	unsigned char* buffer;
	
    buffer = (unsigned char*)malloc( w*h * sizeof(unsigned char));
	
	unsigned _bb;
	for ( _bb = 0; _bb < w*h ; _bb++ )
	{
		buffer[_bb] = 0xFF;
	}
	
		
	
	// Expand bitmap
	
	if( bitLength < 8 )
	{
		unsigned int _w, _h;
		unsigned int intW, wBits;
		unsigned int offsetX, offsetY;
		
		unsigned int dest;
		unsigned int _wLoc;
		
		unsigned char mask = 0xFF;
		mask >>= bitLength; 
		mask = mask ^ 0xFF;
		
		
		offsetX = rect.origin.x;
		offsetY = rect.origin.y;
		
		
		
if( angle == 0 )
{
	//NSLog(@"HERE");
	
	_wLoc = bitLength *   offsetX;
	intW =   _wLoc >> 3 ; // gaitou baito
	wBits = _wLoc & 0x07; // offset
	
	for( _h = 0; _h < h; _h++)
	{
		imgp = imgdata	+ imgBpr * (_h+  offsetY ) + intW; 
		p = data + Bpr * _h;
			
		[MNGraphicAttachmentCell copyBitsFrom:imgp
									bitOffset:wBits
										   to:p 
									bitOffset:0 
									totalBits:Bpr * 8];
	}	
		
	//_scale
	NSSize savingSize = [self trimRect].size;
	savingSize.width *= _scale;
	savingSize.height *= _scale;
	
	[rep setSize:savingSize];
	
	
	return [rep TIFFRepresentationUsingCompression:NSTIFFCompressionCCITTFAX4 factor:1.0];
}
		
		
		for( _h = 0; _h < h; _h++)
		{
			imgp = imgdata	+ imgBpr * (_h+  offsetY ); 
			
			_wLoc = bitLength *   offsetX;
			for( _w = 0; _w < w; _w++)
			{
				 
				if( angle == 270 )
				{
					dest = h * (w-1  - _w) + _h;
					
					
				}else if( angle == 180 )
				{
					dest = w * (h-1  - _h) + (w-1 -_w);
					
				}else if( angle == 90 )
				{
					dest = h * _w + (h-1  - _h);
					
				}else
				{
					dest = w * _h + _w;
					
				}
				
				intW =   _wLoc >> 3 ; // gaitou baito
				wBits = _wLoc & 0x07; // offset
				
				buffer[ dest ] = *(imgp + intW  ) << wBits; 
				// bitLength ga buffer ni hairu  HIDARIZUME
				
				_wLoc += bitLength;

			}
		}	
	
		
		
		
		
		//NSLog(@"expand finished");
	// now buffer contains bitmap for 1-7 bits

		// Compress *(p + _bb  + xOffset )
		
		w = [rep pixelsWide];
		h = [rep pixelsHigh];

		unsigned char oneByteData;
		unsigned int bufferRow;
		for( _h = 0; _h < h; _h++)
		{
			p = data + Bpr*_h;
			bufferRow = w * _h;
			_wLoc = 0;
			for( _w = 0; _w < w; _w++)
			{


				intW =   _wLoc   >> 3 ;
				wBits = _wLoc  & 0x07;

				oneByteData = buffer[ bufferRow + _w ];

				*( p + intW ) |=  (mask & oneByteData ) >> wBits ;
			
				_wLoc += bitLength;

			}
		}
		

		//_scale
		NSSize savingSize = [self trimRect].size;
		savingSize.width *= _scale;
		savingSize.height *= _scale;


		if( angle == 90 || angle == 270 )
		{
			float ww = savingSize.width;
			savingSize.width = savingSize.height;
			savingSize.height = ww;
		}


		[rep setSize:savingSize];

		
		return [rep TIFFRepresentationUsingCompression:NSTIFFCompressionCCITTFAX4 factor:1.0];
		
	}else
	{
		return [self getImageFullColor];
	}
	
	return nil;
	
}


+(void)copyBitsFrom:(unsigned char*)orgn bitOffset:(int)orgnOffset 
				 to:(unsigned char*)dest bitOffset:(int)destOffset
		  totalBits:(unsigned)bits  // bits 32 ijou
{
	int bytes = (int)bits/8 ;
	
	if( bytes *8 < bits ) bytes++;
	
	unsigned char buffer ;

	
	int hoge;
	for ( hoge = 0; hoge < bytes; hoge++ )
	{
		buffer = *orgn;
		*dest = buffer;
		
		dest++;
		orgn++;
	}
	
	
}



- (BOOL)wantsToTrackMouse
{
	return YES;
}


- (BOOL)trackMouse:(NSEvent *)theEvent inRect:(NSRect)cellFrame ofView:(NSView *)aView atCharacterIndex:(unsigned)charIndex untilMouseUp:(BOOL)flag
{
	//NSLog(@"%d", [theEvent clickCount] );
	
	
	if([theEvent clickCount] > 1 )
	{

		[APP_graphicInspectorPanel() makeKeyAndOrderFront:self];
		return NO;
	}
		
	//NSLog(@"track");
	//[self setHighlighted:YES];
	if( [aView isKindOfClass:[NSTextView class]] )
	[aView setSelectedRange:NSMakeRange(charIndex,1)];


	return YES;
}


@end
