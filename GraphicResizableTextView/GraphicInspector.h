/* GraphicInspector */

#import <Cocoa/Cocoa.h>

extern id			APP_graphicInspectorPanel();

@interface GraphicInspector : NSResponder
{
    IBOutlet id panel;
    IBOutlet id scaleFactor;
	IBOutlet id imageView;
	
	NSTextView* textView;
}
- (IBAction)outletChanged:(id)sender;
@end
