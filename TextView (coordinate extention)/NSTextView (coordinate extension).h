//
//  TextView (coordinate extension).h
//  LinkTextViewSampleProject
//
//  Created by Masatoshi Nishikata on 12/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSTextView (coordinate_extension)

-(unsigned)charIndexAtPoint:(NSPoint)aPoint;
	//get charIndex from mouse location in a window
-(unsigned)charIndexOn:(NSPoint)aPoint; // ON MOUSE POINT


-(unsigned)glyphIndexForCharIndex:(unsigned)charIndex;

-(NSRect)rectForGlyphRange:(NSRange)glyphRange;
	//Returns a single bounding rectangle (in container coordinates) 

-(NSRect)rectForCharRange:(NSRange)charRange;
	//Returns a single bounding rectangle (in container coordinates) 

-(NSPoint)screenPointForCharIndex:(unsigned)charIndex;

-(NSRect)screenRectForCharRange:(NSRange)charRange;

-(NSRect)frameInScreen;
	// screen rect for the view



//etc
-(void)pasteText:(id)aText;
-(NSRange)fullRange;

@end
