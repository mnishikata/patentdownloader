//
//  NSString_FullwidthHalfwidth.h
//  ZenkakuHankakuConvert
//


#import <Foundation/Foundation.h>
#import <OgreKit/OgreKit.h>


typedef unsigned FWHWOptions;
enum {
	FWHWSpaceMask = 0x01, 
	
	FWHWNumberMask = 0x02,
	FWHWBasicLatinLetterMask = 0x04,
	FWHWBasicLatinSymbolMask = 0x08,
	FWHWBasicLatinAllMask = 0x02 | 0x04 | 0x08,
	
	FWHWBasicJapaneseAllMask = 0x10, 
	FWHWOtherMask = 0x80
};

enum {
	FWHWNotVoiced = 0, 
	FWHWVoiced, 
	FWHWSemiVoiced
};

@interface NSString (FullwidthHalfwidth)
- (NSString *)halfwidthString;
- (NSString *)fullwidthString;
- (NSString *)stringWithConvertFullwidthToHalfwidthOptions:(unsigned)convertFwToHwMask
						convertHalfwidthToFullwidthOptions:(unsigned)convertHwToFwMask 
									  passFullwidthOptions:(unsigned)passFwMask 
									  passHalfwidthOptions:(unsigned)passHwMask;
- (NSString *)hiraganaString;
- (NSString *)katakanaString;

@end
