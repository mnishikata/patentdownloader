//
//  TextView (coordinate extension).m
//  LinkTextViewSampleProject
//
//  Created by Masatoshi Nishikata on 12/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "NSTextView (coordinate extension).h"


@implementation NSTextView (coordinate_extension)

#pragma mark Coordinate System

-(unsigned)charIndexAtPoint:(NSPoint)aPoint // locate closest index
{
	
	
	/*
	 get charIndex from mouse location in a window
	 eg
	 
	 mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	 charIndex = [self charIndexAtPoint:mouseLoc];
	 
	 */
	if( [[self textStorage] length] == 0 ) return NSNotFound;
	
	
	
    int				glyphIndex, charIndex;
	//	Convert to view coordinates
	aPoint = [self convertPoint: aPoint  fromView: nil];
	
	//	Map the mouse location to the glyph index.
	glyphIndex = [[self layoutManager] glyphIndexForPoint: aPoint  inTextContainer: [self textContainer]];
	
	//	Glyphs aren't always the same as characters in a string, so now find the character index in the string.
	
	if( glyphIndex != NSNotFound )
		charIndex = [[self layoutManager] characterIndexForGlyphAtIndex: glyphIndex];
	else
		charIndex = [[self layoutManager] characterIndexForGlyphAtIndex: [[self layoutManager] numberOfGlyphs]-1];

	
	NSRect aRect = [self rectForCharRange:NSMakeRange(charIndex,1)];
	////NSLog(@"Point X %f, letter X %f -- %f", aPoint.x, aRect.origin.x, aRect.origin.x + aRect.size.width);
	
	if(  (aRect.origin.x + aRect.origin.x + aRect.size.width)/2 < aPoint.x  && ([[self textStorage] length] !=0) )
		charIndex++;
	
	return charIndex;
}

-(unsigned)charIndexOn:(NSPoint)aPoint // locate index ON THE MOUSE
{
	/*
	 get charIndex from mouse location in a window
	 eg
	 
	 mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  // mouse location in the window
	 charIndex = [self charIndexAtPoint:mouseLoc];
	 
	 */
	
	
	// return if view is empty
	
	if( [[self textStorage] length] == 0 ) return NSNotFound;
	
	
    int				glyphIndex, charIndex;
	//	Convert to view coordinates
	aPoint = [self convertPoint: aPoint  fromView: nil];
	
	//	Map the mouse location to the glyph index.
	glyphIndex = [[self layoutManager] glyphIndexForPoint: aPoint  inTextContainer: [self textContainer]];
	
	//	Glyphs aren't always the same as characters in a string, so now find the character index in the string.
	
	if( glyphIndex != NSNotFound )
		charIndex = [[self layoutManager] characterIndexForGlyphAtIndex: glyphIndex];
	else
		charIndex = [[self layoutManager] characterIndexForGlyphAtIndex: [[self layoutManager] numberOfGlyphs]-1];
	
	
	
	NSRect aRect = [self rectForCharRange:NSMakeRange(charIndex,1)];
	////NSLog(@"Point X %f, letter X %f -- %f", aPoint.x, aRect.origin.x, aRect.origin.x + aRect.size.width);
	
	
	return charIndex;
}

-(unsigned)glyphIndexForCharIndex:(unsigned)charIndex
{
	NSRange gRange = 
	[[self layoutManager] glyphRangeForCharacterRange:NSMakeRange(charIndex,0) actualCharacterRange:NULL];
	return gRange.location;
}

-(NSRect)rectForGlyphRange:(NSRange)glyphRange
{//Returns a single bounding rectangle (in container coordinates) 
	return [[self layoutManager] boundingRectForGlyphRange:glyphRange inTextContainer:[self textContainer]];	
}

-(NSRect)rectForCharRange:(NSRange)charRange
{//Returns a single bounding rectangle (in container coordinates) 
	return [[self layoutManager] boundingRectForGlyphRange:
		[[self layoutManager] glyphRangeForCharacterRange:charRange  actualCharacterRange:NULL]
										   inTextContainer:[self textContainer]];	
}

-(NSPoint)screenPointForCharIndex:(unsigned)charIndex
{
	NSRange aRange = NSMakeRange(charIndex, 0);
	NSRect aRect = [self rectForCharRange:aRange];
	aRect = [self convertRect:aRect toView:[[self window] contentView]];
	NSPoint aPoint= [[self window] convertBaseToScreen:aRect.origin];
	return aPoint;
}

-(NSRect)screenRectForCharRange:(NSRange)charRange
{
	NSRect aRect = [self rectForCharRange:charRange];
	aRect = [self convertRect:aRect toView:[[self window] contentView]];
	NSPoint aPoint= [[self window] convertBaseToScreen:aRect.origin];
	return NSMakeRect(aPoint.x, aPoint.y, aRect.size.width, aRect.size.height);
}



-(NSRect)frameInScreen // screen rect for MNTextView.  should be enclosed in NSScrollView
{
	NSRect aRect = [[[self superview] superview] frame]; //get enclosed scroll view frame
	
	NSPoint anOrigin = aRect.origin;
	NSPoint aSize;
	aSize.x = aRect.size.width;
	aSize.y = aRect.size.height;
	anOrigin = [[self window] convertBaseToScreen:anOrigin];
	
	return NSMakeRect(anOrigin.x, anOrigin.y, aSize.x, aSize.y);
}

-(void)pasteText:(id)aText
{	
	
	NSRange selectedRange = [self selectedRange];
	if(selectedRange.length != 0) [self insertText:@""];
	
	[self insertText:aText];
	
}

-(NSRange)fullRange
{
	return NSMakeRange(0, [[self textStorage] length]);
}

@end
