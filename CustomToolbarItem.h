//
//  StyleToolbarItem.h
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/04/07.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "CustomToolbarCell.h"
#import "CustomToolbarControl.h"
@interface CustomToolbarItem : NSToolbarItem {
	

	
	CustomToolbarCell*  cell;
	CustomToolbarControl* view;
	
	}


-(CustomToolbarControl*)control;

-(CustomToolbarCell*)cell;

-(void)setImage:(NSImage*)image;

-(void)menuSelected:(id)sender;

- (BOOL)allowsDuplicatesInToolbar;

-(IBAction)buttonClicked:(id)sender;

@end
