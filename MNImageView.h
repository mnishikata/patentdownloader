/* MNImageView */

#import <Cocoa/Cocoa.h>
#import "MNGraphicAttachmentCell.h"

typedef enum {
	NoKnob = 0,
	CornerKnob,
	RightKnob,
	BottomKnob,
	
	kNW,kN,kNE,
	kW,kC,kE,
	kSW,kS,kSE
	
	
	
} DRAGMODE;

@interface MNImageView : NSImageView
{
	MNGraphicAttachmentCell* cell;
	
	NSRect trimRectInImageView;
	BOOL iminhurry;
	
	NSCursor* reszeArrowCornerMirror;
	NSCursor* reszeArrowCorner;
	NSCursor* reszeArrowRight;
	NSCursor* reszeArrowBottom;
	
}

-(void)setAttachmentCell:(MNGraphicAttachmentCell*)aCell;

-(NSRect)rotateRectAccordingToCell:(NSRect)aRect  reverse:(BOOL)flag;

-(void)drawTrimFrame:(NSRect)rect;

-(NSRect)cellRectFromImageViewRect:(NSRect)aRect;


-(float)imageViewZoomFactor;

@end
