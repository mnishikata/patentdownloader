//
//  StyleToolbarItem.m
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/04/07.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "CustomToolbarItem.h"
#import "CustomToolbarControl.h"




@implementation CustomToolbarItem


- (id)initWithItemIdentifier:(NSString *)itemIdentifier
{
	if ((self = [super initWithItemIdentifier:itemIdentifier]) != nil) {
		// Add initialization code here
		
			

		cell = [[CustomToolbarCell alloc] init];
		
		[cell setBordered:NO];
		[cell setTitle:@""];
		
		[cell setTarget: self];
		[cell setAction: @selector(buttonClicked:)];
				
		view = [[CustomToolbarControl alloc] initWithFrame:NSMakeRect(0,0,32,32)] ;
		[view setCell:cell];
		
		[view setAutoresizingMask:NSViewWidthSizable|NSViewHeightSizable];
		
		[self setView: view ];
		[self setMinSize:NSMakeSize(24,24)];
		[self setMaxSize:NSMakeSize(32,32)];
		


	}
	return self;
}
-(void)dealloc
{

	[cell release];
	[view release];
	[super dealloc];
}

-(CustomToolbarControl*)control
{
	return view;
}

-(CustomToolbarCell*)cell
{
	return cell;
}



-(void)setImage:(NSImage*)image
{
	[cell setImage:image];
	/*
	NSMenuItem* mi = [[NSMenuItem alloc] initWithTitle:[self label] action:@selector(menuSelected:) keyEquivalent:@""];
	[mi setImage:image];
	
	[self setMenuFormRepresentation:[mi autorelease] ];
	 */
}

-(void)menuSelected:(id)sender
{
	//NSLog(@"?");
}

- (BOOL)allowsDuplicatesInToolbar
{
	return NO;
}


-(IBAction)buttonClicked:(id)sender
{
 
	//NSLog(@"toolbar style clicked ");	
 
		[[self target] performSelector:[self action]];
	
	
}

@end
