//
//  USDelegate.m
//  PatentDownloader
//
//  Created by Masatoshi Nishikata on 06/05/15.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "USDelegate.h"
#define PARENT_DOCUMENT [[[webView window] windowController] document]


@implementation USDelegate

-(id)init
{
    self = [super init];
    if (self) {
		
		autoPilotMode = NO;
		
		
		[NSBundle loadNibNamed:@"USDelegate"  owner:self];
		
		/*
		loadingIndicator = [[NSProgressIndicator alloc] initWithFrame:NSMakeRect(0,0,32,32)];
		[loadingIndicator setControlSize:NSRegularControlSize];
		[loadingIndicator setStyle:NSProgressIndicatorSpinningStyle];
		
		[loadingIndicator setAutoresizingMask:NSViewMinXMargin|NSViewMaxXMargin|
			NSViewMinYMargin|NSViewMaxYMargin ];
		[webView addSubview:loadingIndicator];
		NSRect frame = [webView frame];
		NSPoint center;
		center.x = frame.size.width /2 - 16;
		center.y = frame.size.height /2 - 16;

		
		[loadingIndicator setFrameOrigin:center ];
		 
		 */
		
		 
		
		
	}
	return self;
}

-(NSView*)view
{
	return view;
}

-(void)downloadingCanceled
{
	[PARENT_DOCUMENT endDownloading];
	autoPilotMode = NO;

}

-(IBAction)buttonClicked:(id)sender
{
//	[PARENT_DOCUMENT startWaitSession:@selector(startSession:) target:self];

	[PARENT_DOCUMENT startDownloading:self ];

}
-(void)downloadingDidStart
{
	

	
// issued
//	http://patft1.uspto.gov/netacgi/nph-Parser?TERM1=78&Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&r=0&f=S&l=50
// Hits 1 through 7 out of 7

	
// published
// http://appft1.uspto.gov/netacgi/nph-Parser?TERM1=20040123124&Sect1=PTO1&Sect2=HITOFF&d=PG01&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.html&r=0&f=S&l=50
// Hits 1 through 1 out of 1
	
	
	// direct
	//http://appft1.uspto.gov/netacgi/nph-Parser?Sect1=PTO1&Sect2=HITOFF&d=PG01&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.html&r=1&f=G&l=50&s1=%2220040123124%22.PGNR.&OS=DN/20040123124&RS=DN/20040123124
	
	
	int popupIndex = [popup indexOfSelectedItem];
	

	autoPilotMode = YES;
	
	
	NSString* urlString;
	
	
	if( popupIndex == 0 )
		urlString = [NSString stringWithFormat:@"http://patft1.uspto.gov/netacgi/nph-Parser?TERM1=%@&Sect1=PTO1&Sect2=HITOFF&d=PALL&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.htm&r=0&f=S&l=50", [textField stringValue]];
	
	if( popupIndex == 1 )
		urlString = [NSString stringWithFormat:@"http://appft1.uspto.gov/netacgi/nph-Parser?TERM1=%@&Sect1=PTO1&Sect2=HITOFF&d=PG01&p=1&u=%2Fnetahtml%2FPTO%2Fsrchnum.html&r=0&f=S&l=50", [textField stringValue]];

	
	
	
	
	NSURL* url = [NSURL URLWithString:  urlString ];
	NSURLRequest* urlRequest = [[[NSURLRequest alloc] initWithURL:url ] autorelease];
	
	
	[[webView mainFrame] loadRequest:urlRequest];
	
}


-(void)openTab
{
	autoPilotMode = NO;
	
	NSURL* url = [NSURL URLWithString:@"http://www.uspto.gov/patft/index.html"];
	NSURLRequest* urlRequest = [[[NSURLRequest alloc] initWithURL:url ] autorelease];


	[[webView mainFrame] loadRequest:urlRequest];
}
- (void)webView:(WebView *)sender didStartProvisionalLoadForFrame:(WebFrame *)frame
{
	[PARENT_DOCUMENT setLoading:YES];


}

- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame
{
	[PARENT_DOCUMENT setLoading:NO];
	[PARENT_DOCUMENT endDownloading];

	autoPilotMode = NO;

}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
	
	if( [sender mainFrame] != frame ) return;
	
	[PARENT_DOCUMENT setLoading:NO];
	
	
	WebDataSource* ds = [[webView mainFrame] dataSource ];
	NSString* title = [ds pageTitle];
	
	if( [title hasPrefix:@"United States Patent"] )
	{
		
		if( autoPilotMode == NO )
		{
		
			int result = NSRunAlertPanel (@"Do you want to download this patent document?",
							 @"",
							 @"OK", @"Cancel", nil);
			if( result != NSAlertDefaultReturn )
				return;
			
			
			//if OK
			
			[PARENT_DOCUMENT startDownloading_startSheet];
		
		}
		
		
		
		
		//// start retrieving
				//progress

				
		
		
		
		[ThreadWorker 
        workOn:self 
  withSelector:@selector(getImages:) 
    withObject:ds
didEndSelector:@selector(getImagesFinished:) ];
			
			
			

	}else 	if( autoPilotMode == YES )
	{
		// get source
		NSString* source = [[[NSString alloc] initWithData:[ds data] encoding:NSASCIIStringEncoding] autorelease]; 
		
		if( [source rangeOfString:@"</strong> out of <strong>1</strong>"].length != 0 )
		{
		
		//Hits <strong>1</strong> through <strong>1
		//</strong> out of <strong>1</strong>
			
			
			NSString* keyStr = @"(?<=<TD valign=top>1</TD>\n<TD valign=top><A  HREF=)[^>]*";
			
			NSRange range = [source rangeOfRegularExpressionString:keyStr];
			if( range.length != 0 )
			{
				NSString* nextLink = [NSString stringWithFormat:@"http://appft1.uspto.gov%@",
					[source substringWithRange:range]]; 
				
				NSURL* url = [NSURL URLWithString:nextLink];
				NSURLRequest* urlRequest = [[[NSURLRequest alloc] initWithURL:url ] autorelease];
				
				
				[[webView mainFrame] loadRequest:urlRequest];
				
				
			}else{
				[PARENT_DOCUMENT endDownloading];
				autoPilotMode = NO;
				
			}
			
			
		}else if( [source rangeOfString:@"</strong> out of <strong>"].length != 0 ||
				  [source rangeOfString:@"No patents have matched your query"].length != 0 ||
				  [source rangeOfString:@": 0 applications"].length != 0)
		{
			[PARENT_DOCUMENT endDownloading];
			autoPilotMode = NO;
	
	
		
		}else if( [source rangeOfString:@"Patent Database Search: Error"].length != 0 )
		{
			[PARENT_DOCUMENT endDownloading];
			autoPilotMode = NO;
			
		}
		
	}
	
	
}

-(id)getImages:(id)ds //ds = dataSource
{
	
	// get source
	NSString* source = [[[NSString alloc] initWithData:[ds data] encoding:NSASCIIStringEncoding] autorelease]; 
	
	// get "images" link
	// 
	
	NSRange linkRange = [source rangeOfRegularExpressionString:@"http://patimg[^<>]*|http://aiw[1-9].uspto.gov:[^>]*"];
	
	NSString* nextLink = [source substringWithRange:linkRange];
	
	// get str
	NSURL* url = [NSURL URLWithString:nextLink];
	NSMutableString* _contents = [NSMutableString stringWithContentsOfURL:url];
	
	NSRange pageRange = [_contents rangeOfRegularExpressionString:@"(?<=of) +[0-9]*"];
	int page = [[_contents substringWithRange:pageRange] intValue];
	
	[_contents chomp];
	NSRange imgRange = [_contents rangeOfRegularExpressionString :@"(?<=embed src=\")[^\"]*"];
	
	NSMutableString* imgURL = [NSMutableString stringWithString:[_contents substringWithRange:imgRange]];
	
	NSString* _absURL = [[[ds request] URL] absoluteString];
	NSRange _baseRange = [nextLink  rangeOfRegularExpressionString:@"http://[^/]*"];
	NSString* baseURL = [nextLink substringWithRange:_baseRange];
	
	
	NSRange _varRange = [imgURL rangeOfRegularExpressionString:@"(?<=PageNum=)[0-9]*(?=&)"];
	[imgURL replaceCharactersInRange:_varRange withString:@"%d"];
	
	//NSLog(@"page %d , image url %@ , abs  %@",page, imgURL, baseURL);
	
	
	[imgURL insertString:baseURL atIndex:0];
	
	///////////
	
	
	downloadedAttributedString = [[NSMutableAttributedString alloc] init];
	
	
	[downloadedAttributedString appendAttributedString:[ [[[webView mainFrame] frameView] documentView] attributedString] ];
	
	
	int currentPage = 1;
	for ( currentPage = 1; currentPage <= page; currentPage ++ )
	{
		if( [PARENT_DOCUMENT cancelState] == NSOnState )
		{
			[PARENT_DOCUMENT endDownloading];
			break;
			
		}
		
		NSString* currentURL = [NSString stringWithFormat:imgURL, currentPage];
		//NSLog(@"%@",currentURL);
		NSData* currentPageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:currentURL]];
		
		if( currentPageData == NULL ) {} //NSLog(@"err");
		
		NSFileWrapper* aWrapper = [[NSFileWrapper alloc] initRegularFileWithContents:currentPageData];
		
		[aWrapper setFilename:[NSString stringWithFormat:@"page %d.tiff", currentPage]];
		NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] 
												initWithFileWrapper:aWrapper] autorelease];
		[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
			NSAttributedString* aStr = [NSAttributedString
									attributedStringWithAttachment:anAttachment];
			[aWrapper release];
			
			
			[downloadedAttributedString appendAttributedString:aStr];
			
			
			[PARENT_DOCUMENT setProgressBar: (double)currentPage * 100.0 / (double)page ];

			
	}//6347683
	
	
	//NSLog(@"download finishing...");
	return nil;
	
}

-(void)getImagesFinished:(id)dummy
{
	//NSLog(@"closing process");

	
	[[webView window] setTitle:[[[webView mainFrame] dataSource] pageTitle]];
	
	[PARENT_DOCUMENT setProgressBar:-1.0];

	[PARENT_DOCUMENT setResult:downloadedAttributedString];

	if( PREF_onDownloadOptionTags() == YES )
		[PARENT_DOCUMENT convertUSTags:self];
	
	//set busy no
	[PARENT_DOCUMENT endDownloading];
	
	autoPilotMode = NO;
	
	


		/*
		void* contextInfo;
		NSSavePanel* aPanel = [NSSavePanel savePanel];
		[aPanel beginSheetForDirectory:NULL
								  file:[NSString stringWithFormat:@"%@.rtfd",[[[webView mainFrame] dataSource] pageTitle]]
						modalForWindow:[webView window]
						 modalDelegate:self
						didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:)
						   contextInfo: contextInfo ];
		 */

	
	
}

- (void)savePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode contextInfo:(void *)dummy
{
	if( returnCode == NSFileHandlingPanelOKButton ){
		
		
		NSFileWrapper *wrapper = [downloadedAttributedString
				RTFDFileWrapperFromRange:NSMakeRange(0, [downloadedAttributedString length])
													 documentAttributes:(NSDictionary *)NULL];
		
		// Create data from stirng with encoding
		
		//filename settei  ??
		[wrapper setFilename:[sheet filename]];
		[wrapper setPreferredFilename:[sheet filename]];
		
		
		
		BOOL flag = 
		
			[wrapper writeToFile:[sheet filename] atomically:YES updateFilenames:YES];

		
		if( flag == NO )
		{
			NSBeginAlertSheet(@"Error", @"OK",
							  NULL, NULL, [webView window] , NULL
							  ,  NULL, NULL ,NULL ,[ NSString stringWithCString: "保存できません"]);
			
		}
	
	}
	
	[downloadedAttributedString release];
}




- (void)webView:(WebView*)sender 
	   resource:(id)identifier 
        didFinishLoadingFromDataSource:(WebDataSource*)dataSource
{
	
	
}
@end
