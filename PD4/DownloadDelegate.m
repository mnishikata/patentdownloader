//
//  DownloadDelegate.m
//  PD4
//
//  Created by Masatoshi Nishikata on 06/05/24.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "DownloadDelegate.h"


@implementation DownloadDelegate



-(id)initWithRequest:(NSURLRequest*)req destination:(NSString*)path endDelegate:(id)target
{
    self = [super init];
    if (self) {
		
		
		//[NSBundle loadNibNamed:@"DownloadDelegate"  owner:self];
		

		urlDownload = [[NSURLDownload alloc] initWithRequest:req delegate:self];
		[urlDownload setDestination:path allowOverwrite:NO];
		
		endDelegate = target;
		
	}
	return self;
}


- (void)setDownloadResponse:(NSURLResponse *)aDownloadResponse 
{
    [aDownloadResponse retain];
    [downloadResponse release];
    downloadResponse = aDownloadResponse;
}
- (void)download:(NSURLDownload *)download didReceiveResponse:(NSURLResponse *)response
{
    // reset the progress, this might be called multiple times
    bytesReceived=0;
    // retain the response to use later
    [self setDownloadResponse:response];
}
-(id)downloadResponse
{
	return downloadResponse;
}	
- (void)download:(NSURLDownload *)download didReceiveDataOfLength:(unsigned)length
{
    long long expectedLength=[[self downloadResponse] expectedContentLength];
    bytesReceived=bytesReceived+length;
    if (expectedLength != NSURLResponseUnknownLength) {
        // if the expected content length is 
        // available, display percent complete
        float percentComplete=(bytesReceived/(float)expectedLength)*100.0;
        NSLog(@"Percent complete - %f",percentComplete);
    } else {
        // if the expected content length is 
        // unknown just log the progress
        NSLog(@"Bytes received - %d",bytesReceived);
    }
}
- (BOOL)download:(NSURLDownload *)download shouldDecodeSourceDataOfMIMEType:(NSString *)encodingType
{
	return NO;	
}
- (void)download:(NSURLDownload *)download didCreateDestination:(NSString *)path
{
	[destinationPath release];
	destinationPath = [NSString stringWithString:path];
	
	[destinationPath retain];
}


- (void)download:(NSURLDownload *)download didFailWithError:(NSError *)error
{
	[endDelegate performSelector:@selector(downloadDidEnd:)  withObject:nil];
}
- (void)downloadDidFinish:(NSURLDownload *)download
{
	NSData* data = [NSData dataWithContentsOfFile:destinationPath ];
	
	[endDelegate performSelector:@selector(downloadDidEnd:)  withObject:data];
	
}



@end
