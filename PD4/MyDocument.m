//
//  MyDocument.m
//  PD4
//
//  Created by Masatoshi Nishikata on 06/05/21.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//

#import "MyDocument.h"
#import "MNZip.h"




int stringSort(id v1, id v2, void *context)   /// supposed to be number
{		
	
	return [v1 compareNumerically:v2];
}


@implementation MyDocument

- (id)init
{
    self = [super init];
    if (self) {
    
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
    
    }
    return self;
}

- (NSString *)windowNibName
{
    // Override returning the nib file name of the document
    // If you need to use a subclass of NSWindowController or if your document supports multiple NSWindowControllers, you should remove this method and override -makeWindowControllers instead.
    return @"MyDocument";
}

- (void)windowControllerDidLoadNib:(NSWindowController *) aController
{
    [super windowControllerDidLoadNib:aController];
    // Add any code here that needs to be executed once the windowController has loaded the document's window.
}

- (NSData *)dataRepresentationOfType:(NSString *)aType
{
    // Insert code here to write your document from the given data.  You can also choose to override -fileWrapperRepresentationOfType: or -writeToFile:ofType: instead.
    
    // For applications targeted for Tiger or later systems, you should use the new Tiger API -dataOfType:error:.  In this case you can also choose to override -writeToURL:ofType:error:, -fileWrapperOfType:error:, or -writeToURL:ofType:forSaveOperation:originalContentsURL:error: instead.

    return nil;
}

- (BOOL)loadDataRepresentation:(NSData *)data ofType:(NSString *)aType
{
    // Insert code here to read your document from the given data.  You can also choose to override -loadFileWrapperRepresentation:ofType: or -readFromFile:ofType: instead.
    
    // For applications targeted for Tiger or later systems, you should use the new Tiger API readFromData:ofType:error:.  In this case you can also choose to override -readFromURL:ofType:error: or -readFromFileWrapper:ofType:error: instead.
    
    return YES;
}

-(BOOL)isSearchPage:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"Search International Patent Applications" ].length != 0 )
		return YES;
	
	return NO;

}
-(BOOL)hasUniqueSearchResult:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"</STRONG>: 1 record<BR>" ].length != 0 )
		return YES;
	
	return NO;
	
}

-(BOOL)isBiblio:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"<SPAN CLASS='tabOn'>Biblio. Data</SPAN>" ].length != 0 )
		return YES;
			
	return NO;
}
-(BOOL)isDescription:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"<SPAN CLASS='tabOn'>Description</SPAN>" ].length != 0 )
		return YES;
	
	return NO;	
}
-(BOOL)isClaims:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"<SPAN CLASS='tabOn'>Claims</SPAN>" ].length != 0 )
		return YES;
	
	return NO;	
}
-(BOOL)isDocuments:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"<SPAN CLASS='tabOn'>Documents</SPAN>" ].length != 0 )
		return YES;
	
	return NO;	
}

-(BOOL)isLoadingPDF:(NSString*)sourceStr
{
//
	if( [sourceStr rangeOfString:@"PCT Online File Inspection Document Retrieval Service" ].length != 0 )
		return YES;
	
	return NO;	
}


-(NSString*)getBiblio:(NSString*)sourceStr
{
	
	NSString* startStr = @"<!-- testDate";
	NSString* endStr = @"<!---- end of content section ---->";
	
	NSRange startRange =	[sourceStr rangeOfString:startStr];
	NSRange endRange =		[sourceStr rangeOfString:endStr];
	
	
	NSString* extStr = [sourceStr substringWithRange:NSUnionRange(startRange, endRange)];


	return extStr;
}

-(NSString*)getDescription:(NSString*)sourceStr
{
	
	NSString* startStr = @"<!-- foo -->";
	NSString* endStr = @"<!---- end of content section ---->";
	
	NSRange startRange =	[sourceStr rangeOfString:startStr];
	NSRange endRange =		[sourceStr rangeOfString:endStr];
	
	
	NSString* extStr = [sourceStr substringWithRange:NSUnionRange(startRange, endRange)];
	
	
	return extStr;
}

-(NSString*)getClaims:(NSString*)sourceStr
{
	
	NSString* startStr = @"<!-- foo -->";
	NSString* endStr = @"<!---- end of content section ---->";
	
	NSRange startRange =	[sourceStr rangeOfString:startStr];
	NSRange endRange =		[sourceStr rangeOfString:endStr];
	
	
	NSString* extStr = [sourceStr substringWithRange:NSUnionRange(startRange, endRange)];
	
	
	return extStr;
}


-(NSMutableArray*)getDocuments:(NSString*)sourceString
{
	[sourceString chomp];
	
	// addLine から addLineSortData　まで切り取る
	NSString* key = @"(?<=addLine\\(')(.(?!\\);t\\.addLineSortData))*";
	
	
	NSMutableArray* docsArray = [NSMutableArray array];
	
	OGRegularExpression    *regex = [OGRegularExpression regularExpressionWithString:key];
	// マッチ結果の列挙子の生成
	NSEnumerator    *enumerator = [regex matchEnumeratorInString:sourceString];
	OGRegularExpressionMatch    *match;	// マッチ結果
	while ((match = [enumerator nextObject]) != nil) {	// 順番にマッチ結果を得る
		
		NSLog(@"\n");
		//NSLog(@"%@", [match matchedString]);
		
		[docsArray addObject:[match matchedString]];
	}
	
	// Get URL
	//(?<=HREF=").[^"]*(?="><IMG ALT="PDF") 
	
	
	NSMutableArray* returnArray = [NSMutableArray array];
	
	
	
	unsigned hoge;
	for(hoge = 0; hoge < [docsArray count]; hoge++)
	{
		NSString* oneDoc = [docsArray objectAtIndex:hoge];
		
		NSRange urlRange = [oneDoc rangeOfRegularExpressionString:@"(?<=HREF=\").[^\"]*(?=\"><IMG ALT=\"ZIP\")"];
		NSString* urlString = [oneDoc substringWithRange:urlRange];
		
		NSArray* components = [oneDoc componentsSeparatedByRegularExpressionString:@"', *'"];
		NSString* nameString = [components objectAtIndex:0];
		NSString* dateString = [components objectAtIndex:1];
		NSString* pageString = [components objectAtIndex:3];
		
		NSLog(@"%@, %@, %@, %@", nameString, dateString, pageString, urlString);
		
		NSDictionary* _dic = [NSDictionary dictionaryWithObjectsAndKeys:
			
			nameString,		@"name",
			dateString,		@"date",
			pageString,		@"page",
			urlString,		@"url", nil];
		
		[returnArray addObject:_dic];
		
	}	
	
	return returnArray;
	
}
-(void)requestRelativeURLString:(NSString*)str inWebView:(WebView*)webView
{
	NSURL* url = [NSURL URLWithString:str
						relativeToURL:[[[[webView mainFrame] dataSource ] request] URL]];
	
		
	NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
	[[webView mainFrame] loadRequest: urlRequest];
	
}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
	
	if( frame != [sender mainFrame] )	return;
	
	NSData* sourceData = [[frame dataSource ] data];
	NSMutableString* sourceString = [[NSMutableString alloc] initWithData:sourceData encoding:NSASCIIStringEncoding]; 
	
	
	////////////
	
	if( [self isSearchPage:sourceString] )
	{

		
		[sender stringByEvaluatingJavaScriptFromString:@"document.frm.QUERY.value='WO2005052378'"];
		
				[sender stringByEvaluatingJavaScriptFromString:@"document.frm.submit();"];
		
		
	}
	
	
	else if( [self hasUniqueSearchResult:sourceString] )
	{
		//
		NSRange range = [sourceString 
rangeOfRegularExpressionString:@"(?<=<!--#ISEARCH-FETCH-LINK--><A HREF\\=')[^']*"];
		NSString* uniqueUrl = [sourceString substringWithRange:range];
		
		NSURL* url = [NSURL URLWithString:uniqueUrl];
		
		NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
		[[sender mainFrame] loadRequest: urlRequest];
		
		
	}
	
	
	
	else if( [self isBiblio:sourceString] )
	{
		[downloadedAttributedString release];
		downloadedAttributedString = [[NSMutableAttributedString alloc] init];

		
		
		[downloadedHTMLString release];
		downloadedHTMLString = [[NSMutableString alloc] init];
		
		[downloadedHTMLString appendString:@"<html><head><title>Downloaded Item</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" /><SCRIPT LANGUAGE=\"JavaScript\">drawingString = \"NULL\";</SCRIPT>"];
		
		
		NSString* _str = [self getBiblio:sourceString];
		[downloadedHTMLString appendString:_str];


		//
		NSRange range = [sourceString 
				rangeOfRegularExpressionString:@"(?<=DescTab' href\\=')[^']*"
									   options:OgreIgnoreCaseOption];
		NSString* str = [sourceString substringWithRange:range]; 
		

		[self requestRelativeURLString:str inWebView:sender];

	}
	
	else if( [self isDescription:sourceString] )
	{
		
		
		
		NSString* _str = [self getDescription:sourceString];
		[downloadedHTMLString appendString:_str];
		

		
		//
		
		
		NSRange range = [sourceString 
				rangeOfRegularExpressionString:@"(?<=ClaimsTab' href\\=')[^']*"
									   options:OgreIgnoreCaseOption];
		NSString* str = [sourceString substringWithRange:range]; 
		
		
		[self requestRelativeURLString:str inWebView:sender];
		
	}

	
	else if( [self isClaims:sourceString] )
	{
				
		NSString* _str = [self getClaims:sourceString];
		[downloadedHTMLString appendString:_str];
		
		
		//
		
		NSRange range = [sourceString 
				rangeOfRegularExpressionString:@"(?<=DocsTab' HREF\\=')[^']*"
									   options:OgreIgnoreCaseOption];
		NSString* str = [sourceString substringWithRange:range]; 
		
		
		[self requestRelativeURLString:str inWebView:sender];
		
	}

	
	else if( [self isDocuments:sourceString] )
	{
		NSMutableArray* array = [self getDocuments:sourceString];

		unsigned hoge;
		for( hoge = 0; hoge < [array count] ; hoge++ )
		{
			NSDictionary* _dic = [array objectAtIndex:hoge];
			if( [[_dic objectForKey:@"name"] isEqualToString:@"Publication"] )
			{
				NSURL* url = [NSURL URLWithString:[_dic objectForKey:@"url"]];
				
				NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
				[[sender mainFrame] loadRequest: urlRequest];
			}
		}
		
		
		WebView* wv = [[[WebView alloc] initWithFrame:NSMakeRect(0,0,1000,100) 
											frameName:@"downloadedHTML"
											groupName:@"Delegate" ] autorelease];
		
		
		[wv setFrameLoadDelegate:self];
		[[wv mainFrame] loadHTMLString:downloadedHTMLString
							   baseURL:nil];
		
	}
	
	
	else if( [self isLoadingPDF:sourceString] )
	{
		NSLog(@"loading pdf...");
		
	}

	
	
	
	
	////////////////
	
	else if( [[frame name] isEqualToString:@"downloadedHTML"] )
	{
		NSData* _data = [[frame dataSource] data];
		NSAttributedString* astr = [ [[frame frameView] documentView] attributedString];
		
		[downloadedAttributedString appendAttributedString:astr];
		
		NSLog(@"result %@",[astr string]);

	}
	
	else
	{
		NSLog(@"error");
		
	}
	
	
}


- (void)webView:(WebView *)sender decidePolicyForMIMEType:(NSString *)type request:(NSURLRequest *)request frame:(WebFrame *)frame decisionListener:(id<WebPolicyDecisionListener>)listener
{
	NSLog(@"type %@",type);
	if( [type isEqualToString:@"application/pdf"] )
	{
	NSLog(@"PDF REQUEST  URL %@", [[request URL] absoluteString]);
		[listener ignore];
	}
	
	
	else if( [type isEqualToString:@"application/zip"] )
	{
		
		NSString* scratchFolder =[NSHomeDirectory() 
					stringByAppendingPathComponent: @"Library/Application Support/PatentDownloader/"];
		
		
		//create folder if it does not exist
		if( ! [[NSFileManager defaultManager] directoryContentsAtPath:scratchFolder ] )
			[[NSFileManager defaultManager] createDirectoryAtPath:scratchFolder attributes:NULL];

		NSString* filename = [[[request URL] absoluteString] lastPathComponent];
		NSString* destinationPath = [scratchFolder stringByAppendingPathComponent:filename];
		
		
		NSLog(@"ZIP REQUEST  URL %@", [[request URL] absoluteString]);

		
		downloadDelegate = [[DownloadDelegate alloc] initWithRequest:request
									  destination:destinationPath
									  endDelegate:self];
		/*
		NSData* zipData = [NSData dataWithContentsOfURL:[request URL]];
		
		NSFileWrapper* filewrapper = [ MNZip unzip:zipData ];

		if( ![ filewrapper isDirectory] ) 
		{
			NSLog(@"err");
			return;				//err
		}
		
		
		NSDictionary* dic = [filewrapper fileWrappers];
		NSArray* keys = [dic allKeys];
		unsigned hoge;
		for ( hoge = 0; hoge < [keys count]; hoge++)
		{
			NSString* filename = [keys objectAtIndex:hoge];
			NSFileWrapper* aWrapper = [dic objectForKey:filename];
			

			NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] 
												initWithFileWrapper:aWrapper] autorelease];
			[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
				NSAttributedString* aStr = [NSAttributedString
									attributedStringWithAttachment:anAttachment];
				
				
				
			[downloadedAttributedString appendAttributedString:aStr];
				
			
		}*/
		
		// [[result textStorage] setAttributedString:downloadedAttributedString];

		
		[listener ignore];

	}

	
}

-(void)downloadDidEnd:(id)data
{
	[downloadDelegate release];
	
	if( data == nil )
		return;
	
		
	NSFileWrapper* filewrapper = [ MNZip unzip:data ];
	
	if( ![ filewrapper isDirectory] ) 
	{
		NSLog(@"err");
		return;				//err
	}
	
	
	NSDictionary* dic = [filewrapper fileWrappers];
	NSArray* keys = [dic allKeys];
	
	//sort
	
	keys = [keys sortedArrayUsingFunction:stringSort context:nil];
	
	
	unsigned hoge;
	
	for ( hoge = 0; hoge < [keys count]; hoge++)
	{
		NSString* filename = [keys objectAtIndex:hoge];
		
		if( [[[filename pathExtension] lowercaseString] isEqualToString:@"tif"] 
			|| [[[filename pathExtension] lowercaseString] isEqualToString:@"tiff"])
		{
		
		NSFileWrapper* aWrapper = [dic objectForKey:filename];
		
		NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] 
												initWithFileWrapper:aWrapper] autorelease];
		[[anAttachment fileWrapper] setFilename:filename];
		[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
		NSAttributedString* aStr = [NSAttributedString
								attributedStringWithAttachment:anAttachment];
		
		
		
		[downloadedAttributedString appendAttributedString:aStr];
		
		
		[[result textStorage] setAttributedString:downloadedAttributedString];
		}
	}
	
}


@end
