//
//  main.m
//  PD4
//
//  Created by Masatoshi Nishikata on 06/05/21.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, char *argv[])
{
    return NSApplicationMain(argc, (const char **) argv);
}
