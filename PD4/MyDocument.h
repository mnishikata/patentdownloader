//
//  MyDocument.h
//  PD4
//
//  Created by Masatoshi Nishikata on 06/05/21.
//  Copyright __MyCompanyName__ 2006 . All rights reserved.
//


#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import <OgreKit/OgreKit.h>
#import "DownloadDelegate.h"

@interface MyDocument : NSDocument
{
	IBOutlet id result;
	
	NSMutableString* downloadedHTMLString;

	NSMutableAttributedString* downloadedAttributedString;\
		
		DownloadDelegate* downloadDelegate;
}
@end
