//
//  DownloadDelegate.h
//  PD4
//
//  Created by Masatoshi Nishikata on 06/05/24.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface DownloadDelegate : NSObject {
	NSURLDownload* urlDownload;
	NSURLResponse* downloadResponse;
	
	unsigned bytesReceived;
	
	id endDelegate;
	NSString* destinationPath;
}

@end
