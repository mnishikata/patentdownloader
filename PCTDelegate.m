//
//  PCTDelegate.m
//  PatentDownloader
//
//  Created by Masatoshi Nishikata on 06/05/25.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "PCTDelegate.h"
#define PARENT_DOCUMENT [[[webView window] windowController] document]



int stringSortPCT(id v1, id v2, void *context)   /// supposed to be number
{		
	
	return [v1 compareNumerically:v2];
}



@implementation PCTDelegate



- (id)init
{
    self = [super init];
    if (self) {
		
		autoPilot = NO;
		[NSBundle loadNibNamed:@"PCTDelegate"  owner:self];

		
        // Add your subclass-specific initialization here.
        // If an error occurs here, send a [self release] message and return nil.
		
    }
    return self;
}

-(NSView*)view
{
	return view;
}

-(void)openTab
{
	autoPilot = NO;
	
	NSURL* url = [NSURL URLWithString:@"http://www.wipo.int/pctdb/en/search-simp.jsp"];
	NSURLRequest* urlRequest = [[[NSURLRequest alloc] initWithURL:url ] autorelease];
	
	
	[[webView mainFrame] loadRequest:urlRequest];
}
-(IBAction)buttonClicked:(id)sender
{
	NSString* key = [textField stringValue];
	if( [key isEqualToString:@"" ] ) return;
	
	
	[PARENT_DOCUMENT startDownloading:self];

	
}

-(void)downloadingDidStart
{
	autoPilot = YES;
	
	
	//http://www.wipo.int/pctdb/en/
	
	NSURL* url = [NSURL URLWithString:@"http://www.wipo.int/pctdb/en/search-simp.jsp"];
	
	NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
	[[webView mainFrame] loadRequest: urlRequest];
		
}

-(void)downloadingCanceled
{
	[webView stopLoading:self];
	autoPilot = NO;
}


-(BOOL)isSearchPage:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"Search International Patent Applications" ].length != 0 )
		return YES;
	
	return NO;
	
}
-(BOOL)hasUniqueSearchResult:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"</STRONG>: 1 record<BR>" ].length != 0 )
		return YES;
	
	return NO;
	
}

-(BOOL)isBiblio:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"<SPAN CLASS='tabOn'>Biblio. Data</SPAN>" ].length != 0 )
		return YES;
	
	return NO;
}
-(BOOL)isDescription:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"<SPAN CLASS='tabOn'>Description</SPAN>" ].length != 0 )
		return YES;
	
	return NO;	
}
-(BOOL)isClaims:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"<SPAN CLASS='tabOn'>Claims</SPAN>" ].length != 0 )
		return YES;
	
	return NO;	
}
-(BOOL)isDocuments:(NSString*)sourceStr
{
	if( [sourceStr rangeOfString:@"<SPAN CLASS='tabOn'>Documents</SPAN>" ].length != 0 )
		return YES;
	
	return NO;	
}

-(BOOL)isLoadingPDF:(NSString*)sourceStr
{
	//
	if( [sourceStr rangeOfString:@"PCT Online File Inspection Document Retrieval Service" ].length != 0 )
		return YES;
	
	return NO;	
}


-(NSString*)getBiblio:(NSString*)sourceStr
{
	
	NSString* startStr = @"<!-- testDate";
	NSString* endStr = @"<!-- content end -->";
	
	NSRange startRange =	[sourceStr rangeOfString:startStr];
	NSRange endRange =		[sourceStr rangeOfString:endStr];
	
	
	NSString* extStr = [sourceStr substringWithRange:NSUnionRange(startRange, endRange)];
	
	
	return extStr;
}

-(NSString*)getDescription:(NSString*)sourceStr
{
	
	NSString* startStr = @"<DIV ID=\"DescContent\" NAME=\"DescContent\">";
	NSString* endStr = @"<!-- content end -->";
	
	NSRange startRange =	[sourceStr rangeOfString:startStr];
	NSRange endRange =		[sourceStr rangeOfString:endStr];
	
	
	NSString* extStr = [sourceStr substringWithRange:NSUnionRange(startRange, endRange)];
	
	
	return extStr;
}

-(NSString*)getClaims:(NSString*)sourceStr
{
	
	NSString* startStr = @"<DIV ID=\"ClaimsContent\" NAME=\"ClaimsContent\">";
	NSString* endStr = @"<!-- content end -->";
	
	NSRange startRange =	[sourceStr rangeOfString:startStr];
	NSRange endRange =		[sourceStr rangeOfString:endStr];
	
	
	NSString* extStr = [sourceStr substringWithRange:NSUnionRange(startRange, endRange)];
	
	
	return extStr;
}

-(NSString*)publicationURL:(NSMutableString*)sourceString
{
	[sourceString chomp];

	NSString* extractHref = @"(?<=HREF=').[^']*(?='>ZIP \\(XML)";
	NSRange urlRange = [sourceString rangeOfRegularExpressionString: extractHref];
	NSString* urlString = [sourceString substringWithRange:urlRange];

	return urlString;
}


-(NSArray*)allDocumentsURL:(NSMutableString*)sourceString
{
	[sourceString chomp];
	
	NSMutableArray* docsArray = [[[NSMutableArray alloc] init] autorelease];
	NSString* extractHref = @"(?<=HREF=').[^']*(?='>ZIP)";
	OGRegularExpression *regex = [OGRegularExpression regularExpressionWithString:extractHref options:OgreNoneOption];
		
	NSEnumerator    *enumerator = [regex matchEnumeratorInString:sourceString];
	OGRegularExpressionMatch    *match;	// マッチ結果
	while ((match = [enumerator nextObject]) != nil) 
	{	// 順番にマッチ結果を得る
		
		
		[docsArray addObject:[match matchedString]];
	}
	
	
	
	return docsArray;
}


-(NSMutableArray*)getDocuments:(NSString*)sourceString //obsolete
{
	[sourceString chomp];
	
	//<TR><TD class='sortTableOdd'> kara 
	//<IMG ALT="ZIP" SRC="/shared/images/icon/zip.gif" BORDER=0></A></TD></TR>
	
	// addLine(' から );t.addLineSortData 　まで切り取る // format (?<= )(.(?! ))*
	NSString* key = @"(?<=\\<TR>\\<TD class='sortTableOdd'>)(.(?!/shared/images/icon/zip\\.gif\" BORDER=0>\\</A>\\</TD>\\</TR>))*";
	

	
	NSMutableArray* docsArray = [NSMutableArray array];
	
	OGRegularExpression    *regex = [OGRegularExpression regularExpressionWithString:key];
	// マッチ結果の列挙子の生成
	NSEnumerator    *enumerator = [regex matchEnumeratorInString:sourceString];
	OGRegularExpressionMatch    *match;	// マッチ結果
	while ((match = [enumerator nextObject]) != nil) 
	{	// 順番にマッチ結果を得る
		
		
		[docsArray addObject:[match matchedString]];
	}
	
	// Get URL
	//(?<=HREF=").[^"]*(?="><IMG ALT="PDF") 
	
	
	NSMutableArray* returnArray = [NSMutableArray array];
	
	
	NSString* extractHref = @"(?<=HREF=').[^']*(?='>ZIP \\(XML)";
													  NSLog(@"href %@",extractHref);
	unsigned hoge;
	for(hoge = 0; hoge < [docsArray count]; hoge++)
	{
		NSString* oneDoc = [docsArray objectAtIndex:hoge];
		NSRange urlRange = [oneDoc rangeOfRegularExpressionString: extractHref];
		NSString* urlString = [oneDoc substringWithRange:urlRange];
		
		
		NSArray* components = [oneDoc componentsSeparatedByRegularExpressionString:@"<"];
		NSString* nameString = [components objectAtIndex:0];
		//NSString* dateString = [components objectAtIndex:1];
		//NSString* pageString = [components objectAtIndex:3];
		
	NSLog(@"%@,  %@", nameString,  urlString);
		
		NSDictionary* _dic = [NSDictionary dictionaryWithObjectsAndKeys:
			
			nameString,		@"name",
			//dateString,		@"date",
			//pageString,		@"page",
			urlString,		@"url",
			nil];
		
		
		[returnArray addObject:_dic];
		
	}	
	NSLog(@"%@",[returnArray description]);
	return returnArray;
	
}

-(void)startDocumentDownload
{

	if( [documentsToBeDownloaded count] == 0 )
	{
		
		[PARENT_DOCUMENT setProgressBar:-1.0];
		
		[PARENT_DOCUMENT setResult:downloadedAttributedString];
		
		[PARENT_DOCUMENT endDownloading];
		autoPilot = NO;
		
		[documentsToBeDownloaded release];
		return;
	}
	
	NSURL* url = [NSURL URLWithString:[documentsToBeDownloaded objectAtIndex:0]];
	
	NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
	[[webView mainFrame] loadRequest: urlRequest];
	
	[documentsToBeDownloaded removeObjectAtIndex:0];
}


-(void)requestRelativeURLString:(NSString*)str inWebView:(WebView*)webView
{
	NSURL* url = [NSURL URLWithString:str
						relativeToURL:[[[[webView mainFrame] dataSource ] request] URL]];
	
	
	NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
	[[webView mainFrame] loadRequest: urlRequest];
	
}

- (void)webView:(WebView *)sender didStartProvisionalLoadForFrame:(WebFrame *)frame
{
	[PARENT_DOCUMENT setLoading:YES];
	
	
}

- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame
{
	[PARENT_DOCUMENT setLoading:NO];
	[PARENT_DOCUMENT endDownloading];

	autoPilot = NO;
	
}


- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
	//NSLog(@"finish");
	
	if( frame != [sender mainFrame] )	return;
	
	[PARENT_DOCUMENT setLoading:NO];

	
	
	NSData* sourceData = [[frame dataSource ] data];
	NSMutableString* sourceString = [[NSMutableString alloc] initWithData:sourceData encoding:NSASCIIStringEncoding]; 
	
	
	////////////
	
	
	if( autoPilot ==YES && [self isSearchPage:sourceString] )
	{
		
		NSString* str = [NSString stringWithFormat:@"document.frm.QUERY.value='%@'",[textField stringValue]];
		
		[sender stringByEvaluatingJavaScriptFromString:str ];
		
		[sender stringByEvaluatingJavaScriptFromString:@"document.frm.submit();"];
		
		
	}
	
	
	else if( autoPilot ==YES && [self hasUniqueSearchResult:sourceString] )
	{
		//
		NSRange range = [sourceString 
rangeOfRegularExpressionString:@"(?<=<!--#ISEARCH-FETCH-LINK--><A HREF\\=')[^']*"];
		NSString* uniqueUrl = [sourceString substringWithRange:range];
		
		NSURL* url = [NSURL URLWithString:uniqueUrl];
		
		NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
		[[sender mainFrame] loadRequest: urlRequest];
		
		
	}
	
	
	
	else if( [self isBiblio:sourceString] )
	{
		
		if( autoPilot == NO )
		{
			int result = NSRunAlertPanel (@"Do you want to download this patent document?",
										  @"",
										  @"OK", @"Cancel", nil);
			if( result != NSAlertDefaultReturn )
				return;
			
			
			//if OK
			autoPilot = YES;
			[PARENT_DOCUMENT startDownloading_startSheet];	
			
		}
		
		
		//set title
		[[webView window] setTitle:[[[webView mainFrame] dataSource] pageTitle]];

		
		[downloadedAttributedString release];
		downloadedAttributedString = [[NSMutableAttributedString alloc] init];
		
		
		
		//get url
		

		
		NSURL* requesturl = [[[[webView mainFrame] dataSource] request] URL ];
		
		
		NSDictionary* dic = [NSDictionary dictionaryWithObject:requesturl forKey:NSLinkAttributeName];
		
		NSAttributedString* astr = [[[NSAttributedString alloc] initWithString:@"This document was downloaded from WIPO website.\n\n" attributes:dic] autorelease];
		
		[downloadedAttributedString appendAttributedString:astr];
			
			
		
		
		
		[downloadedHTMLString release];
		downloadedHTMLString = [[NSMutableString alloc] init];
		
		[downloadedHTMLString appendString:@"<html><head><title>Downloaded Item</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"><meta http-equiv=\"Content-Type\" content=\"text/html;charset=UTF-8\" /><SCRIPT LANGUAGE=\"JavaScript\">drawingString = \"NULL\";</SCRIPT>"];
		
		
		NSString* _str = [self getBiblio:sourceString];
		[downloadedHTMLString appendString:_str];
		
		
		//
		NSRange range = [sourceString 
				rangeOfRegularExpressionString:@"(?<=DescTab' href\\=')[^']*"
									   options:OgreIgnoreCaseOption];
		NSString* str = [sourceString substringWithRange:range]; 
		
		
		[self requestRelativeURLString:str inWebView:sender];
		
	}
	
	else if( autoPilot ==YES && [self isDescription:sourceString] )
	{
		
		
		
		NSString* _str = [self getDescription:sourceString];
		[downloadedHTMLString appendString:_str];
		
		
		
		//
		
		
		NSRange range = [sourceString 
				rangeOfRegularExpressionString:@"(?<=ClaimsTab' href\\=')[^']*"
									   options:OgreIgnoreCaseOption];
		NSString* str = [sourceString substringWithRange:range]; 
		
		
		[self requestRelativeURLString:str inWebView:sender];
		
	}
	
	
	else if( autoPilot ==YES && [self isClaims:sourceString] )
	{
		
		NSString* _str = [self getClaims:sourceString];
		[downloadedHTMLString appendString:_str];
		
		
		//
		
		NSRange range = [sourceString 
				rangeOfRegularExpressionString:@"(?<=DocsTab' HREF\\=')[^']*"
									   options:OgreIgnoreCaseOption];
		NSString* str = [sourceString substringWithRange:range]; 
		
		
		[self requestRelativeURLString:str inWebView:sender];
		
	}
	
	
	else if( autoPilot ==YES && [self isDocuments:sourceString] )
	{
		
		
		
		if( [downloadOption selectedRow] == 0 ) // none
		{
			documentsToBeDownloaded = [[NSMutableArray array] retain];

			
		}else if( [downloadOption selectedRow] == 1 ) //pub
		{
			
			documentsToBeDownloaded = [[NSMutableArray array] retain];
			
			[documentsToBeDownloaded addObject:[self publicationURL:sourceString ] ];

			
			
		}else if( [downloadOption selectedRow] == 2 ) //all
		{
			documentsToBeDownloaded = [[NSMutableArray arrayWithArray: [self allDocumentsURL:sourceString ] ] retain];

			/*
			unsigned hoge;
			for( hoge = 0; hoge < [array count] ; hoge++ )
			{
				NSDictionary* _dic = [array objectAtIndex:hoge];

				NSURL* url = [NSURL URLWithString:[_dic objectForKey:@"url"]];
				
				NSURLRequest* urlRequest = [NSURLRequest requestWithURL:url];
				[[sender mainFrame] loadRequest: urlRequest];

			}
			 */

			
		}
		
		
		
		
		WebView* wv = [[[WebView alloc] initWithFrame:NSMakeRect(0,0,1000,100) 
											frameName:@"downloadedHTML"
											groupName:@"Delegate" ] autorelease];
		
		
		[wv setFrameLoadDelegate:self];
		[[wv mainFrame] loadHTMLString:downloadedHTMLString
							   baseURL:nil];
		
		


		
	}
	
	
	else if( autoPilot ==YES && [self isLoadingPDF:sourceString] )
	{
		//NSLog(@"loading pdf...");
		
	}
	
	
	
	
	
	////////////////
	
	else if( [[frame name] isEqualToString:@"downloadedHTML"] )
	{
		NSData* _data = [[frame dataSource] data];
		NSAttributedString* astr = [ [[frame frameView] documentView] attributedString];
		
		[downloadedAttributedString appendAttributedString:astr];
		
		////NSLog(@"result %@",[astr string]);
		
		[self startDocumentDownload];

	}
	
	else if( autoPilot ==YES )
	{
		//NSLog(@"error");
		
		autoPilot = NO;
		[PARENT_DOCUMENT endDownloading];
		
	}
	
	
}


- (void)webView:(WebView *)sender decidePolicyForMIMEType:(NSString *)type request:(NSURLRequest *)request frame:(WebFrame *)frame decisionListener:(id<WebPolicyDecisionListener>)listener
{
	//NSLog(@"type %@",type);
	if( [type isEqualToString:@"application/pdf"] )
	{
		//NSLog(@"PDF REQUEST  URL %@", [[request URL] absoluteString]);
		[listener ignore];
	}
	
	
	else if( [type isEqualToString:@"application/zip"] )
	{
		
		NSString* scratchFolder =[NSHomeDirectory() 
					stringByAppendingPathComponent: @"Library/Application Support/PatentDownloader/"];
		
		
		//create folder if it does not exist
		if( ! [[NSFileManager defaultManager] directoryContentsAtPath:scratchFolder ] )
			[[NSFileManager defaultManager] createDirectoryAtPath:scratchFolder attributes:NULL];
		
		NSString* filename = [[[request URL] absoluteString] lastPathComponent];
		NSString* destinationPath = [scratchFolder stringByAppendingPathComponent:filename];
		
		
		//NSLog(@"ZIP REQUEST  URL %@", [[request URL] absoluteString]);
		
		
		downloadDelegate = [[DownloadDelegate alloc] initWithRequest:request
														 destination:destinationPath
														 endDelegate:self
													   ownerDocument:PARENT_DOCUMENT];
			
		
		[listener ignore];
		
	}
	
	
}

-(void)downloadDidEnd:(id)data
{
	
	[downloadDelegate release];
	[PARENT_DOCUMENT setLoading:NO];

	
	if( data == nil )
	{
		[self startDocumentDownloadLater];

		return;
	}
	
	
	NSFileWrapper* filewrapper = [ MNZip unzip:data ];
	
	if( ![ filewrapper isDirectory] ) 
	{
		//NSLog(@"err");
		[self startDocumentDownloadLater];

		return;				//err
	}
	
	
	NSDictionary* dic = [filewrapper fileWrappers];
	NSArray* keys = [dic allKeys];
	
	//sort
	
	keys = [keys sortedArrayUsingFunction:stringSortPCT context:nil];
	
	
	unsigned hoge;
	
	for ( hoge = 0; hoge < [keys count]; hoge++)
	{
		NSString* filename = [keys objectAtIndex:hoge];
		
		if( [[[filename pathExtension] lowercaseString] isEqualToString:@"tif"] 
			|| [[[filename pathExtension] lowercaseString] isEqualToString:@"tiff"])
		{
			
			NSFileWrapper* aWrapper = [dic objectForKey:filename];
			
			NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] 
												initWithFileWrapper:aWrapper] autorelease];
			[[anAttachment fileWrapper] setFilename:filename];
			[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
				NSAttributedString* aStr = [NSAttributedString
								attributedStringWithAttachment:anAttachment];
				
				
				
				[downloadedAttributedString appendAttributedString:aStr];
				
				
		}
	}

	
	[self startDocumentDownloadLater];
	
	
}
-(void)startDocumentDownloadLater
{
		[NSTimer scheduledTimerWithTimeInterval:1.0 target:self
									   selector:@selector(startDocumentDownload)
									   userInfo:nil repeats:NO];
}

@end
