//
//  USDelegate.h
//  PatentDownloader
//
//  Created by Masatoshi Nishikata on 06/05/15.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import <OgreKit/OgreKit.h>
#import "ThreadWorker.h"

@interface USDelegate : NSObject {

	IBOutlet id view;
	IBOutlet id webView;



	IBOutlet id popup;
	IBOutlet id textField;
	
	/////
	
	BOOL	autoPilotMode;
	NSMutableAttributedString* downloadedAttributedString;
	
}

@end
