//
//  AboutDelegate.h
//  PatentDownloader
//
//  Created by Masatoshi Nishikata on 06/05/15.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>


@interface AboutDelegate : NSObject {

	
	NSProgressIndicator* loadingIndicator;
	WebView* webView;
}

@end
