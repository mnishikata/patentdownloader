//
//  TextView(printing).h
//  SampleApp
//
//  Created by Masatoshi Nishikata on 18/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "AppController.h"
#import "GraphicResizableTextView.h"


@interface PrintingTextView : GraphicResizableTextView

{
	int		currentPageNumber;
	int		totalPageNumber;
	
	PrintingTextView* pview;
	NSMutableDictionary* rectForPageCache;
}

-(NSRect)properPageRectFor:(NSRect)aRect;
@end
