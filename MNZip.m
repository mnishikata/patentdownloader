//
//  MNZip.m
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/04/05.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "MNZip.h"


@implementation MNZip

+(NSData*)zip:(id)raw //raw is nsdata or nsfilewrapper
{
	NSString* scratchFolder =[NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/PatentDownloader/"];
	
	
	//create folder if it does not exist
	if( ! [[NSFileManager defaultManager] directoryContentsAtPath:scratchFolder ] )
		[[NSFileManager defaultManager] createDirectoryAtPath:scratchFolder attributes:NULL];
	
	
	
	
	NSString* sourceFilename = @"data";
	NSString* targetFilename = @"zipped data";
	
	NSString* sourcePath =[scratchFolder stringByAppendingPathComponent: sourceFilename];
	NSString* targetPath =[scratchFolder stringByAppendingPathComponent: targetFilename];
	
	
	BOOL flag;
	
	
	if( [raw isKindOfClass:[NSData class]] )
		flag = [raw writeToFile:sourcePath atomically:YES];
	
	
	else 	if( [raw isKindOfClass:[NSFileWrapper class]] )
		flag = [raw writeToFile:sourcePath atomically:YES updateFilenames:YES];
	
	
	if( flag == NO )
		return NULL;
	
	/* Assumes sourcePath and targetPath are both
		valid, standardized paths. */
	
	
	
	//----------------
	// Create the zip task
	NSTask * backupTask = [[NSTask alloc] init];
	[backupTask setLaunchPath:@"/usr/bin/ditto"];
	[backupTask setArguments:
		[NSArray arrayWithObjects:@"-c", @"-k", @"-X", @"--rsrc", 
			sourcePath, targetPath, nil]];
	
	// Launch it and wait for execution
	[backupTask launch];
	[backupTask waitUntilExit];
	
	// Handle the task's termination status
	if ([backupTask terminationStatus] != 0)
		return NULL;
	
	// You *did* remember to wash behind your ears ...
	// ... right?
	[backupTask release];
	
	
	NSData* convertedData = [[[NSData alloc] initWithContentsOfFile:targetPath] autorelease];
	
	//delete scratch
	
	[[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceDestroyOperation
												 source:scratchFolder
											destination:@"" 
												  files:[NSArray arrayWithObjects:sourceFilename, targetFilename,NULL]
													tag:NULL];
	
	
	return convertedData;
}


+(NSFileWrapper*)unzip:(NSData*)zipData
{
	NSString* scratchFolder =[NSHomeDirectory() 
					stringByAppendingPathComponent: @"Library/Application Support/PatentDownloader/"];
	
	
	//create folder if it does not exist
	if( ! [[NSFileManager defaultManager] directoryContentsAtPath:scratchFolder ] )
		[[NSFileManager defaultManager] createDirectoryAtPath:scratchFolder attributes:NULL];
	
	
	
	NSString* sourceFilename = @"zipped data";
	NSString* targetFilename = @"unzipped folder";
	
	NSString* sourcePath =[scratchFolder stringByAppendingPathComponent: sourceFilename];
	NSString* targetPath =[scratchFolder stringByAppendingPathComponent: targetFilename];
	
	
	
	BOOL flag = [zipData writeToFile:sourcePath atomically:YES];
	
	if( flag == NO )
		return NULL;
	
	
	//Unzip
	
	//-------------------
	NSTask *cmnd=[[NSTask alloc] init];
	[cmnd setLaunchPath:@"/usr/bin/ditto"];
	[cmnd setArguments:[NSArray arrayWithObjects:
		@"-v",@"-x",@"-k",@"--rsrc",sourcePath,targetPath,nil]];
	[cmnd launch];
	[cmnd waitUntilExit];
	
	// Handle the task's termination status
	if ([cmnd terminationStatus] != 0)
		return NULL;
	
	// You *did* remember to wash behind your ears ...
	// ... right?
	[cmnd release];
	
	
	
	//unzip　(1)ファイルがジップされていた場合は、フォルダにひとつだけ　(2)フォルダの場合は、その中身。
	
	//
	
	NSArray* contents = [[NSFileManager defaultManager] directoryContentsAtPath:targetPath];
	
	
	NSFileWrapper* wrapper;
	
	if( [contents count] == 1 )
	{
		NSString* onepath;
		onepath = [targetPath stringByAppendingPathComponent:[contents lastObject]];
		
		NSData* data = [NSData dataWithContentsOfFile:onepath];
		
		wrapper = [[[NSFileWrapper alloc] initRegularFileWithContents:data  ] autorelease];
		
		
	}
	else	if( [contents count] > 1 )
	{
		
		wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
		
		unsigned hoge;
		for( hoge = 0; hoge < [contents count]; hoge++ )
		{
			NSString* onepath;
			NSString* onefilename;
			
			onefilename = [contents objectAtIndex:hoge];
			onepath = [targetPath stringByAppendingPathComponent:onefilename];
			
			NSData* data = [NSData dataWithContentsOfFile:onepath];
			
			[wrapper addRegularFileWithContents:data   preferredFilename:onefilename ];
		}
	}
	
	
	
	//delete scratch
	
	[[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceDestroyOperation
												 source:scratchFolder
											destination:@"" 
												  files:[NSArray arrayWithObjects:sourceFilename, targetFilename,NULL]
													tag:NULL];
	
	
	return wrapper;
}

/*
 - (IBAction)open:(id)sender {
	 
	 NSString* text = @"test";
	 
	 NSData* data = [text dataUsingEncoding:NSUTF8StringEncoding];
	 
	 
	 
	 //zip
	 
	 
	 NSData* convertedData = [self zip:data];
	 
	 [convertedData writeToFile:[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop/zip result"] atomically:YES];
	 
	 
	 

	  // zip 2
	  
	  NSFileWrapper* zipwrap = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL ] autorelease];
	  
	  [zipwrap addRegularFileWithContents:data   preferredFilename:@"data1" ];
	  [zipwrap addRegularFileWithContents:data   preferredFilename:@"data2" ];
	  
	  NSData* convertedData = [self zip:zipwrap];
	  
	  [convertedData writeToFile:[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop/zip result"] atomically:YES];
	  
	 
	 
	 
	 // unzip
	 
	 NSFileWrapper* wrapper = [self unzip:convertedData];
	 
	 if( [wrapper isRegularFile] )
	 {
		 //NSLog(@"this is regular file wrapper");
		 
		 
	 }
	 else
	 {
		 //NSLog(@"this is directory wrapper");
		 
		 
		 NSDictionary* regularFileWrappers = [wrapper fileWrappers];
		 
		 //NSLog(@"contains %@", [[regularFileWrappers allKeys] description] );
		 
	 }
	 
	 [wrapper  writeToFile:[NSHomeDirectory() stringByAppendingPathComponent: @"Desktop/unzip result"] atomically:YES updateFilenames:YES];
	 
 }
 
 
 
 */
@end
