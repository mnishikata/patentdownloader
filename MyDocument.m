//
// MyDocument.m
//

#import "MyDocument.h"
#define NUMEROFTABS 4

@implementation MyDocument


static NSString*    JapanIdentifier = @"JapanIdentifier";
static NSString*    USIdentifier = @"USIdentifier";
static NSString*    AboutIdentifier = @"AboutIdentifier";
static NSString*    PCTIdentifier = @"PCTIdentifier";
static NSString*    ActionIdentifier = @"ActionIdentifier";


//--------------------------------------------------------------//
// NSDocument のメソッド
//--------------------------------------------------------------//

// ウィンドウに関連付けられる nib ファイルの名前を返します
- (NSString*)windowNibName
{
    return @"MyDocument";
	
	iAmBusy  = NO;
	
	
	
}

-(void)startWaitSession:(SEL)endSelector target:(id)endTarget // return YES when ready. NO when canceled
{
	int waitStatus = 1; // 0 no  1 ok  -1 cancel
	unsigned hoge;

	NSDocumentController* docCtrl = [NSDocumentController sharedDocumentController];
	NSArray* array = [docCtrl documents];

	
	// get initial status
	for( hoge = 0; hoge < [array count] ; hoge++ )
	{
		NSDocument* doc = [array objectAtIndex:hoge];
		if( doc != self && [doc isBusy] )
		{
			waitStatus = 0;
			break;
		}
	}
	if( waitStatus == 1 )
	{
		
		[endTarget performSelector:endSelector withObject:[NSNumber numberWithBool:YES]]; 
		return ;
	}
	
	
	
	// start sheet
	
	[queCancelButton setState:NSOffState];
	
	[[NSApplication sharedApplication] beginSheet:queSheet modalForWindow:[self windowForSheet]
									modalDelegate:NULL didEndSelector:NULL contextInfo:NULL];
	
	
	queSelector = endSelector;
	queTarget = endTarget;
	
	queTimer = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(startWaitSession_func:)  userInfo:nil repeats:NO];
	
	[queTimer retain];
}

-(void)startWaitSession_func:(NSTimer*)myTimer
{
	int waitStatus = 1; // 0 no  1 ok  -1 cancel
	unsigned hoge;
	
	
	NSDocumentController* docCtrl = [NSDocumentController sharedDocumentController];
	NSArray* array = [docCtrl documents];
	
	
	
	if( [queCancelButton state] == NSOnState)
	{
		waitStatus = -1;
		
		[queSheet close];
		[[NSApplication sharedApplication] endSheet:queSheet ];

		[queTarget performSelector:queSelector withObject:[NSNumber numberWithBool:NO]]; 
		
		return;
	}
	
	//check other docs
	
	waitStatus = 1; // assume ok. cancel it later.
	array = [docCtrl documents];
	
	for( hoge = 0; hoge < [array count] ; hoge++ )
	{
		
		NSDocument* doc = [array objectAtIndex:hoge];
		if( doc != self && [doc isBusy] )
		{
			waitStatus = 0;
			break;
		}
	}
	
	
	
	if( waitStatus != 0 )
	{
		[queSheet close];
		[[NSApplication sharedApplication] endSheet:queSheet ];

		[queTarget performSelector:queSelector withObject:[NSNumber numberWithBool:YES]]; 

		return;
	}
	
	[NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(startWaitSession_func:)
							   userInfo:nil repeats:NO];		

	
	
	

	return;
}



-(void)createAboutTab
{
	NSRect baseFrame = [[[tabView tabViewItemAtIndex:0] view] frame];
	NSTabViewItem* tabViewItem = [[NSTabViewItem alloc] init];

	WebView* aboutWebView = [[WebView alloc] initWithFrame: baseFrame];
	[tabViewItem setView: aboutWebView];

	[aboutWebView setFrameSize:	baseFrame.size ];

	[aboutWebView setAutoresizingMask:NSViewHeightSizable| NSViewWidthSizable ];

	[tabViewItem setIdentifier:AboutIdentifier];
	
	[tabView addTabViewItem: tabViewItem  ];
	
	
	
	aboutDelegate = [[AboutDelegate alloc] init ];
		
	[aboutDelegate setWebView:aboutWebView];
	
	[aboutDelegate openTab];
	[self setBusy:YES];


}


-(void)dealloc
{
	
	[super dealloc];
}	

- (void)windowControllerDidLoadNib:(NSWindowController*)windowController
{
	
	[[windowController  window] setDelegate:self];

	//setup toolbar
	
	// NSToolbarのインスタンスを作ります
    NSToolbar*  toolbar;
    toolbar = [[[NSToolbar alloc] initWithIdentifier:@"ToolbarForTabView"] autorelease];
	
	// ツールバーを設定します
    [toolbar setDelegate:self];
    [[windowController window] setToolbar:toolbar];
	

	[self createAboutTab];

	if( [ tabView numberOfTabViewItems ] != NUMEROFTABS )
	{
		//NSLog(@"terminate %d", [ tabView numberOfTabViewItems ]);
		[[NSApplication sharedApplication] terminate:self];
	}
	
	[[tabView tabViewItemAtIndex:0] setIdentifier:JapanIdentifier];
	[[tabView tabViewItemAtIndex:1] setIdentifier:USIdentifier];
	[[tabView tabViewItemAtIndex:2] setIdentifier:PCTIdentifier];

	[tabView selectLastTabViewItem:self];
	[toolbar setSelectedItemIdentifier:AboutIdentifier];

	
 	

	
	//Japan
	japaneseDelegate = [[JapaneseDelegate alloc] init];
	
	NSTabViewItem* tbi = [tabView tabViewItemAtIndex:0];
	[tbi setView:[japaneseDelegate view]]; 
	
	
	// us
	
	usDelegate = [[USDelegate alloc] init];

	tbi = [tabView tabViewItemAtIndex:1];
	[tbi setView:[usDelegate view]];
	
	
	// pct
	
	pctDelegate = [[PCTDelegate alloc] init];
	
	tbi = [tabView tabViewItemAtIndex:2];
	[tbi setView:[pctDelegate view]];
	
	

	[saveButton setEnabled:NO];
	[printButton setEnabled:NO];

	
	
	[[self windowForSheet] setFrameUsingName: @"MyDocumentWindow"];
    [[self windowForSheet] setFrameAutosaveName: @"MyDocumentWindow"];
	
		
	//[self showAboutPeriodically];
	
	///// pref
	id rbsplitsub=  [[[textView superview] superview] superview];
	id rbsplit = [rbsplitsub superview];
	
	BOOL vertical = PREF_splitVertically();
	[rbsplit setVertical: vertical ];

	[self adjustSplitSize];
	
	
	
	[[[[textView superview] superview] superview] collapse];

}
-(void)adjustSplitSize
{
	id rbsplitsub=  [[[textView superview] superview] superview];
	id rbsplit = [rbsplitsub superview];

	
	if( [rbsplit isVertical] == YES )
		[[rbsplit subviewAtPosition:0] setMinDimension:400.0 andMaxDimension:3000];
	else
		[[rbsplit subviewAtPosition:0] setMinDimension:150.0 andMaxDimension:3000];
	
	
}
-(WebView*)webView
{

	return [[[tabView selectedTabViewItem] view] nextKeyView] ;	
}
-(NSTextView*)textView
{
	return textView;
}

-(void)setResult:(NSAttributedString*)astr
{
	[[textView textStorage] setAttributedString:astr];
	[saveButton setEnabled:YES];
	[printButton setEnabled:YES];

	[[[[textView superview] superview] superview] expand];
	
	NSRect newRect = [[textView window] frame];
	newRect.origin.y --;
	newRect.size.height ++;
	[[textView window] setFrame:newRect display:YES animate:YES];
	/*
	NSRect frame = [[textView window] frame];
	frame.size.height -= 1;
	[[textView window] setFrame:frame display:YES];
	frame.size.height += 1;
	[[textView window] setFrame:frame display:YES];
*/
	
	
	[textView sizeToFit];
	[textView convertAlltextAttachments];

	if( PREF_onDownloadOptionTrim() == YES )
		[textView trimGraphics];

	if( PREF_onDownloadOptionScale() == YES )
		[textView fitGraphics];
	
	
	if( NO /*PREF_onDownloadOptionMakeList() == YES*/ )
	{
	
		NSString* message = @"\n<This list was created and inserted by Patent Downloader>\n";
		NSString* _str = [[[textView textStorage] fugoFinder] buildTable:nil];
		
		NSString* str = [NSString stringWithFormat:@"%@%@",message,_str];
		
		NSAttributedString* attr = [[[NSAttributedString alloc] initWithString:str] autorelease];

		[[textView textStorage] appendAttributedString:attr];
	}
}

- (BOOL)windowShouldClose:(id)sender
{
	
	if( [self isBusy] ) 
	{
		NSBeep();
		return NO;
	}
	
	if( [aboutTimer isValid] ) [aboutTimer invalidate];
	return YES;
}


-(void)showAboutPeriodically
{
	aboutTimer = [NSTimer scheduledTimerWithTimeInterval:600.0 target:self selector:@selector(showAboutPeriodically)
								   userInfo:nil repeats:NO];
	
	
	if( [self isBusy] )	return;
	
	[tabView selectLastTabViewItem:self];
	
}


-(BOOL)isBusy
{
	return iAmBusy;
}

-(void)setBusy:(BOOL)flag
{
	iAmBusy = flag;
}




//print
- (IBAction)printDocument:(id)sender
{
	
	// Ask PrintingTextView to print
	
	[[self textView] print:self];
	
}
- (IBAction)rotateFigures:(id)sender
{
	[[self textView] rotateImagesInSelectedRangeBy90Degrees];

}

- (IBAction)fitAllGraphicsToPaperSize:(id)sender
{
	[[self textView] fitGraphics];
}

-(IBAction )adjustAllGraphicsToPaperSize:(id)sender
{
	[[self textView] maximizeGraphics];

}
-(IBAction )trimAllGraphics:(id)sender
{
	[[self textView] trimGraphics];
	
}

- (IBAction)toggleSplit:(id)sender
{
	id rbsplit = [[[[textView superview] superview] superview] superview];
	[rbsplit setVertical: ! [rbsplit isVertical] ];
	
	[self adjustSplitSize];
}

//save
- (IBAction)saveDocumentAs:(id)sender
{
	[self setFileName:[[self windowForSheet] title]];

	[super saveDocumentAs:sender];

	
}

- (IBAction)cog:(id)sender
{
	
	

	NSURL* url;
	
	CFLocaleRef locale = CFLocaleCopyCurrent();
	CFTypeRef string = CFLocaleGetValue(
										locale,
										kCFLocaleCurrencyCode
										);
	
	
	
	//NSLog(@"language lang %@",string);
	
	
	if( [ (NSString*)string isEqualToString:@"JPY"] )
		url = [NSURL URLWithString:@"http://www.oneriver.jp/Cog/index.html"];
	
	else
		url = [NSURL URLWithString:@"http://www.oneriver.jp/Cog/index_e.html"];
	
	
	NSString* path;
	
	NSFileWrapper *textStorage_rtfd_wrapper = 
		[[textView textStorage] RTFDFileWrapperFromRange:NSMakeRange( 0, [[textView textStorage] length] )
						   documentAttributes:nil];
	
	path =[NSTemporaryDirectory() stringByAppendingPathComponent:@"Untitled.rtfd"];
	[textStorage_rtfd_wrapper writeToFile:path atomically:NO updateFilenames:NO];
	
	NSString* cogPath = [[NSWorkspace sharedWorkspace] absolutePathForAppBundleWithIdentifier:@"com.pukeko.oneriver.cog"];
	
	if( cogPath == nil || [cogPath isEqualToString:@""] )
	{
		[[NSWorkspace sharedWorkspace] openURL:url];
		
	}else
	{
		[[NSWorkspace sharedWorkspace] openFile:path withApplication:cogPath];
	}
		
	
		
}


-(IBAction)convertUSTags:(id)sender
{
	[self convertUSTagsSub];
	
	[self convertUSTagsSub];

}

-(void)convertUSTagsSub
{
	NSString* path = [[NSBundle bundleForClass:[self class]] pathForResource:@"USMathTags" ofType:@"rtf"] ;
	NSData* data = [[[NSData alloc] initWithContentsOfFile:path] autorelease];
	
	NSAttributedString* attr = [[[NSAttributedString alloc] initWithRTF:data documentAttributes:nil] autorelease];
	
	
	//find ^.*$
	NSMutableArray* array = [NSMutableArray array];
	
	OGRegularExpression    *regex = [OGRegularExpression regularExpressionWithString:@"^..*$" ];
	NSEnumerator    *enumerator = [regex matchEnumeratorInAttributedString: attr ];	// 
	
	OGRegularExpressionMatch    *match;	// 
	while ((match = [enumerator nextObject]) != nil)
	{	
		[array addObject: [attr attributedSubstringFromRange: [match rangeOfMatchedString] ]];
		
	}
	//NSLog(@"%@",[array description]);
	
	// array ;	0, 2, 4, .... contains original attr string
	//			1, 3, 5 ... attr string to be converted
	
	
	///////////////////////
	
	NSAttributedString* newTextStorage = [[[NSAttributedString alloc] 
									initWithAttributedString:[textView textStorage]] autorelease];
	
	unsigned hoge;
	for( hoge = 0; hoge < [array count]; hoge = hoge + 2)
	{
		NSAttributedString* oAttr = [array objectAtIndex:hoge];
		NSAttributedString* nAttr = [array objectAtIndex:hoge +1];
		
		regex = [OGRegularExpression regularExpressionWithString:[oAttr string] ];
		
		
		newTextStorage = [regex replaceAttributedString: newTextStorage
								   withAttributedString: nAttr
												options:OgreReplaceWithAttributesOption | OgreReplaceFontsOption | OgreMergeAttributesOption
												  range:NSMakeRange(0, [[newTextStorage string] length])
											 replaceAll:YES];
	}
	
	[[textView textStorage] setAttributedString: newTextStorage] ;	
	
}


- (NSFileWrapper *)fileWrapperRepresentationOfType:(NSString*)type//overide
{
	
	[textView bakeFigures];
	
	
	NSData* mySavingData;
	NSRange mySavingRange;
	mySavingRange.location = 0;
	mySavingRange.length =  [[textView textStorage] length];
	mySavingData = [textView RTFDFromRange:mySavingRange];
    
	//NSLog(@"format %@", type);
	
	NSFileWrapper *wrapper;
	
	if( [type isEqualToString:@"RTFD"] )
	wrapper = [[textView textStorage] RTFDFileWrapperFromRange:mySavingRange
															 documentAttributes:(NSDictionary *)NULL];
	
	else if( [type isEqualToString:@"MicrosoftWord"] )
	{
		NSData* _data = [[textView textStorage] docFormatFromRange:mySavingRange
												documentAttributes:(NSDictionary *)NULL];
		
		wrapper = [[[NSFileWrapper alloc ] initRegularFileWithContents:_data] autorelease];
	}

    // Create data from stirng with encoding
	
	//filename settei  ??
 	[wrapper setFilename:[[self windowForSheet] title] ];
	[wrapper setPreferredFilename:[wrapper filename]];

	
    return wrapper;
}

/*
- (IBAction)savePatentDocument:(id)sender
{
	void* contextInfo;
	NSSavePanel* aPanel = [NSSavePanel savePanel];
	[aPanel beginSheetForDirectory:NULL
							  file:[NSString stringWithFormat:@"%@.rtfd",[[self windowForSheet] title]]
					modalForWindow:[self windowForSheet]
					 modalDelegate:self
					didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:)
					   contextInfo:contextInfo];
	///
}

- (void)savePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
	if( returnCode == NSFileHandlingPanelOKButton ){
		BOOL flag = 
			
			[[self fileWrapperRepresentationOfType:NULL] 
				writeToFile:[sheet filename] atomically:YES updateFilenames:YES];
		
		if( flag == NO )
		{
			NSBeginAlertSheet(@"Error", @"OK",
							  NULL, NULL, [self windowForSheet] , NULL
							  ,  NULL, NULL ,NULL ,[ NSString stringWithCString: "保存できません"]);
			
		}
	}
}
*/






- (void)tabView:(NSTabView *)aTabView didSelectTabViewItem:(NSTabViewItem *)tabViewItem
{
	int index = [aTabView indexOfTabViewItem:tabViewItem];
	if( index == 0 )
		[japaneseDelegate openTab];

	if( index == 1 )
		[usDelegate openTab];
	
	if( index == 2 )
		[pctDelegate openTab];

	if( index == 3 )
		[aboutDelegate openTab];
	
	
	
	
}

- (NSArray*)toolbarDefaultItemIdentifiers:(NSToolbar*)toolbar
{
	// 識別子の配列を返します
    return [NSArray arrayWithObjects: 
		JapanIdentifier, 
		USIdentifier, 
		PCTIdentifier,
		NSToolbarFlexibleSpaceItemIdentifier,
//		ActionIdentifier,
	AboutIdentifier,
		nil];
}
- (BOOL) validateToolbarItem: (NSToolbarItem *) toolbarItem
{
		return YES;

}
- (NSArray*)toolbarAllowedItemIdentifiers:(NSToolbar*)toolbar
{
	
    return [self toolbarDefaultItemIdentifiers:toolbar];
}

- (NSArray*)toolbarSelectableItemIdentifiers:(NSToolbar*)toolbar
{
    return [self toolbarDefaultItemIdentifiers:toolbar];
}

- (NSToolbarItem*)toolbar:(NSToolbar*)toolbar 
	itemForItemIdentifier:(NSString*)itemId 
willBeInsertedIntoToolbar:(BOOL)willBeInserted
{
	// NSToolbarItem を作ります
    NSToolbarItem*  toolbarItem;
    toolbarItem = [[[NSToolbarItem alloc] initWithItemIdentifier:itemId] autorelease];
	[toolbarItem setTarget:self];
	[toolbarItem setAction:@selector(_showTab:)];
    
	// NSToolbarItemを設定します
    if ([itemId isEqualToString:JapanIdentifier]) {
        [toolbarItem setLabel:@"Japan"];
        [toolbarItem setImage:[NSImage imageNamed:@"Japan"]];
        
        return toolbarItem;
    }
    if ([itemId isEqualToString:USIdentifier]) {
        [toolbarItem setLabel:@"US"];
        [toolbarItem setImage:[NSImage imageNamed:@"USA"]];
        
        return toolbarItem;
    }
	if ([itemId isEqualToString:PCTIdentifier]) {
        [toolbarItem setLabel:@"PCT"];
        [toolbarItem setImage:[NSImage imageNamed:@"PCT"]];
        
        return toolbarItem;
    }
	if ([itemId isEqualToString:AboutIdentifier]) {
        [toolbarItem setLabel:@"About"];
        [toolbarItem setImage:[NSImage imageNamed:@"About"]];
        
        return toolbarItem;
    
	
	
	}else if ([itemId isEqual: ActionIdentifier]) {
	CustomToolbarItem* toolbarItem2 = [[CustomToolbarItem alloc] initWithItemIdentifier: itemId] ; // autorelease crashes!!
	
	
	
	// Set the text label to be displayed in the toolbar and customization palette 
	[toolbarItem2 setLabel: @"Action"];
	[toolbarItem2 setPaletteLabel: @"Action"];
	
	// Set up a reasonable tooltip, and image   Note, these aren't localized, but you will likely want to localize many of the item's properties 
	[toolbarItem2 setToolTip: @"Action"];
	

	
	NSImage* iconImage = [NSImage imageNamed:@"About"];
	[toolbarItem2 setImage:iconImage  ];
	
	
	
	[[toolbarItem2 control] setImmediateTarget: self];
	[[toolbarItem2 control] setImmediateAction: @selector(showActionMenu)];
	
	
	
	return toolbarItem2;
	
}
	
    
    return nil;
}


-(void)showActionMenu
{
	
	
	NSMenu* menu = actionMenu;
	
	
	NSPoint mousePoint = [[self windowForSheet] mouseLocationOutsideOfEventStream];
	
	NSEvent* ev = [NSEvent mouseEventWithType:NSRightMouseDown location:mousePoint modifierFlags:0 timestamp:0 windowNumber:[[self windowForSheet] windowNumber] context:NULL eventNumber:0 clickCount:1 pressure:1.0];
	
	[NSMenu popUpContextMenu:menu withEvent:ev forView:textView ];
}

- (void)_showTab:(id)sender
{
	
	// タブを選択します
	// このサンプルでは、NSToolbarItemの識別子と、NSTabViewItemの識別子を、
	// 同じにしてあります
    [tabView selectTabViewItemWithIdentifier:[sender itemIdentifier]];
}


-(void)startDownloading:(id)aDownloadDelegate
{
	downloadDelegate = aDownloadDelegate;

	[self startWaitSession:@selector(startDownloading_main:) target:self];
}
	
-(void)startDownloading_main:(NSNumber*)flag
{	
	if( [flag boolValue] == NO ) 
	{
		return;
	}
	
	[self setBusy:YES];
	
	
	[self startDownloading_startSheet];
	

//	[progressBar display];
	
	if( [downloadDelegate respondsToSelector:@selector(downloadingDidStart)] )
		[downloadDelegate performSelector:@selector(downloadingDidStart)];
	
	
}
-(void)startDownloading_startSheet
{
	[progressDownloadBar setIndeterminate:YES];
	[progressDownloadBar startAnimation:nil];
	//[progressBar setUsesThreadedAnimation:YES];
	//	[progressBar displayIfNeeded];
	
	[progressCancelButton setState:NSOffState];
	
	[[NSApplication sharedApplication] beginSheet:progressSheet modalForWindow:[self windowForSheet]
									modalDelegate:NULL didEndSelector:NULL contextInfo:NULL];
	
}

-(void)endDownloading
{
	[progressDownloadBar stopAnimation:self];

	[progressSheet close];
	[[NSApplication sharedApplication] endSheet:progressSheet ];

	[self setBusy:NO];

}

-(void)setProgressBar:(double)value // minus = indeterminate, 0-100
{
	if( value < 0 )
	{
		[progressDownloadBar setIndeterminate:YES];
	}
	else
	{
		[progressDownloadBar setIndeterminate:NO];

		[progressDownloadBar setDoubleValue:value];

	}

//	[progressBar display];
	
	//NSLog(@"progress %f",value);

}
-(IBAction)progressCancelButtonClicked:(id)sender
{
	
	[self endDownloading];

	
	if( [downloadDelegate respondsToSelector:@selector(downloadingCanceled)] )
		[downloadDelegate performSelector:@selector(downloadingCanceled)];
}
-(int)cancelState
{
	return [progressCancelButton state];
}

-(void)setLoading:(BOOL)flag
{
	if( flag == YES )
		[loadingWheel startAnimation:self];
	else
		[loadingWheel stopAnimation:self];

}


@end
