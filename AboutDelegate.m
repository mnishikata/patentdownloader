//
//  AboutDelegate.m
//  PatentDownloader
//
//  Created by Masatoshi Nishikata on 06/05/15.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "AboutDelegate.h"
#import <WebKit/WebKit.h>

#import "DEFINITION.m"

#define PARENT_DOCUMENT [[[webView window] windowController] document]


@implementation AboutDelegate

-(void)openTab
{
	NSURL* url;
	
	CFLocaleRef locale = CFLocaleCopyCurrent();
	CFTypeRef string = CFLocaleGetValue(
							   locale,
							    kCFLocaleCurrencyCode
							   );

	

	//NSLog(@"language lang %@",string);
	
	
	if( [ (NSString*)string isEqualToString:@"JPY"] )
		url = [NSURL URLWithString:@"http://www.oneriver.jp/PatentDownloader/ja/index_j.html"];

		else
			url = [NSURL URLWithString:@"http://www.oneriver.jp/PatentDownloader/index_e.html"];
//http://www.oneriver.jp/PatentDownloader/index_e.html
	//http://www.oneriver.jp/PatentDownloader/ja/index_j.html
	
		
	NSURLRequest* urlRequest = [[[NSURLRequest alloc] initWithURL:url] autorelease];
	
	if( webView != NULL )
	[[webView mainFrame] loadRequest:urlRequest];
	
	
	
	if( locale != nil )
		CFRelease(locale);
}

-(void)setWebView:(WebView* )wv
{
	
	[wv setFrameLoadDelegate:self ];
	[wv setResourceLoadDelegate:self ];
	[wv setPolicyDelegate:self ];

	webView = wv;
	[webView setCustomUserAgent: USER_AGENT];
	
	loadingIndicator = [[NSProgressIndicator alloc] initWithFrame:NSMakeRect(0,0,32,32)];
	[loadingIndicator setControlSize:NSRegularControlSize];
	[loadingIndicator setStyle:NSProgressIndicatorSpinningStyle];
	
	[loadingIndicator setAutoresizingMask:NSViewMinXMargin|NSViewMaxXMargin|
		NSViewMinYMargin|NSViewMaxYMargin ];
	[webView addSubview:loadingIndicator];
	NSRect frame = [webView frame];
	NSPoint center;
	center.x = frame.size.width /2 - 16;
	center.y = frame.size.height /2 - 16;
	
	[loadingIndicator setFrameOrigin:center ];
	
}

- (void)webView:(WebView *)sender didStartProvisionalLoadForFrame:(WebFrame *)webFrame
{
	[PARENT_DOCUMENT setBusy:YES];

	
	[loadingIndicator setHidden:NO];
	[loadingIndicator startAnimation:self];
	
}

- (void)webView:(WebView *)sender didFailLoadWithError:(NSError *)error forFrame:(WebFrame *)frame
{
	/*
	NSBeginAlertSheet(@"Error", @"OK",
					  NULL, NULL, [sender window] , NULL ,
					  NULL, NULL ,NULL ,@"Internet connect error.");
	 */
	[loadingIndicator setHidden:YES];
	[loadingIndicator stopAnimation:self];
	
	[PARENT_DOCUMENT setBusy:NO];

}

- (void)webView:(WebView *)sender didFinishLoadForFrame:(WebFrame *)frame
{
	if( [sender mainFrame ] ==  frame )
	{
		[PARENT_DOCUMENT setBusy:NO];
		
		[loadingIndicator setHidden:YES];
		[loadingIndicator stopAnimation:self];

		[PARENT_DOCUMENT setBusy:NO];

	}
}

 

- (void)webView:(WebView *)sender decidePolicyForMIMEType:(NSString *)type request:(NSURLRequest *)request frame:(WebFrame *)frame decisionListener:(id<WebPolicyDecisionListener>)listener
{

	
	NSString* baseURLStr = [[request URL]  absoluteString];
	//NSLog(@"policy %@ type %@",baseURLStr,type);
	if(  [baseURLStr rangeOfString:@"/PatentDownloader/"].length != 0 ||
		 [baseURLStr rangeOfString:@"googlesyndication"].length != 0 )
	{

		[webView performFindPanelAction:self];
	}else
	{
		
		NSURL* url = [request URL];
		
		
		NSWorkspace* ws = [[NSWorkspace sharedWorkspace] openURL:url];	
		
		[listener ignore];
		
		[PARENT_DOCUMENT setBusy:NO];
		
		[loadingIndicator setHidden:YES];
		[loadingIndicator stopAnimation:self];
		
		[PARENT_DOCUMENT setBusy:NO];

	}
	
}


- (void)webView:(WebView *)sender decidePolicyForNewWindowAction:(NSDictionary *)actionInformation request:(NSURLRequest *)request newFrameName:(NSString *)frameName decisionListener:(id<WebPolicyDecisionListener>)listener
{

	NSURL* url = [request URL];
	

	NSWorkspace* ws = [[NSWorkspace sharedWorkspace] openURL:url];
	
	[PARENT_DOCUMENT setBusy:NO];
	
	[loadingIndicator setHidden:YES];
	[loadingIndicator stopAnimation:self];
	
	[PARENT_DOCUMENT setBusy:NO];
}

@end
