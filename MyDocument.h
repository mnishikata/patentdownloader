//
// MyDocument.h
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import "RBSplitView.h"

#import "USDelegate.h"
#import "AboutDelegate.h"
#import "JapaneseDelegate.h"
#import "PCTDelegate.h"
#import "CustomToolbarItem.h"


@interface MyDocument : NSDocument
{
	IBOutlet id tabView;
	
	AboutDelegate* aboutDelegate;
	
	IBOutlet id queSheet;
	IBOutlet id queCancelButton;
	
	IBOutlet id progressSheet;
	IBOutlet NSProgressIndicator* progressDownloadBar;
	IBOutlet id progressCancelButton;
	id			downloadDelegate;

	IBOutlet id saveButton;
	IBOutlet id printButton;

	IBOutlet id textView;

	IBOutlet id loadingWheel;

	
	
	IBOutlet id actionMenu;

	
	NSTimer* queTimer;
	SEL			queSelector;
	id			queTarget;
	
	
	// japan
	
	
	JapaneseDelegate* japaneseDelegate;
	
	   
	   
	
	// us
	
	USDelegate* usDelegate;
	
	
	
	// pct
	PCTDelegate* pctDelegate;
		
	// etc
	

	
	
	BOOL iAmBusy;
	NSTimer* aboutTimer;
	
}

@end
