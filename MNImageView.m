#import "MNImageView.h"
#define SKT_HANDLE_WIDTH 8
#define SKT_HALF_HANDLE_WIDTH 4

#define MINIMUM_RESIZABLE_SIZE 10

#define TIMEOUT_WHILE(n) NSDate* __timeOutDate = [NSDate dateWithTimeIntervalSinceNow: n ];	while ( [__timeOutDate compare:[NSDate date]] == NSOrderedDescending )


@implementation MNImageView
- (void)awakeFromNib
{
	

	NSBundle* myBundle;
	myBundle =[NSBundle bundleForClass:[self class]];
	NSString* path;
	
	path = [myBundle pathForResource:@"resizeArrowCorner" ofType:@"tif"];
	NSImage* reszeArrowCornerImage = [[[NSImage alloc] initWithContentsOfFile:path ] autorelease];
	reszeArrowCorner = [[NSCursor alloc] initWithImage:reszeArrowCornerImage hotSpot:NSMakePoint(8.0,8.0)];
	
	path = [myBundle pathForResource:@"resizeArrowCornerMirror" ofType:@"tif"];
	NSImage* reszeArrowCornerMirrorImage = [[[NSImage alloc] initWithContentsOfFile:path ] autorelease];
	reszeArrowCornerMirror = [[NSCursor alloc] initWithImage:reszeArrowCornerMirrorImage hotSpot:NSMakePoint(8.0,8.0)];
	
	
	path = [myBundle pathForResource:@"resizeArrowBottom" ofType:@"tif"];
	NSImage* reszeArrowBottomImage = [[[NSImage alloc] initWithContentsOfFile:path ] autorelease];
	reszeArrowBottom = [[NSCursor alloc] initWithImage:reszeArrowBottomImage hotSpot:NSMakePoint(8.0,8.0)];
	
	path = [myBundle pathForResource:@"resizeArrowRight" ofType:@"tif"];
	NSImage* reszeArrowRightImage = [[[NSImage alloc] initWithContentsOfFile:path ] autorelease];
	reszeArrowRight = [[NSCursor alloc] initWithImage:reszeArrowRightImage hotSpot:NSMakePoint(8.0,8.0)];
	
	
	
	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}

- (void)resetCursorRects
{
	
	[self discardCursorRects];
	
	
	NSRect rr = [self bounds];
	NSRect trimFrame = trimRectInImageView;

    [self addCursorRect:rr cursor:[NSCursor crosshairCursor]];
	
	
	if( NSEqualRects( trimRectInImageView , NSZeroRect)  )
	{
		
		return;
	}
	
	

	//C	
	[self addCursorRect:trimFrame cursor: [NSCursor openHandCursor] ];
	
	
	//SE
	rr = NSMakeRect(  NSMaxX(trimFrame) - SKT_HANDLE_WIDTH,
										   NSMaxY(trimFrame) - SKT_HANDLE_WIDTH,
										   SKT_HANDLE_WIDTH*2,
								SKT_HANDLE_WIDTH*2  ) ;
    [self addCursorRect:rr cursor: reszeArrowCorner ];
	

	//S
	
	rr = NSMakeRect(  trimFrame.origin.x + trimFrame.size.width/2 - SKT_HANDLE_WIDTH,
												NSMaxY(trimFrame) - SKT_HANDLE_WIDTH,
												SKT_HANDLE_WIDTH*2,
								SKT_HANDLE_WIDTH*2  ) ;
    [self addCursorRect:rr cursor: reszeArrowBottom ];

	
	//E
	
	rr = NSMakeRect(  NSMaxX(trimFrame) - SKT_HANDLE_WIDTH,
												trimFrame.origin.y +  trimFrame.size.height/2 - SKT_HANDLE_WIDTH,
												SKT_HANDLE_WIDTH *2,
								SKT_HANDLE_WIDTH *2) ;
	
    [self addCursorRect:rr cursor: reszeArrowRight ];
	
	
	///N

	
	rr = NSMakeRect ( trimFrame.origin.x +  trimFrame.size.width/2 - SKT_HANDLE_WIDTH,
												 trimFrame.origin.y - SKT_HANDLE_WIDTH,
												 SKT_HANDLE_WIDTH *2,
								SKT_HANDLE_WIDTH *2) ;
    [self addCursorRect:rr cursor: reszeArrowBottom ];
	
	
	
	//NW
	rr =  NSMakeRect( trimFrame.origin.x - SKT_HANDLE_WIDTH,
												 trimFrame.origin.y - SKT_HANDLE_WIDTH,
												 SKT_HANDLE_WIDTH *2,
								  SKT_HANDLE_WIDTH *2) ;
	[self addCursorRect:rr cursor: reszeArrowCorner ];
	

	//NE

	rr =  NSMakeRect( NSMaxX(trimFrame) - SKT_HANDLE_WIDTH,
												 trimFrame.origin.y - SKT_HANDLE_WIDTH,
												 SKT_HANDLE_WIDTH *2,
								  SKT_HANDLE_WIDTH *2) ;
	[self addCursorRect:rr cursor: reszeArrowCornerMirror ];


	//W
	
	
	rr =  NSMakeRect( trimFrame.origin.x - SKT_HANDLE_WIDTH,
												 trimFrame.origin.y +  trimFrame.size.height/2 - SKT_HANDLE_WIDTH,
												 SKT_HANDLE_WIDTH *2,
								  SKT_HANDLE_WIDTH *2) ;
[self addCursorRect:rr cursor: reszeArrowRight ];

	//SW
	rr =   NSMakeRect( trimFrame.origin.x - SKT_HANDLE_WIDTH,
												 NSMaxY(trimFrame) - SKT_HANDLE_WIDTH,
												 SKT_HANDLE_WIDTH *2,
								   SKT_HANDLE_WIDTH *2) ;
[self addCursorRect:rr cursor: reszeArrowCornerMirror ];



	
}






-(void)setAttachmentCell:(MNGraphicAttachmentCell*)aCell
{
	[cell release];
	cell = aCell;
	[cell retain];

}
- (BOOL)isFlipped
{
	return YES;
}


-(void)drawRect:(NSRect)rect
{
	
	if( cell == nil )	return;
	if( ! [cell isKindOfClass:[MNGraphicAttachmentCell class]]  ) return;
	
	
	NSRect contentRect = [[self cell] drawingRectForBounds:[self bounds]];

	NSRect imageRect = NSZeroRect;
	imageRect.size = [cell originalSize];
	
	
	[super drawRect:rect];
	
	
	
	float zoom = [self imageViewZoomFactor];
	

	//float angle = [cell angle];
	
	//setting
		[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];

	
	//affline
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: contentRect.origin.x 
					 yBy: contentRect.origin.y ]; 
	
	[affine scaleBy: zoom ];
	[affine concat];	
	
	
	
	//draw

		[[cell image] setFlipped:YES];
		[cell drawFullImageWithFrame:imageRect inView:self];

	//finish
	[context restoreGraphicsState];
	
	
	
	//draw trim frame
	NSRect trimFrame = [cell trimRect];
	NSRect fullFrame = NSZeroRect;
	fullFrame.size = [cell originalSize];
	
	
	trimRectInImageView = NSZeroRect;
	if( ! NSEqualRects( trimFrame , fullFrame) )
	{
		[NSBezierPath setDefaultLineWidth:1.0 ];
		[[NSColor blackColor] set];
		
		/*trimFrame = [self rotateRectAccordingToCell:trimFrame reverse:NO];

		if( angle == 90 || angle == 270 )
		{
			trimFrame.origin.x += (fullFrame.size.height - fullFrame.size.width)/2;
			trimFrame.origin.y += (fullFrame.size.width - fullFrame.size.height)/2;
			
		}*/
		trimFrame.size = [affine transformSize:trimFrame.size];
		trimFrame.origin = [affine transformPoint:trimFrame.origin ];

		if( iminhurry == NO )
		{
			[NSBezierPath strokeRect:trimFrame];
			
			[self drawHandlesForRect:trimFrame];
		}
		
		trimRectInImageView = trimFrame;

	}
	
	
	[self resetCursorRects];


	
}

-(NSRect)rotateRectAccordingToCell:(NSRect)aRect  reverse:(BOOL)flag
{
	NSRect originalRect = NSZeroRect;
	originalRect.size = [cell originalSize];
	
	
	float angle = [cell angle];
	if( flag == YES )
		angle = angle * -1;
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: originalRect.origin.x + originalRect.size.width/2
					 yBy: originalRect.origin.y + originalRect.size.height/2 ]; 
	
	// rotate axis
	[affine rotateByDegrees:angle];
	
	[affine translateXBy: -originalRect.origin.x  - originalRect.size.width/2
					 yBy: -originalRect.origin.y - originalRect.size.height/2 ]; 
	
	
	NSPoint p1,p2,p3,p4;
	
	p1 = [affine transformPoint:aRect.origin];
	p2 = [affine transformPoint:NSMakePoint( NSMaxX(aRect),aRect.origin.y )];
	p3 = [affine transformPoint:NSMakePoint( aRect.origin.x , NSMaxY(aRect ) )];
	p4 = [affine transformPoint:NSMakePoint( NSMaxX(aRect) , NSMaxY(aRect ) )];

	NSRect r1,r2;
	r1 = NSUnionRect( NSMakeRect(p1.x,p1.y,1,1) , NSMakeRect(p2.x,p2.y,1,1) );
	r2 = NSUnionRect( NSMakeRect(p3.x,p3.y,1,1) , NSMakeRect(p4.x,p4.y,1,1) );

	aRect = NSUnionRect(r1,r2);


	

	
	
	return aRect;
	
	

}


-(void)drawTrimFrame:(NSRect)rect
{
	
	if( cell == nil )	return;
	if( ! [cell isKindOfClass:[MNGraphicAttachmentCell class]]  ) return;
	
	
	NSRect contentRect = [[self cell] drawingRectForBounds:[self bounds]];
	
	NSRect imageRect = NSZeroRect;
	imageRect.size = [cell originalSize];
	
	
	
	
	float zoom = [self imageViewZoomFactor];
	
	
	//float angle = [cell angle];
	
	//setting
	if( iminhurry == YES )
		[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationNone];
	else
		[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
	
	
	//affline
	NSGraphicsContext *context = [NSGraphicsContext currentContext];
	[context saveGraphicsState];
	
	
	NSAffineTransform* affine = [NSAffineTransform transform];
	// move axis
	[affine translateXBy: contentRect.origin.x 
					 yBy: contentRect.origin.y ]; 
	
	[affine scaleBy: zoom ];

	
	
	
	
	//draw trim frame
	NSRect trimFrame = rect;
	NSRect fullFrame = NSZeroRect;
	fullFrame.size = [cell originalSize];
	
	
	trimRectInImageView = NSZeroRect;
	if( ! NSEqualRects( trimFrame , fullFrame) )
	{
		[NSBezierPath setDefaultLineWidth:1.0 ];
		[[NSColor blackColor] set];
		
		/*trimFrame = [self rotateRectAccordingToCell:trimFrame reverse:NO];
		
		if( angle == 90 || angle == 270 )
		{
			trimFrame.origin.x += (fullFrame.size.height - fullFrame.size.width)/2;
			trimFrame.origin.y += (fullFrame.size.width - fullFrame.size.height)/2;
			
		}*/
		trimFrame.size = [affine transformSize:trimFrame.size];
		trimFrame.origin = [affine transformPoint:trimFrame.origin ];
		
		[NSBezierPath strokeRect:trimFrame];
		
		//
		[self drawHandlesForRect:trimFrame];
		
		trimRectInImageView = trimFrame;
		
	}
	
	
	[self resetCursorRects];

	
}



-(NSRect)cellRectFromImageViewRect:(NSRect)aRect
{
	
	if( cell == nil )	return NSZeroRect;
	if( ! [cell isKindOfClass:[MNGraphicAttachmentCell class]]  ) return NSZeroRect;
	
	
	//
	NSRect contentRect = [[self cell] drawingRectForBounds:[self bounds]];
	

	
	
	float zoom = [self imageViewZoomFactor];
	
	aRect.origin.x -= contentRect.origin.x;
	aRect.origin.y -= contentRect.origin.y;
	
	aRect.origin.x = aRect.origin.x / zoom;
	aRect.origin.y = aRect.origin.y / zoom;
	
	aRect.size.width = aRect.size.width / zoom;
	aRect.size.height = aRect.size.height / zoom;

	/*
	
	if( [cell angle] == 90 || [cell angle] == 270 )
	{
		float dd;
		dd = aRect.origin.y;
		aRect.origin.y = aRect.origin.x;
		aRect.origin.x = dd;
		
		
		dd = aRect.size.height;
		aRect.size.height = aRect.size.width;
		aRect.size.width = dd;
	
	
	}
	 */
	return aRect;
	
}



- (void)mouseDown:(NSEvent *)theEvent
{
	if( cell == nil )
		[super mouseDown:theEvent];

	else
		[self trackMouse];
}



///////

- (void)drawHandleAtPoint:(NSPoint)point inColor:(NSColor*)color fill:(BOOL)flag
{
    NSRect handleRect;
 
	
    handleRect.origin.x = point.x - SKT_HALF_HANDLE_WIDTH  + 1.0;
    handleRect.origin.y = point.y - SKT_HALF_HANDLE_WIDTH  + 1.0;
    handleRect.size.width = SKT_HANDLE_WIDTH  - 1.0;
    handleRect.size.height = SKT_HANDLE_WIDTH - 1.0;
	
	
	[NSBezierPath setDefaultLineWidth:1.0 ];

	
	[[NSColor whiteColor] set];
	[NSBezierPath fillRect:handleRect];	

		
	[color set];
	[NSBezierPath strokeRect:handleRect];
	
	
}

- (void)drawHandlesForRect:(NSRect)rect
{

	

	NSColor* blackColor = [NSColor blackColor];
	
	[self drawHandleAtPoint:rect.origin
					inColor:blackColor fill:YES];
	[self drawHandleAtPoint:NSMakePoint( rect.origin.x + rect.size.width/2, rect.origin.y)
					inColor:blackColor fill:YES];
	
	[self drawHandleAtPoint:NSMakePoint( rect.origin.x, rect.origin.y +  rect.size.height/2 )
					inColor:blackColor fill:YES];
	
	[self drawHandleAtPoint:NSMakePoint( rect.origin.x + rect.size.width, rect.origin.y)
					inColor:blackColor  fill:YES];
	
	[self drawHandleAtPoint:NSMakePoint( rect.origin.x, rect.origin.y +  rect.size.height )
					inColor:blackColor fill:YES];
	//
	
	
	[self drawHandleAtPoint:NSMakePoint(NSMaxX(rect), NSMaxY(rect))
					inColor:blackColor fill:YES];
	[self drawHandleAtPoint:NSMakePoint( rect.origin.x + rect.size.width/2, NSMaxY(rect))
					inColor:blackColor  fill:YES];
	
	[self drawHandleAtPoint:NSMakePoint(NSMaxX(rect), rect.origin.y +  rect.size.height/2 )
					inColor:blackColor  fill:YES];
	
	//[self setDefaultCursorRect];
	[self resetCursorRects];
	
}


-(DRAGMODE)cursorOnKnob
{
	NSRect trimFrame = trimRectInImageView;

	
	if(  NSEqualRects( trimFrame , NSZeroRect) ) return 0;
		
	
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];
	mouseLoc = [self convertPoint:mouseLoc fromView:[[self window] contentView]];

	
	//NSLog(@"rect %@",NSStringFromRect(trimFrame));
	//NSLog(@"point %@",NSStringFromPoint(mouseLoc));

	if( NSPointInRect(mouseLoc, NSMakeRect(NSMaxX(trimFrame) - SKT_HANDLE_WIDTH,
										   NSMaxY(trimFrame) - SKT_HANDLE_WIDTH,
										   SKT_HANDLE_WIDTH*2,
										   SKT_HANDLE_WIDTH*2  ) ) )
		return kSE;
	
	else if( NSPointInRect(mouseLoc, NSMakeRect(trimFrame.origin.x + trimFrame.size.width/2 - SKT_HANDLE_WIDTH,
										   NSMaxY(trimFrame) - SKT_HANDLE_WIDTH,
										   SKT_HANDLE_WIDTH*2,
										   SKT_HANDLE_WIDTH*2  ) ) )
		return kS;
	
	else if( NSPointInRect(mouseLoc, NSMakeRect(NSMaxX(trimFrame) - SKT_HANDLE_WIDTH,
										   trimFrame.origin.y +  trimFrame.size.height/2 - SKT_HANDLE_WIDTH,
										   SKT_HANDLE_WIDTH *2,
										   SKT_HANDLE_WIDTH *2) ) )
		return  kE;
	
	else if( NSPointInRect(mouseLoc, NSMakeRect( trimFrame.origin.x +  trimFrame.size.width/2 - SKT_HANDLE_WIDTH,
										   trimFrame.origin.y - SKT_HANDLE_WIDTH,
										   SKT_HANDLE_WIDTH *2,
										   SKT_HANDLE_WIDTH *2) ) )
		return  kN;
	
	
	else if( NSPointInRect(mouseLoc, NSMakeRect( trimFrame.origin.x - SKT_HANDLE_WIDTH,
											trimFrame.origin.y - SKT_HANDLE_WIDTH,
											SKT_HANDLE_WIDTH *2,
											SKT_HANDLE_WIDTH *2) ) )
		return  kNW;
	
	else if( NSPointInRect(mouseLoc, NSMakeRect( NSMaxX(trimFrame) - SKT_HANDLE_WIDTH,
											trimFrame.origin.y - SKT_HANDLE_WIDTH,
											SKT_HANDLE_WIDTH *2,
											SKT_HANDLE_WIDTH *2) ) )
		return  kNE;
	
	
	else if( NSPointInRect(mouseLoc, NSMakeRect( trimFrame.origin.x - SKT_HANDLE_WIDTH,
											 trimFrame.origin.y +  trimFrame.size.height/2 - SKT_HANDLE_WIDTH,
											SKT_HANDLE_WIDTH *2,
											SKT_HANDLE_WIDTH *2) ) )
		return  kW;
	
	else if( NSPointInRect(mouseLoc, NSMakeRect( trimFrame.origin.x - SKT_HANDLE_WIDTH,
											 NSMaxY(trimFrame) - SKT_HANDLE_WIDTH,
											SKT_HANDLE_WIDTH *2,
											SKT_HANDLE_WIDTH *2) ) )
		return  kSW;
	
	else if( NSPointInRect(mouseLoc, trimFrame ) )
		return  kC;
	
	return NoKnob;
	
	
}


-(void)trackMouse:(DRAGMODE)mode
{
	iminhurry = YES;
	
	NSEvent* theEvent;
	
	NSPoint originalMouseLoc = [[self window] mouseLocationOutsideOfEventStream];
	
	NSRect originalRect = NSZeroRect;
	originalRect.size = [cell originalSize];

	
	[self lockFocus];
	[self drawRect:[self bounds]];
	[self unlockFocus];
	[[self window] cacheImageInRect:[self frame]];
	
	
	float angle = [cell angle];
	NSRect fullFrame = NSZeroRect;
	fullFrame.size = [cell originalSize];
	
	NSRect oldTrimRect = trimRectInImageView;
	

	//NSRect clipBounds = [[self superview] bounds];//NSLog(@"bounds %@",NSStringFromRect(clipBounds));
	

	if( mode == kC )
	{
		[[NSCursor closedHandCursor] set];
	}
	
	NSRect newRect = NSZeroRect;
	TIMEOUT_WHILE (30)
	{
		//NSLog(@"%d",mode);

		
		
        theEvent = [[self window] 
            nextEventMatchingMask:NSAnyEventMask ];
		
		NSPoint currentMouseLoc = [[self window] mouseLocationOutsideOfEventStream];
		
		
		float difference_width = currentMouseLoc.x - originalMouseLoc.x;
		float difference_height = - currentMouseLoc.y + originalMouseLoc.y;
		

		
		
		//calc new size
		newRect = NSZeroRect;
		
		if( mode == kC )
		{

			newRect.origin = 
			NSMakePoint( oldTrimRect.origin.x + difference_width, oldTrimRect.origin.y + difference_height);
			
			newRect.size = oldTrimRect.size;
			
		}else if( mode == kSE )
		{
			newRect.origin = oldTrimRect.origin;
			
			newRect.size =
			NSMakeSize( oldTrimRect.size.width + difference_width, oldTrimRect.size.height + difference_height);
			
		}else if( mode == kE )
		{
			newRect.origin = oldTrimRect.origin;
			
			newRect.size =
				NSMakeSize( oldTrimRect.size.width + difference_width, oldTrimRect.size.height );
			
		}else if( mode == kNE )
		{
			newRect.origin =
			NSMakePoint( oldTrimRect.origin.x , oldTrimRect.origin.y + difference_height);
			
			
			newRect.size =
				NSMakeSize( oldTrimRect.size.width + difference_width,  oldTrimRect.size.height - difference_height);
			
		}else if( mode == kN )
		{
			newRect.origin =
			NSMakePoint( oldTrimRect.origin.x , oldTrimRect.origin.y + difference_height);
			
			
			newRect.size =
				NSMakeSize( oldTrimRect.size.width ,  oldTrimRect.size.height - difference_height);
			
		}else if( mode == kNW )
		{
			newRect.origin =
			NSMakePoint( oldTrimRect.origin.x+ difference_width , oldTrimRect.origin.y + difference_height);
			
			
			newRect.size =
				NSMakeSize( oldTrimRect.size.width - difference_width,  oldTrimRect.size.height - difference_height);
			
		}else if( mode == kW )
		{
			newRect.origin =
			NSMakePoint( oldTrimRect.origin.x+ difference_width , oldTrimRect.origin.y );
			
			
			newRect.size =
				NSMakeSize( oldTrimRect.size.width - difference_width,  oldTrimRect.size.height);
			
		}else if( mode == kSW )
		{
			newRect.origin =
			NSMakePoint( oldTrimRect.origin.x+ difference_width , oldTrimRect.origin.y );
			
			
			newRect.size =
				NSMakeSize( oldTrimRect.size.width - difference_width,  oldTrimRect.size.height +  difference_height);
			
		}else if( mode == kS )
		{
			newRect.origin =oldTrimRect.origin ;
					
			newRect.size =
				NSMakeSize( oldTrimRect.size.width ,  oldTrimRect.size.height +  difference_height);
			
		}
		
		
		
		
	//	newRect = [self convertRect:newRect fromView:[[self window] contentView]];
		newRect = [self cellRectFromImageViewRect:newRect];
		
		
		
		////////////// Resize image. ////////////////////

		
		
			
			
		
		//
		
		
		
		[[self window] restoreCachedImage];
		//[[self window] flushWindow];

		//[[self window] cacheImageInRect:[self frame]];
		
		[self lockFocus];
		[self drawTrimFrame:newRect];
		[self unlockFocus];
		[[self window] flushWindow];
		
		
		
		
		if ([theEvent type] == NSLeftMouseUp)
		{		
			//NSLog(@"draggingFinished");
			
			// set cursor
			

			
            break;
		}
		
	}
	[cell setTrimRect:newRect];

	if( mode == kC )
	{
		[[NSCursor openHandCursor] set];
	}
	
	
	
	iminhurry = NO;
	

	
	[self lockFocus];
	[self drawRect:[self bounds]];
	[self unlockFocus];
	[[self window] cacheImageInRect:[self frame]];
	[[self window] flushWindow];
	
	[[[self window] delegate] refresh];
}

 

-(float)imageViewZoomFactor
{
	
	if( cell == nil )	return 0.0;
	if( ! [cell isKindOfClass:[MNGraphicAttachmentCell class]]  ) return 0.0;
	
	
	//
	NSRect contentRect = [[self cell] drawingRectForBounds:[self bounds]];
	
	NSRect imageRect = NSZeroRect;
	imageRect.size = [cell originalSize];
	
	
	
	float zoom = contentRect.size.width /  imageRect.size.width;
	zoom = (  ( contentRect.size.height /  imageRect.size.height )< zoom ?
			  ( contentRect.size.height /  imageRect.size.height ) : zoom );
	
	return zoom;
}


-(void)trackMouse
{

	int mode = [self cursorOnKnob];
	//NSLog(@"%d",mode);
	if( mode != 0 )
	{
		[self trackMouse:mode];
		return;
		
	}
	
	iminhurry = YES;

	
	NSEvent* theEvent;
	
	NSPoint originalMouseLoc = [[self window] mouseLocationOutsideOfEventStream];
	
	
	[self lockFocus];
	[self drawRect:[self bounds]];
	[self unlockFocus];
	[[self window] cacheImageInRect:[self frame]];
	
	
	float angle = [cell angle];
	NSRect fullFrame = NSZeroRect;
	fullFrame.size = [cell originalSize];
	
	
	//NSRect clipBounds = [[self superview] bounds]; //NSLog(@"bounds %@",NSStringFromRect(clipBounds));
	NSRect newTrimRect;
	
	TIMEOUT_WHILE (30)
	{
        theEvent = [[self window] 
            nextEventMatchingMask:NSAnyEventMask];
		
		NSPoint currentMouseLoc = [[self window] mouseLocationOutsideOfEventStream];
		
		
		
		float difference_width = currentMouseLoc.x - originalMouseLoc.x;
		float difference_height =  currentMouseLoc.y - originalMouseLoc.y;
		float xx = originalMouseLoc.x;
		float yy = originalMouseLoc.y;
		
		
		if( difference_width < 0 )
		{
			xx += difference_width;
			difference_width = 0-difference_width;
		}
		if( difference_height < 0 )
		{
			yy += difference_height;
			difference_height = 0-difference_height;
		}
		
		
		
		newTrimRect = NSMakeRect( xx, yy, difference_width,difference_height);
		
		newTrimRect = [self convertRect:newTrimRect fromView:[[self window] contentView]];
		newTrimRect = [self cellRectFromImageViewRect:newTrimRect];
		//newTrimRect = [self rotateRectAccordingToCell:newTrimRect reverse:YES];
		

		

		
		
		
		
		[[self window] restoreCachedImage];
	//	[[self window] cacheImageInRect:[self frame]];
		
		[self lockFocus];
		[self drawTrimFrame: newTrimRect ];
		[self unlockFocus];
		[[self window] flushWindow];
		
		
		if ([theEvent type] == NSLeftMouseUp)
		{		
			//NSLog(@"draggingFinished");
			
			// set cursor
			
			
            break;
		}
		
		
	}
	[cell setTrimRect:newTrimRect];


	iminhurry = NO;

	[self lockFocus];
	[self drawRect:[self bounds]];
	[self unlockFocus];
	[[self window] cacheImageInRect:[self frame]];
	[[self window] flushWindow];

	[[[self window] delegate] refresh];
}

@end
