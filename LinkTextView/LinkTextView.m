#import "LinkTextView.h"
#import "NDAlias.h"


#define AVAILABLE_TYPES [NSArray arrayWithObjects: NSRTFDPboardType , NSRTFPboardType, NSStringPboardType, @"Apple URL pasteboard type", @"WebURLsWithTitlesPboardType",NSFilenamesPboardType, nil]


@implementation LinkTextView


- (void)clickedOnLink:(id)link atIndex:(unsigned)charIndex
{
    if ([link isKindOfClass: [NDAlias class]])
    {
		[[NSWorkspace sharedWorkspace] openFile:[link path]];
			
			/*
        NSRunAlertPanel (@"Whee!",
						 [NSString stringWithFormat: @"link to ... %@", [link path]],
						 nil, nil, nil);
			 */
        return YES;
    }
	
	[super clickedOnLink:link atIndex:charIndex];
}

-(void)awakeFromNib
{
	droppingInsertionPoint = 0;
	

	
	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender
{
	[[self window] cacheImageInRect:[[self window] frame]];

	
    NSPasteboard *pboard = [sender draggingPasteboard];
	if (self == [sender draggingSource]) //from self
		return NSDragOperationGeneric;
	
	else //from other
	{

		if([[pboard types] containsObject:NSFilenamesPboardType]  )
		{
			return NSDragOperationLink | NSDragOperationCopy | NSDragOperationMove;
			
		}
		else
			return NSDragOperationCopy;
	}	
}
- (void)draggingExited:(id <NSDraggingInfo>)sender
{
	[[self window] restoreCachedImage];
	[[self window] flushWindow];
}



- (NSDragOperation)draggingUpdated:(id <NSDraggingInfo>)sender
{
	//[super draggingUpdated:sender];

	NSPoint aPoint = [[self window] mouseLocationOutsideOfEventStream]; //point
	unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
	BOOL needToBeUpdated = YES;
	

	if( droppingInsertionPoint == insertionCharIndex ) needToBeUpdated = NO;
	
	droppingInsertionPoint = insertionCharIndex;
	
	if( needToBeUpdated == YES )
	{
		[[self window] restoreCachedImage];
		[[self window] flushWindow];	
		[[self window] display];
		[[self window] cacheImageInRect:[[self window] frame]];
	}
	
    NSPasteboard *pboard = [sender draggingPasteboard];
	if (self == [sender draggingSource]) //from self
	{
		NSEvent* theEvent = [[self window] currentEvent];
		
		if( needToBeUpdated == YES )
		[self drawInsertionPoint];
			
		if( [theEvent modifierFlags] == 524576)
			return NSDragOperationCopy;
		else return NSDragOperationMove;
	}
	else //from other
	{
		if([[pboard types] containsObject:NSFilenamesPboardType]  )
		{
			NSRange linkRange;
			NDAliasToBeDropped = [self mouseOnNDAlias:&linkRange];
			
			if( ! [[NSFileManager defaultManager] isExecutableFileAtPath:[NDAliasToBeDropped path]] )
				NDAliasToBeDropped = NULL;
				
			if( NDAliasToBeDropped != NULL ) 
				//dropping file
			{
				if( needToBeUpdated == YES )
				{
					NSRange linkGlyphRange = [[self layoutManager] glyphRangeForCharacterRange:linkRange actualCharacterRange:NULL];
					NSRect rect = [[self layoutManager] boundingRectForGlyphRange:linkGlyphRange inTextContainer:[self textContainer]];
					
					rect = [self convertRect:rect toView:[[self window] contentView]];
					
					[[[self window] contentView] lockFocus];
					[[NSColor redColor] set];
					[NSBezierPath setDefaultLineWidth:3.0];
					[NSBezierPath strokeRect:rect];
					[[[self window] contentView] unlockFocus];
					
					[[self window] flushWindow];
				}
				
				
				NSEvent* theEvent = [[self window] currentEvent];
				
				//NSLog(@"key %d",[theEvent modifierFlags]);
				
				if( [theEvent modifierFlags] == 524576)
					return NSDragOperationCopy;
				
				else 	if( [theEvent modifierFlags] == 1573160 )
					return NSDragOperationLink;

				else return NSDragOperationMove;
				
			}else
			{
				if( needToBeUpdated == YES )
					[self drawInsertionPoint];

			}
			
			return NSDragOperationLink;
		}
		else
		{
			if( needToBeUpdated == YES )
			[self drawInsertionPoint];
			return NSDragOperationCopy;
		}
	}
	
}

-(void)drawInsertionPoint
{
	
	NSPoint aPoint = [[self window] mouseLocationOutsideOfEventStream]; //point
	unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
	if( insertionCharIndex == NSNotFound ) return;
	

	NSRange glyphRange = [[self layoutManager] 
	glyphRangeForCharacterRange:NSMakeRange(insertionCharIndex,1) actualCharacterRange:NULL ];
	
	NSRect rect = [[self layoutManager] boundingRectForGlyphRange:glyphRange inTextContainer:[self textContainer]];
	
	rect = [self convertRect:rect toView:[[self window]contentView]];
	
	rect.size.width = 1;
	
	[[[self window] contentView] lockFocus];
	[[NSColor blackColor] set];
	[NSBezierPath setDefaultLineWidth:2.0];
	[NSBezierPath strokeRect:rect];
	[[[self window] contentView] unlockFocus];
	[[self window] flushWindow];	

}



-(NDAlias*)mouseOnNDAlias:(NSRange*)rangePointer
{
	NSPoint mouseLoc = [[self window] mouseLocationOutsideOfEventStream];  
	unsigned charIndex = [self charIndexAtPoint:mouseLoc];
	
	if( charIndex == NSNotFound || [self fullRange].length-1 < charIndex ) return NULL;
	
	id someLink = [[self textStorage] attribute:NSLinkAttributeName atIndex:charIndex longestEffectiveRange:rangePointer inRange:[self fullRange]];
	
	if ([someLink isKindOfClass: [NDAlias class]])
		return someLink;
	else return NULL;
	
}



- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
			
    NSPasteboard *pboard = [sender draggingPasteboard];
	BOOL returnStatus = NO;
	//NSLog( @"types %@",[[pboard types] description] );
	//NSLog(@"Drag style %d",[sender draggingSourceOperationMask]);
	
	if( [sender draggingSource] == self && 
		[self selectedRange].length != 0 && 
		! ( [sender draggingSourceOperationMask] & NSDragOperationCopy ) )
		[self insertText:@""];
	
	if([[pboard types] containsObject:@"WebURLsWithTitlesPboardType"])  // safai  url
	{
		
		/*Structure
		(
		 (
		  "http://xtramail.xtra.co.nz/", 
		  "http://www.powerlogix.com/products2/bcg4pismo/index.html"
		  ), 
		 (xtra, "PowerLogix Products - BlueChip G4 \"Pismo\"")
		 )
		
		*/
		NSArray* pbArray = [pboard propertyListForType:@"WebURLsWithTitlesPboardType"]; 
		
		//get insertion point
		NSPoint aPoint = [sender draggingLocation]; //point
		unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
		if( insertionCharIndex == NSNotFound ) insertionCharIndex = 0;

		//set insertion point
		[self setSelectedRange:NSMakeRange(insertionCharIndex,0)];
		
		int hoge;
		for(hoge = 0; hoge < [[pbArray objectAtIndex:0] count]; hoge++)
		{
			NSString* path = [[pbArray objectAtIndex:0] objectAtIndex:hoge];
			NSURL* aURL = [NSURL URLWithString:path];
			NSString* URLtitle =  [[pbArray objectAtIndex:1] objectAtIndex:hoge];
			
			
			if(hoge != 0)
			{
				NSAttributedString* delimiter = [[NSAttributedString alloc] initWithString:@"\n" attributes:NULL];
				[self pasteText:delimiter];
				[delimiter release];
			}
			
			
			//
			if([URLtitle isEqualToString:@""])  URLtitle = [[pbArray objectAtIndex:0] objectAtIndex:hoge];
			
			NSMutableDictionary* aLinkAttr;
			aLinkAttr = [NSMutableDictionary dictionaryWithObject: aURL
														   forKey: NSLinkAttributeName];
			[aLinkAttr setObject: [NSColor blueColor]  forKey: NSForegroundColorAttributeName];
			[aLinkAttr setObject: [NSNumber numberWithBool: YES]  forKey: NSUnderlineStyleAttributeName];
			[aLinkAttr setObject: path  forKey: NSToolTipAttributeName];
			[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];

			
			NSAttributedString* str = [[NSAttributedString alloc] initWithString:URLtitle attributes:aLinkAttr];
			
			[self pasteText:str];
			[str release];
			
		}
		
		returnStatus = YES;	
		
		
	}else if([[pboard types] containsObject:NSFilenamesPboardType] )  // file
	{
		//NSFilenamesPboardType
		NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
		
		//Dropping on NDAlias
		if( NDAliasToBeDropped != NULL )
		{
			BOOL flag;
			NSString* targetPath = [NDAliasToBeDropped path];
			NSFileManager* fm =  [NSFileManager defaultManager] ;

			// drop on folder
			if( [fm fileExistsAtPath:targetPath isDirectory:&flag ] )
			{
				
				if( flag == YES )
				{
					NSString* op;
					if( [sender draggingSourceOperationMask] == 5  ) 
					{
						//NSLog(@"making alias");

						op = NSWorkspaceLinkOperation;
					}
					else if( [sender draggingSourceOperationMask] == 55 ) 
					{						//NSLog(@"move");

						op = NSWorkspaceMoveOperation;
					}else if( [sender draggingSourceOperationMask] == 1 ) 
					{
						//NSLog(@"copy");

						op = NSWorkspaceCopyOperation;
					}
					
					
					else op = NSWorkspaceMoveOperation;
					
					unsigned piyo;
					for( piyo = 0; piyo < [files count]; piyo ++ )
					{
						NSString* fullPath = [files objectAtIndex:piyo];
						
						NSString* filename = [fullPath lastPathComponent];
						NSString* folderForFile = [fullPath folderPath];
						
						
						if( op == NSWorkspaceLinkOperation )
						{
							//NSLog(@"making alias");
							
							NDAlias* anAlias = [NDAlias aliasWithContentsOfFile:fullPath]; //assume finder alias
							if( anAlias == NULL )//if not,
								anAlias = [NDAlias aliasWithPath:fullPath];
							
							NSString* newFilename = [targetPath stringByAppendingPathComponent:filename ];
							[anAlias writeToFile:[newFilename uniquePathForFolder] ];
							
									
						}else
						{
			
								BOOL successMove = [[NSWorkspace sharedWorkspace] 
			performFileOperation:op 
						  source:folderForFile
					 destination:targetPath
						   files:[NSArray arrayWithObject:filename ]
							 tag:NULL];
							if( successMove == NO )
							NSRunAlertPanel (@"Error",
			 [NSString stringWithFormat: @"%@ can not be copied or moved", filename],
											 nil, nil, nil);

						}
						

					}
					SystemSoundPlay(1);
					
					
					//finish action
					NDAliasToBeDropped = NULL;
					[[self window] restoreCachedImage];
					[[self window] flushWindow];
					return YES;
					
				}
			}
				  
			if( [fm isExecutableFileAtPath:targetPath] )
				// targetPath == Application
			{			
				unsigned piyo;
				for( piyo = 0; piyo < [files count]; piyo ++ )
				{
					[[NSWorkspace sharedWorkspace] openFile:[files objectAtIndex:piyo] withApplication:targetPath];
				}
				
				
			} 
				
				

			NDAliasToBeDropped = NULL;
			[[self window] restoreCachedImage];
			[[self window] flushWindow];
			return YES;
		}
		
		
		
		
		//get insertion point
		NSPoint aPoint = [sender draggingLocation]; //point
		unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
		if( insertionCharIndex == NSNotFound ) insertionCharIndex = 0;

		//set insertion point
		[self setSelectedRange:NSMakeRange(insertionCharIndex,0)];
		
		int hoge;
		for(hoge = 0; hoge < [files count]; hoge++)
		{
			
			NSString* path =  [files objectAtIndex:hoge];
			NSURL* aURL = [NSURL fileURLWithPath:path];	
			NSArray *pathArray = [path componentsSeparatedByString:@"/"];
			NSString* filename = [pathArray lastObject];
			pathArray = [path componentsSeparatedByString:@"."];
			NSString* extension = [pathArray lastObject];
			

			NSData* aData;
			NSAttributedString* anAttrStr = [[NSAttributedString alloc] initWithString:filename];			
			aData = [anAttrStr RTFFromRange:NSMakeRange(0,[anAttrStr length]) documentAttributes:NULL];
			[anAttrStr release];
			anAttrStr = [[NSAttributedString alloc] initWithRTF:aData documentAttributes:NULL];
			filename = [NSString stringWithString:[anAttrStr string]];
			[anAttrStr release];
			
			
			
			if(hoge != 0)
			{
				NSAttributedString* delimiter = [[NSAttributedString alloc] initWithString:@"\n" attributes:NULL];
				[self pasteText:delimiter];
				[delimiter release];
				insertionCharIndex++;
			}
			
			/*
			if( ([sender draggingSourceOperationMask] == NSDragOperationLink && [[pboard types] containsObject:NSStringPboardType]) ||
				  ! [[pboard types] containsObject:NSStringPboardType] )
			 */
			if( [sender draggingSourceOperationMask] & NSDragOperationGeneric )
			{
				
				NSData* _pictureData = [[[NSData alloc] initWithContentsOfFile:path] autorelease];
				NSImage* pictureImage = [[[NSImage alloc] initWithData:_pictureData] autorelease];
				
				if( pictureImage == NULL )
				{

					//create alias
					NDAlias* anAlias = [NDAlias aliasWithContentsOfFile:path]; //assume finder alias
					if( anAlias == NULL )//if not,
						anAlias = [NDAlias aliasWithPath:path];
					//NSLog(@"path %@", [anAlias path]);
					
					//create icon
					NSImage* iconImage = [[NSWorkspace sharedWorkspace] iconForFile:[anAlias path]];
					NSData* aData = [iconImage TIFFRepresentation];
					
					NSFileWrapper* aWrapper = [[NSFileWrapper alloc] initRegularFileWithContents:aData];
					[aWrapper setFilename:[NSString stringWithFormat:@"%@.tiff", filename]];
					NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:aWrapper] autorelease];
					[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
					NSAttributedString* aStr = [NSAttributedString attributedStringWithAttachment:anAttachment];
					[aWrapper release];
					
					//paste icon
					[self insertText:aStr];
					//insert filename 		
					[self pasteText:filename];
					
					//create link
					NSMutableDictionary* aLinkAttr;
					aLinkAttr = [NSMutableDictionary dictionaryWithObject: anAlias
																   forKey: NSLinkAttributeName];
					[aLinkAttr setObject: [NSColor blueColor]  forKey: NSForegroundColorAttributeName];
					[aLinkAttr setObject: [NSNumber numberWithBool: YES]  forKey: NSUnderlineStyleAttributeName];
					[aLinkAttr setObject: path  forKey: NSToolTipAttributeName];
					[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];
					
					[[self textStorage] addAttributes:aLinkAttr range:NSMakeRange(insertionCharIndex , [filename length]+1)];
					insertionCharIndex = insertionCharIndex + [filename length]+1;
					
					
				}else //dragging image
				{ 					
					NSData* aData = [pictureImage TIFFRepresentation];
					
					NSFileWrapper* aWrapper = [[NSFileWrapper alloc] initRegularFileWithContents:aData];
					[aWrapper setFilename:[NSString stringWithFormat:@"%@.tiff", filename]];
					NSTextAttachment* anAttachment = [[[NSTextAttachment alloc] initWithFileWrapper:aWrapper] autorelease];
					[[anAttachment fileWrapper] setPreferredFilename:[[anAttachment fileWrapper] filename]];//self rename
					NSAttributedString* aStr = [NSAttributedString attributedStringWithAttachment:anAttachment];
					[aWrapper release];
					
					//paste icon
					[self insertText:aStr];
				}
				
				
			}else
			{ //in case of text clipping, or  not link
				
				[super readSelectionFromPasteboard:[sender draggingPasteboard]];
				insertionCharIndex = insertionCharIndex + [filename length];
			}
		}
		
		returnStatus = YES;	
	}else if([[pboard types] containsObject:@"Apple URL pasteboard type"]) // basic url
	{
		NSURL* aURL = [NSURL URLFromPasteboard:pboard];
		
		//get insertion point
		NSPoint aPoint = [sender draggingLocation]; //point
		unsigned insertionCharIndex = [self charIndexAtPoint:aPoint];
		if( insertionCharIndex == NSNotFound ) insertionCharIndex = 0;

		//set insertion point
		[self setSelectedRange:NSMakeRange(insertionCharIndex,0)];
		
		NSMutableDictionary* aLinkAttr;
		aLinkAttr = [NSMutableDictionary dictionaryWithObject: aURL
													   forKey: NSLinkAttributeName];
		[aLinkAttr setObject: [NSColor blueColor]  forKey: NSForegroundColorAttributeName];
        [aLinkAttr setObject: [NSNumber numberWithBool: YES]  forKey: NSUnderlineStyleAttributeName];
		[aLinkAttr setObject: [aURL absoluteString]  forKey: NSToolTipAttributeName];
		[aLinkAttr setObject:[NSCursor pointingHandCursor] forKey: NSCursorAttributeName];

		
		NSAttributedString* str = [[NSAttributedString alloc] initWithString:[aURL absoluteString] attributes:aLinkAttr];
		//NSLog(@"path", [str string]);
		
		[self pasteText:str];
		[str release];
		
		// debug
		str = [[NSAttributedString alloc] initWithString:[aURL resourceSpecifier]];
		//NSLog(@"specifier", [str string]);
		[str release];
		
		returnStatus = YES;
	}else
	{
		NSRange rangeToBeDelete = {0,0};
		//delete original
		if( [sender draggingSource] == self )
		{
			// delete original if necessary.
			if([sender draggingSourceOperationMask] & NSDragOperationGeneric) // delete original
			{
				
				rangeToBeDelete = [self selectedRange];
				
			}		
			
		}
		
		
		
		
		NSPoint aPoint = [sender draggingLocation]; //point
		NSRange insertedRange = NSMakeRange([self charIndexAtPoint:aPoint],0);
		
		if( insertedRange.location == NSNotFound ) insertedRange.location = 0;
		
		// do nothing if dropped onto originally selected range
		if( ( rangeToBeDelete.location <= insertedRange.location )
			&& ( insertedRange.location <= NSMaxRange(rangeToBeDelete) ) )
		{
			return NO;
		}

		[self setSelectedRange:insertedRange];

		
		unsigned _oldLength = [[self textStorage] length];
		returnStatus = [self readSelectionFromPasteboard:[sender draggingPasteboard]];
		unsigned _newLength = [[self textStorage] length];
		unsigned changeInLength = _newLength - _oldLength;
		
		insertedRange.length = changeInLength;
		
		if( rangeToBeDelete.location < insertedRange.location )
		{
			insertedRange.location -= rangeToBeDelete.length;
		}else
		{
			rangeToBeDelete.location += insertedRange.length;
		}
		[self setSelectedRange:rangeToBeDelete];
		[self pasteText:@""];
		
		
		[self setSelectedRange:insertedRange];
		
		
	}	
	
	

	
	
	return returnStatus;
	
}




@end
