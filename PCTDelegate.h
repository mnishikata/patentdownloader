//
//  PCTDelegate.h
//  PatentDownloader
//
//  Created by Masatoshi Nishikata on 06/05/25.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import <OgreKit/OgreKit.h>
#import "DownloadDelegate.h"
#import "MNZip.h"

@interface PCTDelegate : NSObject
{
	IBOutlet id view;
	IBOutlet id webView;
	IBOutlet id textField;

	IBOutlet id downloadOption;

	
	NSMutableString* downloadedHTMLString;
	
	NSMutableAttributedString* downloadedAttributedString;
		
	DownloadDelegate* downloadDelegate;
	
	NSMutableArray* documentsToBeDownloaded;
	
		
	BOOL autoPilot;
}
@end
