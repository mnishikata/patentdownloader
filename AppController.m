#import "AppController.h"
#import <WebKit/WebKit.h>



NSString* emptyOrValue(id something)
{
	return (something == NULL ? @"" : something );
}

BOOL PREF_printPageNumber()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return [ud boolForKey:@"printPageNumber"];
}

float PREF_pageNumberMargin()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return assignedMetricsToPoints( [ud floatForKey:@"pageNumberMargin"] );
}

NSMutableAttributedString* PREF_pageNumberFormat()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return [NSKeyedUnarchiver unarchiveObjectWithData:[ud dataForKey:@"pageNumberFormat"]];
}







float PREF_topMargin()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return assignedMetricsToPoints( [ud floatForKey:@"topMargin"] );
}

float PREF_bottomMargin()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return assignedMetricsToPoints( [ud floatForKey:@"bottomMargin"] );
}

float PREF_leftMargin()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return assignedMetricsToPoints( [ud floatForKey:@"leftMargin"] );
}

float PREF_rightMargin()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return assignedMetricsToPoints(  [ud floatForKey:@"rightMargin"] );
}



float PREF_documentWidth()
{
	NSPrintInfo *pInfo = [NSPrintInfo sharedPrintInfo];
	return  [pInfo paperSize].width - PREF_leftMargin() - PREF_rightMargin();
}
float PREF_documentHeight()
{
	NSPrintInfo *pInfo = [NSPrintInfo sharedPrintInfo];
	return  [pInfo paperSize].height - PREF_topMargin() - PREF_bottomMargin();
}


BOOL PREF_splitVertically()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return [ud boolForKey:@"splitVertically"];
}
BOOL PREF_onDownloadOptionScale()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return [ud boolForKey:@"onDownloadOptionScale"];
}
BOOL PREF_onDownloadOptionTrim()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return [ud boolForKey:@"onDownloadOptionTrim"];
}
BOOL PREF_onDownloadOptionMakeList()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return [ud boolForKey:@"onDownloadOptionMakeList"];
}
BOOL PREF_onDownloadOptionTags()
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	return [ud boolForKey:@"onDownloadOptionTags"];
}

//onDownloadOptionMakeList
///////




///////

float assignedMetricsToPoints(float value)
{
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	NSString* str = [ud stringForKey:@"metrics"];
	
	if( [str isEqualToString:@"mm" ] )
		return value * 2.83;
	
	if( [str isEqualToString:@"inch" ])
		return value * 72.0;
	
	return value;
}



@implementation AppController


- (void)awakeFromNib
{
	
	
	//set up preference
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	if( ! [ud boolForKey:@"exist"] )  // if pref doesnt exist, set default
	{
		
		
		
		[self ok];
		
		
		
		
	}
	else
		[self loadUserDefaults];
	
	

	
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appWillTerminate:) name:NSApplicationWillTerminateNotification
											   object:[NSApplication sharedApplication]];
	
	
	
}

-(void)appWillTerminate:(id)sender
{
		NSString* scratchFolder =[NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/PatentDownloader/"];
	
	
	
	[[NSWorkspace sharedWorkspace] performFileOperation:NSWorkspaceDestroyOperation
												 source:scratchFolder
											destination:@"" 
												  files:[[NSFileManager defaultManager] 
																	directoryContentsAtPath:scratchFolder]
													tag:NULL];
}

-(void)dealloc
{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[super dealloc];
}	

- (IBAction)buttonClicked:(id)sender
{
	int tag = [sender tag];

	if( tag == -1 )
	{
	}
	
	if( tag == 0 )
	{
		[self ok];
	}
	
	[panel close];
	
}

-(void)loadUserDefaults
{
	// get default values and set.
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	if( [ud boolForKey:@"exist"] )
	{
		id value;
		
		
		[printPageNumber		setState:
			([ud boolForKey:@"printPageNumber"] ? NSOnState : NSOffState ) ];
		
		[pageNumberMargin setFloatValue:[ud floatForKey:@"pageNumberMargin"]];
		
		value = [ud dataForKey:@"pageNumberFormat"];
		if( value != NULL )
			[[pageNumberFormat textStorage] setAttributedString:
				[NSKeyedUnarchiver unarchiveObjectWithData:value ] ];
		
		//
			
		
		//
		[topMargin setFloatValue:[ud floatForKey:@"topMargin"]];
		[bottomMargin setFloatValue:[ud floatForKey:@"bottomMargin"]];
		[leftMargin setFloatValue:[ud floatForKey:@"leftMargin"]];
		[rightMargin setFloatValue:[ud floatForKey:@"rightMargin"]];
		
		NSPrintInfo* pi  = [NSPrintInfo sharedPrintInfo];
		NSRect rect = [pi imageablePageBounds];
		NSSize paperSize = [pi paperSize];
		
		[bottomMargin_min setFloatValue:
			[self showInAssignedMetrics:rect.origin.y]];
		[topMargin_min setFloatValue:
			[self showInAssignedMetrics:paperSize.height - (rect.origin.y + rect.size.height) ]];
		[leftMargin_min setFloatValue:
			[self showInAssignedMetrics:rect.origin.x]];
		[rightMargin_min setFloatValue:
			[self showInAssignedMetrics:paperSize.width - (rect.origin.x + rect.size.width) ]];
		
		//
		value = [ud stringForKey:@"metrics"];
		if( value == NULL ) value = @"mm";
		
		[metrics selectItemWithTitle:value];
		
		
		//////
		
		[splitVertically		setState:
			([ud boolForKey:@"splitVertically"] ? NSOnState : NSOffState ) ];
		[onDownloadOptionScale		setState:
			([ud boolForKey:@"onDownloadOptionScale"] ? NSOnState : NSOffState ) ];
		[onDownloadOptionTrim		setState:
			([ud boolForKey:@"onDownloadOptionTrim"] ? NSOnState : NSOffState ) ];
		//[onDownloadOptionMakeList		setState:
		//	([ud boolForKey:@"onDownloadOptionMakeList"] ? NSOnState : NSOffState ) ];
		[onDownloadOptionTags		setState:
			([ud boolForKey:@"onDownloadOptionTags"] ? NSOnState : NSOffState ) ];

	}
	
}


-(IBAction)metricsChanged:(id)sender
{
	
	NSPrintInfo* pi  = [NSPrintInfo sharedPrintInfo];
	NSRect rect = [pi imageablePageBounds];
	NSSize paperSize = [pi paperSize];
	
	[bottomMargin_min setFloatValue:
		[self showInAssignedMetrics:rect.origin.y]];
	[topMargin_min setFloatValue:
		[self showInAssignedMetrics:paperSize.height - (rect.origin.y + rect.size.height) ]];
	[leftMargin_min setFloatValue:
		[self showInAssignedMetrics:rect.origin.x]];
	[rightMargin_min setFloatValue:
		[self showInAssignedMetrics:paperSize.width - (rect.origin.x + rect.size.width) ]];
	
}


- (IBAction)openPanel:(id)sender
{
	[self loadUserDefaults];
	
	// open
	[panel makeKeyAndOrderFront:self];
	
}

-(float)showInAssignedMetrics:(float)points
{
	
	if( [[metrics selectedItem] tag] == 0 ) // mm
		return points / 2.83;
	
	if( [[metrics selectedItem] tag] == 1 )
		return points / 72.0;
	
	return points;
	
}
-(IBAction)webFind:(id)sender
{
	return;
	NSLog(@"webFind");
	
	NSDocument* document = [[NSDocumentController sharedDocumentController] currentDocument];
	
	//[[[[document valueForKey:@"tabView"] selectedTabViewItem] view]
	//	 performFindPanelAction:(id)sender];
	
	//[[document windowForSheet] tryToPerform:@selector(performFindPanelAction:) with:nil];
	
	


	[[document webView] searchFor:[webFindString stringValue]
		   direction:(BOOL)[sender tag] 
	   caseSensitive:(BOOL)NO
				wrap:(BOOL)NO];	
}
-(IBAction)performFindPanelAction:(id)sender
{

	NSDocument* document = [[NSDocumentController sharedDocumentController] currentDocument];

	//[[[[document valueForKey:@"tabView"] selectedTabViewItem] view]
	//	 performFindPanelAction:(id)sender];

	//[[document windowForSheet] tryToPerform:@selector(performFindPanelAction:) with:nil];
		
	
	NSResponder* responder = [[document windowForSheet] firstResponder];
	
	if( [responder isKindOfClass:[NSTextView class]]  )
	 [responder performFindPanelAction:sender];
	
	/*
	else
		[webFindPanel makeKeyAndOrderFront:self];
	 
	 */
	
	
	
	//NSLog(@"responder %@",[responder className]);
//	[[document textView]
	//	 performFindPanelAction:(id)sender];//performFindPanelAction

	//[NSApp tryToPerform:@selector(performFindPanelAction:) with:nil];
}


- (void)ok
{
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	
	
	
	
	[ud setBool:([printPageNumber state] == NSOnState ? YES:NO) forKey:@"printPageNumber"];
	[ud setFloat:[pageNumberMargin floatValue] forKey:@"pageNumberMargin"];	
	
	
	[ud setFloat:[topMargin floatValue] forKey:@"topMargin"];	
	[ud setFloat:[bottomMargin floatValue] forKey:@"bottomMargin"];	
	[ud setFloat:[leftMargin floatValue] forKey:@"leftMargin"];	
	[ud setFloat:[rightMargin floatValue] forKey:@"rightMargin"];	
	
	[ud setObject:[NSKeyedArchiver archivedDataWithRootObject:[pageNumberFormat textStorage]] 
		   forKey:@"pageNumberFormat"];	
	
	
	
	[ud setObject:[[metrics selectedItem] title]  forKey:@"metrics"];
	
	/////
	
	[ud setBool:([splitVertically state] == NSOnState ? YES:NO) forKey:@"splitVertically"];
	[ud setBool:([onDownloadOptionScale state] == NSOnState ? YES:NO) forKey:@"onDownloadOptionScale"];
	[ud setBool:([onDownloadOptionTrim state] == NSOnState ? YES:NO) forKey:@"onDownloadOptionTrim"];
	//[ud setBool:([onDownloadOptionMakeList state] == NSOnState ? YES:NO) forKey:@"onDownloadOptionMakeList"];
	[ud setBool:([onDownloadOptionTags state] == NSOnState ? YES:NO) forKey:@"onDownloadOptionTags"];
	
	////
	
	[ud setBool:YES forKey:@"exist"];
	//NSLog(@"pref changed");
	
	
	
	
	
}


/////recog





@end
