/* AttributeSafeTextView */
//

#import <Cocoa/Cocoa.h>
#import "NSTextView (coordinate extension).h"


@interface AttributeSafeTextView : NSTextView
{
	NSMutableArray* impersistentAttributes;

}
-(NSData*)portableDateFromRange:(NSRange)range;
// create attributes safe data which keeps all the attributes in NSData.

-(NSMutableAttributedString*)unpackPortable:(NSData*)portableData;
// defrost attribute safe data

-(NSMutableDictionary*)attributesDataFromRange:(NSRange)range;
// internal method to extract attributes

-(void)addImpersistentAttribute:(NSString*)attribute;


@end

