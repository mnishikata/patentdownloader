#import "AttributeSafeTextView.h"
#import "GraphicResizableTextView.h"

#define AVAILABLE_TYPES [NSArray arrayWithObjects:  @"ASTVData", NSRTFDPboardType , NSRTFPboardType, NSStringPboardType, @"Apple URL pasteboard type", @"WebURLsWithTitlesPboardType", NSFilenamesPboardType, nil]

#define AttributeSafeTextViewPboardType @"ASTVData"



@implementation AttributeSafeTextView


- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
		
		[self awakeFromNib];
    }
    return self;
}



- (void)awakeFromNib
{
	impersistentAttributes = [[NSMutableArray alloc] init];
	
	[self addImpersistentAttribute:NSCursorAttributeName];
	[self addImpersistentAttribute:NSToolTipAttributeName];

/*
	TemporaryAttributesTextStorage* ts = [[[TemporaryAttributesTextStorage alloc] init] autorelease];
	[ [self  layoutManager] replaceTextStorage:ts];
*/
	/*
	//NSLog(@"awake");

	 if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		 [super awakeFromNib];
	 }
*/
}
-(void)dealloc
{
	/*
	//remove scratch file if exists
	NSString* path = [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Kojo/GraphicAttachment.tiff"];
	
	[[NSFileManager defaultManager] removeFileAtPath:path handler:NULL];

	 */
	
	
	[impersistentAttributes release];
	[super dealloc];
}

-(void)addImpersistentAttribute:(NSString*)attribute
{
	if( [impersistentAttributes indexOfObject:attribute] == NSNotFound )
		[impersistentAttributes addObject:attribute];
}

- (void)setTypingAttributes:(NSDictionary *)attributes
{
	//NSLog(@"setTypingAttributes");
	
	NSMutableDictionary* _attributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
 
	[_attributes removeObjectsForKeys: impersistentAttributes ];
 
	[super setTypingAttributes:[NSDictionary dictionaryWithDictionary:_attributes]];
}

-(NSData*)portableDateFromRange:(NSRange)range
{
	// create plain text data
	NSAttributedString* textStr = [[self textStorage] attributedSubstringFromRange:range];
	NSData* aTextData = [[textStr string] dataUsingEncoding:NSUTF8StringEncoding];
	
	
	
	//create attributes archive
	
	NSMutableData* archivedData = [NSMutableData data];
	NSKeyedArchiver* archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:archivedData];
	
	[archiver encodeObject:[self attributesDataFromRange:range] forKey:@"attributes"];
	//NSLog(@"portable...att");
	
	
	[archiver finishEncoding];
	[archiver release];
	
	
	
	
	NSFileWrapper* wrapper = [[[NSFileWrapper alloc] initDirectoryWithFileWrappers:NULL] autorelease];
	
	//Add data to wrapper
	[wrapper   addRegularFileWithContents:aTextData preferredFilename:@"StringData"];
	[wrapper   addRegularFileWithContents:archivedData preferredFilename:@"AttributesData"];
	
	
	
	return [wrapper serializedRepresentation];
	
	
}

-(NSMutableDictionary*)attributesDataFromRange:(NSRange)range
{
	NSMutableDictionary* attributesSaver = [NSMutableDictionary dictionary];
	
	unsigned hoge;
	NSDictionary* attributes;
	NSRange longestEffectiveRange;
	
	for(hoge = range.location; hoge < NSMaxRange(range);   )
	{

		attributes = [[self textStorage] attributesAtIndex:hoge 
									 longestEffectiveRange:&longestEffectiveRange inRange:range];
		
		NSRange attributesRange;
		attributesRange.location = longestEffectiveRange.location - range.location;
		attributesRange.length = longestEffectiveRange.length;
		

		//delete impersistentAttributes
		NSMutableDictionary* _attributes = [NSMutableDictionary dictionaryWithDictionary:attributes];
		//[_attributes removeObjectsForKeys: impersistentAttributes ];
		 // do not delete when copy or saving
		
		[attributesSaver setObject:_attributes forKey:NSStringFromRange(attributesRange)];
		
		hoge = NSMaxRange(longestEffectiveRange);
	}

	return attributesSaver;
}



-(NSMutableAttributedString*)unpackPortable:(NSData*)portableData
{

	NSFileWrapper* wrapper = [[NSFileWrapper alloc] initWithSerializedRepresentation:portableData];
	
	//check validity
	if( ! [wrapper isDirectory] )	return NULL;
	
	
	NSDictionary* aDic  = [wrapper fileWrappers];
	
	NSData* textData = [[aDic objectForKey:@"StringData"] regularFileContents];
	NSData* attrData = [[aDic objectForKey:@"AttributesData"] regularFileContents];
	

	NSString* plainText = [[[NSString alloc] initWithData:textData encoding:NSUTF8StringEncoding] autorelease];
	NSMutableAttributedString* unpackedString = [[ NSMutableAttributedString alloc ] initWithString:plainText];
	
	
	
	//unarchive
	
	NSKeyedUnarchiver* unarchiver; 
	unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:attrData];
	NSMutableDictionary* attributeSaver	= [unarchiver decodeObjectForKey:@"attributes"];
	[unarchiver finishDecoding];
	
	
	
	unsigned hoge;
	NSArray* keys = [attributeSaver allKeys];
	
	for( hoge = 0; hoge < [keys count]; hoge++ )
	{
		
		NSString* key = [keys objectAtIndex:hoge];
		//NSLog(@"key %@",key);
		NSRange effectiveRange = NSRangeFromString( key );
		NSDictionary* attributes = [attributeSaver objectForKey:key];
		
		[unpackedString setAttributes:attributes range:effectiveRange];

	}
	
	
	/*
	//adjust graphic size for graphic resizable textView
	NSRange range;
	for( hoge = 0; hoge < [unpackedString length]; hoge++ )
	{
		id something = [unpackedString attribute:MNGraphicSizeAttributeName 
							  atIndex:hoge
			   longestEffectiveRange:&range inRange:NSMakeRange(0,[unpackedString length])];
		
		if( something != NULL)
			
		{
			//NSLog(@"graphic size found");
			
			NSTextAttachment* selectedImage = [unpackedString attribute:NSAttachmentAttributeName 
															   atIndex:hoge
														effectiveRange:NULL];
			NSSize newSize = NSSizeFromString(something);
			
			
			// resize graphic
			NSImage* image = [[selectedImage attachmentCell] image];
			[image setScalesWhenResized:YES];
			
			NSBitmapImageRep* imageRep = [image bestRepresentationForDevice:nil] ;
			
			NSSize size = [imageRep size];
			//NSLog(@"size %@",NSStringFromSize(size));
			
			[imageRep setSize:newSize];
			[image setSize:newSize];
			
			[[selectedImage attachmentCell] setImage:image];
			
			[unpackedString addAttribute:NSAttachmentAttributeName value:selectedImage
									   range:NSMakeRange(hoge, 1)];

		}
		
		hoge = NSMaxRange(range);
	}
	
	*/
	
	
	[unarchiver release];
	[wrapper release];
	[unpackedString autorelease];

	return unpackedString;
}


- (BOOL)writeSelectionToPasteboard:(NSPasteboard *)pboard types:(NSArray *)types
{
	
	[pboard declareTypes:types owner:self];
	
	
	NSData* aDragginData;
	NSRange selRange = [self selectedRange];

	// write to paste board
	
	aDragginData = [self portableDateFromRange:selRange] ;
	[pboard setData:aDragginData forType:AttributeSafeTextViewPboardType];
	

	
	//rtfd
	aDragginData = [[self textStorage] RTFDFromRange:selRange documentAttributes:NULL];
	[pboard setData:aDragginData forType:NSRTFDPboardType]; 
	
	
	//rtf
	aDragginData = [[self textStorage] RTFFromRange:selRange documentAttributes:NULL];
	[pboard setData:aDragginData forType:NSRTFPboardType]; 
	
	//String does not work ????****
	NSString* aStr = [[[self textStorage] string] substringWithRange:selRange];
	[pboard setString:aStr forType:NSStringPboardType];  //export string
	
	
	
	/*
	//TIFF
	NSRange range = [self selectedRange];
	if( range.length == 1 )
	{
		id something = [[self textStorage] attribute:NSAttachmentAttributeName
											 atIndex:range.location effectiveRange:NULL];
		if( something != NULL )
		{
			NSImage* image =  [[something attachmentCell] image];
			
			NSData* tiffData = [image TIFFRepresentation];
			
			NSString* path = [NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Kojo/GraphicAttachment.tiff"];
			
			[tiffData writeToFile:[path uniquePathForFolder] atomically:YES ];
				

			//paste as filename
			[pboard setPropertyList:[[[NSArray arrayWithObject:path] description] propertyList]
							forType:NSFilenamesPboardType]; 

		}
	}*/
	
	
	
	return YES;	
}


-(id)contentsInPaseteboard:(NSPasteboard *)pboard
{
	BOOL returnStatus = NO;
	
	if( [pboard availableTypeFromArray:AVAILABLE_TYPES] == AttributeSafeTextViewPboardType  )
	{	
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:AttributeSafeTextViewPboardType];
		if(aData == NULL) returnStatus = NO;
		
		else
		{
			NSMutableAttributedString* anAttr = [self unpackPortable:aData];
			return anAttr;
		}
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSRTFDPboardType)
	{
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:NSRTFDPboardType];
		
		NSAttributedString* anAttr = [[NSAttributedString alloc] initWithRTFD:aData documentAttributes:NULL];
		
		//paste 
		[anAttr autorelease];
		return anAttr;
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSRTFPboardType)
	{
		//get array data and text data from pboard 
		NSData* aData = [pboard dataForType:NSRTFPboardType];
		
		NSAttributedString* anAttr = [[NSAttributedString alloc] initWithRTF:aData documentAttributes:NULL];
		
		//paste 
		[anAttr autorelease];
		return anAttr;
		
		
	}else if([pboard availableTypeFromArray:AVAILABLE_TYPES] == NSStringPboardType)
	{
		//get array data and text data from pboard 
		NSString* aStr = [pboard stringForType:NSStringPboardType];
		
		//paste 
		return aStr;
		
	}
	
	return NULL;
}

- (BOOL)readSelectionFromPasteboard:(NSPasteboard *)pboard  //Paste
{
	//NSLog(@"read");
	BOOL returnStatus = NO;
	
	id contents = [self contentsInPaseteboard:pboard];
	
	if( contents != NULL )
	{
		[self pasteText:contents];
		returnStatus = YES;
	}
	else
		returnStatus = [super readSelectionFromPasteboard:pboard];

	
	return returnStatus;
}

- (BOOL)resignFirstResponder
{	
	BOOL flag = [super resignFirstResponder];
	
	// Inputting Kanji
	if( [self hasMarkedText] )
		flag = NO;
	
		
	return flag;
	
}



@end
