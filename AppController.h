/* AppController */

#import <Cocoa/Cocoa.h>


NSString* emptyOrValue(id something);




extern BOOL			PREF_printPageNumber();
extern float		PREF_pageNumberMargin();
extern NSMutableAttributedString* PREF_pageNumberFormat();

extern float		PREF_topMargin();
extern float		PREF_bottomMargin();
extern float		PREF_leftMargin();
extern float		PREF_rightMargin();
extern float		PREF_documentWidth();
extern float		PREF_documentHeight();

extern BOOL		PREF_splitVertically();
extern BOOL		PREF_onDownloadOptionScale();
extern BOOL		PREF_onDownloadOptionTrim();
extern BOOL		PREF_onDownloadOptionMakeList();
extern BOOL		PREF_onDownloadOptionTags();



float assignedMetricsToPoints(float value);




@interface AppController : NSObject
{
    IBOutlet id bottomMargin;
    IBOutlet id bottomMargin_min;
    IBOutlet id leftMargin;
    IBOutlet id leftMargin_min;
    IBOutlet id metrics;
    IBOutlet id pageNumberFormat;
    IBOutlet id pageNumberMargin;
    IBOutlet id panel;
    IBOutlet id printPageNumber;
    IBOutlet id rightMargin;
    IBOutlet id rightMargin_min;
    IBOutlet id topMargin;
    IBOutlet id topMargin_min;
	
	IBOutlet id onDownloadOptionScale;
	IBOutlet id onDownloadOptionTrim;
	//IBOutlet id onDownloadOptionMakeList;
	IBOutlet id onDownloadOptionTags;

	IBOutlet id splitVertically;

	IBOutlet id webFindString;
	IBOutlet id webFindPanel;

}
- (IBAction)buttonClicked:(id)sender;
- (IBAction)metricsChanged:(id)sender;
- (IBAction)openPanel:(id)sender;

-(float)showInAssignedMetrics:(float)points;

@end
