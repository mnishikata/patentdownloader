//
//  JapaneseDelegate.m
//  PatentDownloader
//
//  Created by Masatoshi Nishikata on 06/05/17.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "JapaneseDelegate.h"
#import "MyDocument.h"

#define PARENT_DOCUMENT [[[webView window] windowController] document]


@implementation JapaneseDelegate



-(id)init
{
    self = [super init];
    if (self) {
		
		autoPilotMode = NO;
		
		
		[NSBundle loadNibNamed:@"JapaneseDelegate"  owner:self];
		

		
		//　初期値設定
		myStep = 0;
		myPatentNumber = @"H14-65845";
		mySearchCategory = 31;
		mySearchArchive = 0;
		
		//preference
		[[WebPreferences standardPreferences] setDefaultFontSize:13];
		
		
		
		
		
	}
	return self;
}



-(NSView*)view
{
	return view;
}

-(void)openTab
{
	autoPilotMode = NO;
	
	NSURL* url = [NSURL URLWithString:@"http://www7.ipdl.inpit.go.jp/Tokujitu/tjkta.ipdl?N0000=108"];
	NSURLRequest* urlRequest = [[[NSURLRequest alloc] initWithURL:url ] autorelease];
	
	
	[[webView mainFrame] loadRequest:urlRequest];
}


-(void)step1_OpenWebPage
{
	NSLog(@"step1_OpenWebPage");
	NSString* myUrl;
	myStep = 100;
	
	//read form
	myPatentNumber = [patentNumberTextField stringValue];
	mySearchCategory = 	
		[[kindOfSearchCategory selectedItem] tag];
	mySearchArchive = 	
		[[kindOfSearchArchive selectedItem] tag];
	if( mySearchCategory == 35   && ( mySearchArchive == 0  || mySearchArchive == 1))
	{
		NSBeginAlertSheet(@"Error", @"OK",
						  NULL, NULL, [webView window] , NULL ,
						  NULL, NULL ,NULL ,[ NSString stringWithCString: "登録番号で検索するときには、公開公報ではなく特許公報または登録実用新案公報を指定して下さい。"]);
[self abortProcess];	
return;
	}
//start connecting

myUrl = @"http://www7.ipdl.inpit.go.jp/Tokujitu/tjkta.ipdl?N0000=108";

resourceCount++;

[self connectURLString:myUrl];

}

-(void)step2_AutoFillAndSearch
{
	//種別ボタンをオスbutton1
	
	// reset first
	[ webView stringByEvaluatingJavaScriptFromString : @" if( document.form1.checkbox0.checked == true ) document.form1.checkbox0.click();" ];
	[ webView stringByEvaluatingJavaScriptFromString : @" if( document.form1.checkbox1.checked == true ) document.form1.checkbox1.click();" ];
	[ webView stringByEvaluatingJavaScriptFromString : @" if( document.form1.checkbox2.checked == true ) document.form1.checkbox2.click();" ];
	[ webView stringByEvaluatingJavaScriptFromString : @" if( document.form1.checkbox3.checked == true ) document.form1.checkbox3.click();" ];
	[ webView stringByEvaluatingJavaScriptFromString : @" if( document.form1.checkbox4.checked == true ) document.form1.checkbox4.click();" ];

	//mySearchArchive
	//checkbutton javascript
	
	NSString* javaString = [NSString stringWithFormat:@"document.form1.checkbox%d.click();",mySearchArchive];
	[ webView stringByEvaluatingJavaScriptFromString : javaString ];

	
	NSLog(@"step2");

	myStep = 200;

	
	
	//自動入力 eg. autoFillStr1 + @"31" + autoFillStr2 + @"H12-3456" + autoFillStr3;
	// euivalent to
	// javascript:	document.form1.N0131.value='31';
	//				document.form1.N0132.value='H12-3456';
	//				document.form1.N0130[0].click();

	/*[ webView stringByEvaluatingJavaScriptFromString :
		[autoFillStr1 stringByAppendingString:
			[mySearchCategory stringByAppendingString:
				[autoFillStr2 stringByAppendingString:
					[myPatentNumber stringByAppendingString:
						[autoFillStr3 stringByAppendingString:
							[mySearchArchive stringByAppendingString:
								autoFillStr4
								]]]]]]];*/
	
	javaString = [NSString stringWithFormat:@"document.form1.N0131.value='%d'; document.form1.N0132.value='%@';",
		mySearchCategory,
		myPatentNumber		];
	[ webView stringByEvaluatingJavaScriptFromString : javaString ];

	
	//検索ボタンをオスbutton1
	//[ webView stringByEvaluatingJavaScriptFromString : @" document.form1.checkbox0.click();" ];　これはだめだった
	[ webView stringByEvaluatingJavaScriptFromString : @"goKensaku(203);" ];
}

-(void)step3_ShowList
{
	NSLog(@"step3");

	myStep = 300;
	
	NSString* myKey= @">1</FONT>";
	NSString* mySourceString;
	NSData* mySourceData;
	NSRange myRange;
	
	mySourceData = [[[webView mainFrame] dataSource ] data];
	mySourceString = [[NSString alloc] initWithData:mySourceData encoding:NSASCIIStringEncoding]; 
	myRange = [mySourceString rangeOfString:myKey];
	if(myRange.length != 0){

		[ webView stringByEvaluatingJavaScriptFromString : @"goList();" ]; // goto next step
	}else{	//　以下エラー処理　<FONT COLOR="blue">1</FONT>がある場合にのみすすむ。
		
		[self abortProcess];	

		
		NSBeginAlertSheet(@"Error", @"OK",
						  NULL, NULL, [webView window] , NULL
						  ,  NULL, NULL ,NULL ,@"The document cannot be found.");
		

	}
	[mySourceString release];
}

-(void)step4_OpenPatentDocument 
{
	NSLog(@"step4");

	myStep = 400;

	//NSString* theFrameName= @"tjswh";
	//WebFrame* targetFrame;
	
	//targetFrame = [[webView mainFrame] findFrameNamed:theFrameName];
	[ webView stringByEvaluatingJavaScriptFromString : @"window.parent.tjswh.ListSubmit(0);" ];
	
}

-(void)step5_ShowTheDocument//javascript:ShowFrames('11110000000110011000')
{
	NSLog(@"step5");

	myStep = 500;

	[ webView stringByEvaluatingJavaScriptFromString :
		@"window.parent.tjswh.tjitemidx.ShowFrames('11110000000110011000');" ];
}

- (void)step6_GetThePatentDocument
{
	NSLog(@"step6");

	//set page title
	 [[webView window] setTitle:myPatentNumber];
	 
	 
	myStep = 600;
	
	NSString* theFrameName= @"tjitemcnt";
	WebFrame* targetFrame;

	
	targetFrame = [[webView mainFrame] findFrameNamed:theFrameName]; //文献が表示されているフレーム
	downloadedAttributedString = [ [[targetFrame frameView] documentView] attributedString]; //webDocumentプロトコルを呼び出す。
									
	[PARENT_DOCUMENT setProgressBar:-1.0];

	[PARENT_DOCUMENT setResult:downloadedAttributedString];
	
	//step 7
	myStep = 700;
	//あとしまつ
	[self abortProcess];
	
	
	
}

//save

/*
- (NSFileWrapper *)fileWrapperRepresentationOfType:(NSString*)type//overide
{
	NSData* mySavingData;
	NSRange mySavingRange;
	mySavingRange.location = 0;
	mySavingRange.length =  [[htmlSource textStorage] length];
	mySavingData = [htmlSource RTFDFromRange:mySavingRange];
    
	
	NSFileWrapper *wrapper = [[htmlSource textStorage] RTFDFileWrapperFromRange:mySavingRange
															 documentAttributes:(NSDictionary *)NULL];
	
    // Create data from stirng with encoding
	
	//filename settei  ??
 	[wrapper setFilename:myPatentNumber];
	[wrapper setPreferredFilename:[wrapper filename]];
	
	
    return wrapper;
}


- (IBAction)savePatentDocument:(id)sender
{
	void* contextInfo;
	NSSavePanel* aPanel = [NSSavePanel savePanel];
	[aPanel beginSheetForDirectory:NULL
							  file:[NSString stringWithFormat:@"%@.rtfd",myPatentNumber]
					modalForWindow:[self windowForSheet]
					 modalDelegate:self
					didEndSelector:@selector(savePanelDidEnd:returnCode:contextInfo:)
					   contextInfo:contextInfo];
	///
}

- (void)savePanelDidEnd:(NSSavePanel *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo
{
	if( returnCode == NSFileHandlingPanelOKButton ){
		BOOL flag = 
		
		[[self fileWrapperRepresentationOfType:NULL] 
				writeToFile:[sheet filename] atomically:YES updateFilenames:YES];
		
		if( flag == NO )
		{
			NSBeginAlertSheet(@"Error", @"OK",
							  NULL, NULL, [webView window] , NULL
							  ,  NULL, NULL ,NULL ,[ NSString stringWithCString: "保存できません"]);
			
		}
	}
}

*/
//--------------------------------------------------------------//
// WebFrameLoadDelegate のメソッド
//--------------------------------------------------------------//

//アドレス表示
- (void)webView:(WebView*)sender 
                didStartProvisionalLoadForFrame:(WebFrame*)frame
{
	
	
	[PARENT_DOCUMENT setLoading:YES];

	
	
    // フレームがメインフレームの場合
    if (frame == [sender mainFrame]) {
        // URL を文字列型で取得します
        NSString*	urlStr;
        urlStr = [[[[frame provisionalDataSource] request] URL] absoluteString];
        
        // URL を設定します
      //  [urlTextField setStringValue:urlStr];
			
    }
}

//タイトル設定
- (void)webView:(WebView*)sender 
didReceiveTitle:(NSString*)title 
	   forFrame:(WebFrame*)frame
{
    // フレームがメインフレームの場合
    if (frame == [sender mainFrame]) {
        // タイトルを設定します
     //  
    }
}


//フレームを読み終えたときの処理
- (void)webView:(WebView*)sender 
        didFinishLoadForFrame:(WebFrame*)frame
{
	
	if (frame == [sender mainFrame]) {

    
		[PARENT_DOCUMENT setLoading:NO];

	}
	

	
	if( autoPilotMode == NO ) return;


	switch(myStep)
	{
		//		case 0:	break;
		case 100:	[self step2_AutoFillAndSearch];	break;
		case 200:	[self step3_ShowList];	break; 
		case 300:	myStep ++;	break;
		case 301:	myStep ++;	break;
		case 302:	myStep ++;	break;
		case 303:	myStep ++;	break;
		case 304:	myStep ++;	break; //  以上は他のフレーム読み込み
		case 305:	[self step4_OpenPatentDocument];	break;
		case 400:	myStep ++;	break;
		case 401:	myStep ++;	break;
		case 402:	myStep ++;	break;
		case 403:	myStep ++;	break;
		case 404:	myStep ++;	break; //  以上は他のフレーム読み込み
		case 405:	[self step5_ShowTheDocument];	break;
		case 500:	myStep ++;	break;
		case 501:	[self step6_GetThePatentDocument];	break;//////////
		default:	break;
	}
	
}

//progress bar
- (id)webView:(WebView*)sender 
        identifierForInitialRequest:(NSURLRequest*)request
        fromDataSource:(WebDataSource*)dataSource
{
    // リソース識別子として、resourceCount の NSNumber を作成します
    NSNumber*	number;
    number = [NSNumber numberWithInt:resourceCount++];
    return number;
}

- (void)webView:(WebView*)sender 
	   resource:(id)identifier 
        didFinishLoadingFromDataSource:(WebDataSource*)dataSource
{
    resourceCompletedCount++;
    [self _updateResourceStatus];
}

- (void)webView:(WebView*)sender 
	   resource:(id)identifier 
        didFailLoadingWithError:(NSError*)error 
 fromDataSource:(WebDataSource*)dataSource
{
    resourceFailedCount++;
    [self _updateResourceStatus];
}
- (void)_updateResourceStatus // プログレスバーの設定をします
{
	if( autoPilotMode == NO ) return;
	
    if (resourceCount == 0) {
 		
		[PARENT_DOCUMENT setProgressBar:0.0]; 
    }
	else {
        double	value;
        value = ((resourceCompletedCount + resourceFailedCount) / (double)resourceCount)/10 + 
			(myStep / 600.0);
		
		

			[PARENT_DOCUMENT setProgressBar:value*100]; 


    }
}

//URL
- (void)connectURLString:(NSString*)urlString
{
    // NSURL を作成します
    NSURL*	url;
    url = [NSURL URLWithString:urlString];
    
    // NSURLRequest を作成します
    NSURLRequest*	request;
    request = [NSURLRequest requestWithURL:url];
	
    // メインフレームで、URL を開きます
    [[webView mainFrame] loadRequest:request];
}


//スタートボタンがクリックされた
- (IBAction)startButtonClicked:(id)sender;
{
	if(myStep ==0)
	{
		//wait other task
		
		[PARENT_DOCUMENT startDownloading:self];
		
		
		
	}else{
		
		autoPilotMode = NO;
		
		[self abortProcess];


		
	}
	
}

-(void)downloadingDidStart
{

	
	//Step1 接続
	autoPilotMode = YES;
	
	
	[self step1_OpenWebPage];	
}

-(void)downloadingCanceled
{
	autoPilotMode = NO;
	
	[self abortProcess];
	
}

- (void)abortProcess
{
	NSLog(@"abortProcess");
	
	[PARENT_DOCUMENT endDownloading];

	
	[webView stopLoading:self];
	//[viewFilter setHidden:YES];
	
	myStep = 0;
	resourceCount = 0.0;
	resourceCompletedCount=0;
    resourceFailedCount=0;
	[self _updateResourceStatus];

	
	
	autoPilotMode = NO;
	
	
	[PARENT_DOCUMENT setLoading:NO];
	


}

/*
-(IBAction)lockButtonClicked:(id)sender
{
	
	if( [lockView isHidden] )
	{
		[lockView setHidden:NO];
		[lockButton setTitle:@"Lock"];
		
	}
	else
	{
		
		[lockView setHidden:YES];
		[lockButton setTitle:@"Unlock"];
		
	}
	
}*/




@end
