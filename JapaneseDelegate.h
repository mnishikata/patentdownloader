//
//  JapaneseDelegate.h
//  PatentDownloader
//
//  Created by Masatoshi Nishikata on 06/05/17.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface JapaneseDelegate : NSObject {

	
	// japan
	
    IBOutlet id webView;
	
	   IBOutlet id patentNumberTextField;
	   IBOutlet id kindOfSearchCategory;
	   IBOutlet id kindOfSearchArchive;
	   
	   IBOutlet id view;

	   
	   
	   
	   int myStep;
	   
	   int resourceCount;
	   int resourceCompletedCount;
	   int resourceFailedCount;
	   
	   
	   
	   
	   // etc
	   
	   NSTimeInterval timerInterval;
	   NSString* myPatentNumber;
	  int mySearchCategory;
	  int mySearchArchive;

	  NSInvocation* timerInvocation;
	   NSTimer*	timeOutTimer;

	   
	   NSAttributedString* downloadedAttributedString;
	   BOOL autoPilotMode;
}

@end
