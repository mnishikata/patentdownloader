//
//  RecognitionDictionaryController.h
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/06/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "NSTextView (coordinate extension).h"
#import <OgreKit/OgreKit.h>



extern NSString*	FUGO_language( NSDictionary* item );
extern NSString*	FUGO_paragraphDelimiter( NSDictionary* item );

extern NSString*	FUGO_numberRecog( NSDictionary* item );
extern NSString*	FUGO_numberRecogForbiddenPrefix( NSDictionary* item );
extern NSString*	FUGO_numberRecogForbiddenSuffix( NSDictionary* item );
extern NSString*	FUGO_numberConnector( NSDictionary* item );


extern NSString*	FUGO_memberNameExtractor( NSDictionary* item );

extern NSString*	FUGO_altExtractor( NSDictionary* item );

extern NSString*	FUGO_stringRecog( NSDictionary* item );

extern BOOL			FUGO_compareByWord( NSDictionary* item );




@interface RecognitionDictionaryController : NSObject {
	
}

+(NSDictionary*)defaultRecognitionDictionaryFor:(NSString*) contents;
+(NSMutableArray*)recognitionDictionaries;

+(NSDictionary*)loadRecognitionSettingFromFile:(NSString*)path;
+(NSString*)extract:(NSString*)identifier from:(NSString*)oya;

@end
