#import <OgreKit/OgreKit.h>
#import <Cocoa/Cocoa.h>

#import "PrintingTextView.h"
#import "FugoCompletionTextStorage.h"


extern NSMutableString* fillTemplate(NSString* template, FUGO_FINDER entry);



@interface FugoCompletionTextView : PrintingTextView
{
	id dummy;
}

- (id)initWithFrame:(NSRect)frame;



-(void)dealloc;
- (void)awakeFromNib;
-(NSView*)fugoView;
-(IBAction)copyFugo:(id)sender;
- (NSRange)rangeForUserCompletion;
- (NSArray *)completionsForPartialWordRange:(NSRange)charRange indexOfSelectedItem:(int *)index;
-(NSMutableArray*)additionalAutoCompletionListFor:(FUGO_FINDER)entry;
- (NSMenu *)menuForEvent:(NSEvent *)theEvent;
-(void)copyTooltip;









@end