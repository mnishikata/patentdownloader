/*
 符号データはfugoControllerが持っている。
 
 */
//

#import <Cocoa/Cocoa.h>
#import "FugoFinder.h"
#import "RecognitionDictionaryController.h"


@interface FugoCompletionTextStorage : NSTextStorage
{
	NSMutableAttributedString *m_attributedString;

	
	
	// subclass
	FugoFinder* fugoFinder;

	NSDictionary* recognitionSetting;
}
- (id)init;

- (void)dealloc;

-(void)setRecognitionSetting:(NSDictionary*)item;

-(NSDictionary*)recognitionSetting;

-(FugoFinder*)fugoFinder;

-(void)setFugoFinder:(FugoFinder*)ff;



-(NSView*)fugoView;

-(void)addFugo:(NSString*)str;

-(NSArray*)fugoArray;

-(void)setFugoArray:(NSArray*)anArray;


-(BOOL)fugoFinderArrayBuilt;

-(void)buildTable;

-(void)updateTable;


@end
