//
//  FugoCompletionTextStorage.m
//  SampleApp
//
//  Created by Masatoshi Nishikata on 13/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "FugoCompletionTextStorage.h"

@interface FugoCompletionTextStorage ( NSTextStorage )


- (NSString *)string;
- (NSDictionary *)attributesAtIndex:(unsigned)index effectiveRange:(NSRangePointer)aRange;
- (void)replaceCharactersInRange:(NSRange)aRange withString:(NSString *)str;
- (void)setAttributes:(NSDictionary *)attributes range:(NSRange)aRange;

@end


@implementation FugoCompletionTextStorage

- (id)init {
    self = [super init];
    if (self) {
		
		
		m_attributedString = [[NSMutableAttributedString alloc] init];

		

		fugoFinder = [[FugoFinder alloc] init];
		[fugoFinder setTextStorage:self];
		
		
		[self setRecognitionSetting:[RecognitionDictionaryController defaultRecognitionDictionaryFor:@""] ];
		
    }
    
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
 	[fugoFinder release]; 
	[recognitionSetting release];
    [super dealloc];
}
-(void)setRecognitionSetting:(NSDictionary*)item
{
	[recognitionSetting release];
	recognitionSetting = item;	
	[recognitionSetting retain];
}

-(NSDictionary*)recognitionSetting
{
	return recognitionSetting;
}

-(FugoFinder*)fugoFinder
{
	return fugoFinder;
}
-(void)setFugoFinder:(FugoFinder*)ff
{
	[fugoFinder release];
	fugoFinder = ff;
	[fugoFinder retain];
}

-(NSView*)fugoView
{
	
	return [fugoFinder view];
}

-(void)addFugo:(NSString*)str
{
	[fugoFinder addThisString:str];
}

-(NSArray*)fugoArray
{
	return [fugoFinder fugoFinderArray_unique];
		
}

-(void)setFugoArray:(NSArray*)anArray
{
	[fugoFinder setFugoFinderArray_unique:anArray];
}


-(BOOL)fugoFinderArrayBuilt
{
	return [fugoFinder fugoFinderArrayBuilt];
}
-(void)buildTable
{
	[fugoFinder buildTable:self];
}
-(void)updateTable
{
	[fugoFinder updateTable:self];
}



@end


//  ######## fundamental subclassing ##########
@implementation FugoCompletionTextStorage (NSTextStorage)

- (void)replaceCharactersInRange:(NSRange)range withString:(NSString *)str
{
	
    [m_attributedString replaceCharactersInRange:range withString:str];
    
    int lengthChange = [str length] - range.length;
    [self edited:NSTextStorageEditedCharacters range:range changeInLength:lengthChange];
	
	
}


- (NSString *)string
{
    return [m_attributedString string];
}

- (NSDictionary *)attributesAtIndex:(unsigned)index effectiveRange:(NSRangePointer)aRange
{
    return [m_attributedString attributesAtIndex:index effectiveRange:aRange];
}



- (void)setAttributes:(NSDictionary *)attributes range:(NSRange)range
{
    [m_attributedString setAttributes:attributes range:range];
    [self edited:NSTextStorageEditedAttributes range:range changeInLength:0];
	
	
}


@end