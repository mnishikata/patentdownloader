#import "FugoFinder.h"
#import "RecognitionDictionaryController.h"

#define TableRowType @"fugo"
#define TableRowTypes [NSArray arrayWithObjects:@"fugo",NSStringPboardType, nil]

#define COUNTER_RADIUS 7.0

#define UNIQUEID [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];

//#define LONGEST_FUGO_TEMPLATE	[ NSString stringWithCString:"[^0-9|０-９|,、　−〜第項【】(（図\n]+[0-9|０-９]+[a-z|A-Z|ａ-ｚ|Ａ-Ｚ]*"]




NSDictionary* dictionaryFromFUGO_FINDER( FUGO_FINDER aFugo_row )
{	
	return [NSDictionary dictionaryWithObjectsAndKeys:
		NSStringFromRange(aFugo_row.range),@"range",
		[NSString stringWithString:aFugo_row.fugoPrefix], @"fugoPrefix",
		[NSString stringWithString:aFugo_row.fugoString], @"fugoString",
		[NSString stringWithString:aFugo_row.fugoSuffix], @"fugoSuffix",
		[NSString stringWithString:aFugo_row.fugoFlag], @"fugoFlag",
		[NSString stringWithString:aFugo_row.fugoComment], @"fugoComment",
		[NSString stringWithString:aFugo_row.fugoID], @"fugoID",	
		NULL];
}

FUGO_FINDER FUGO_FINDERFromDictionary( NSDictionary* aDic )
{
	FUGO_FINDER returnValue = { {0,0}, @"", @"", @"", @"", @"", @"" };
	
	returnValue.range = NSRangeFromString( [aDic objectForKey:@"range"] );
	returnValue.fugoPrefix = [NSString stringWithString:[aDic objectForKey:@"fugoPrefix"]];
	returnValue.fugoString = [NSString stringWithString:[aDic objectForKey:@"fugoString"]];
	returnValue.fugoSuffix = [NSString stringWithString:[aDic objectForKey:@"fugoSuffix"]];
	returnValue.fugoFlag = [NSString stringWithString:emptyOrValue( [aDic objectForKey:@"fugoFlag"] ) ];
	returnValue.fugoComment = [NSString stringWithString:emptyOrValue( [aDic objectForKey:@"fugoComment"] ) ];
	returnValue.fugoID = [NSString stringWithString:emptyOrValue( [aDic objectForKey:@"fugoID"] ) ];

	//id
	
	if( [returnValue.fugoID isEqualToString:@""] )
		returnValue.fugoID = UNIQUEID;
	
	
	return returnValue;
}




int prefixSort(id num1, id num2, void *context)
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoPrefix;
	NSString* v2 = aRow2.fugoPrefix;
	
	return [v1 compare:v2];
}

int prefixDecSort(id num1, id num2, void *context)
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoPrefix;
	NSString* v2 = aRow2.fugoPrefix;
	
	return [v2 compare:v1];
}

int stringSort(id num1, id num2, void *context)
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoString;
	NSString* v2 = aRow2.fugoString;
	
	return [v1 compare:v2];
}

int stringDecSort(id num1, id num2, void *context)
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoString;
	NSString* v2 = aRow2.fugoString;
	
	return [v2 compare:v1];
}




int suffixSort(id num1, id num2, void *context)   /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoSuffix;
	NSString* v2 = aRow2.fugoSuffix;
	
	
	v1 = [v1 halfwidthString];
	v2 = [v2 halfwidthString];
	


	return [v1 compareNumerically:v2];
}

int suffixDecSort(id num1, id num2, void *context)  /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoSuffix;
	NSString* v2 = aRow2.fugoSuffix;
	

	v1 = [v1 halfwidthString];
	v2 = [v2 halfwidthString];
	
	
	
	return [v2 compareNumerically:v1];

}


int rangeSort(id num1, id num2, void *context)   /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	unsigned v1 = aRow1.range.location;
	unsigned v2 = aRow2.range.location;
	
	if( v1 < v2 )	return NSOrderedAscending;
	if( v1 > v2 )	return NSOrderedDescending;
		return NSOrderedSame;
}

int rangeDecSort(id num1, id num2, void *context)  /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	unsigned v1 = aRow1.range.location;
	unsigned v2 = aRow2.range.location;
	
	if( v1 > v2 )	return NSOrderedAscending;
	if( v1 < v2 )	return NSOrderedDescending;
		return NSOrderedSame;
}

int counterSort(id num1, id num2, void *context)   /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	unsigned v1 = aRow1.range.length;
	unsigned v2 = aRow2.range.length;
	
	if( v1 < v2 )	return NSOrderedAscending;
	if( v1 > v2 )	return NSOrderedDescending;
	return NSOrderedSame;}

int counterDecSort(id num1, id num2, void *context)  /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	unsigned v1 = aRow1.range.length;
	unsigned v2 = aRow2.range.length;
	
	if( v1 > v2 )	return NSOrderedAscending;
	if( v1 < v2 )	return NSOrderedDescending;
	return NSOrderedSame;}




int commentSort(id num1, id num2, void *context)   /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoComment;
	NSString* v2 = aRow2.fugoComment;
	
	
	return [v1 compare:v2];
}

int commentDecSort(id num1, id num2, void *context)  /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoComment;
	NSString* v2 = aRow2.fugoComment;
	
	return [v2 compare:v1];
	
}



int flagSort(id num1, id num2, void *context)   /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoFlag;
	NSString* v2 = aRow2.fugoFlag;
	
	
	return [v1 compare:v2];
}

int flagDecSort(id num1, id num2, void *context)  /// supposed to be number
{
	FUGO_FINDER aRow1 = FUGO_FINDERFromDictionary(num1);
	FUGO_FINDER aRow2 = FUGO_FINDERFromDictionary(num2);
	NSString* v1 = aRow1.fugoFlag;
	NSString* v2 = aRow2.fugoFlag;
	
	return [v2 compare:v1];
	
}



@implementation FugoFinder

- (id)init
{
    self = [super init];
    if (self) {
		
		// load nib
		[NSBundle loadNibNamed:@"FugoFinder"  owner:self];
		

		
		//[fugoFinderArray release];
		//fugoFinderArray = [[NSArray alloc] init];

		
		[fugoFinderArray_original release];
		fugoFinderArray_original = [[NSArray alloc] init];
		
	
		[fugoFinderArray_unique release];
		fugoFinderArray_unique = [[NSArray alloc] init];
		
		
		[fugoFinderArray_unique_filtered release];
		fugoFinderArray_unique_filtered = [[NSArray alloc] init];
	
		
		[fugoFinderTable reloadData];
		
		//d&d
		[fugoFinderTable  registerForDraggedTypes:TableRowTypes];
		[fugoFinderOutlineView  registerForDraggedTypes:TableRowTypes];
	
		
		[fugoFinderOutlineView setVerticalMotionCanBeginDrag:YES];
		
		[fugoFinderOutlineView setAutoresizesOutlineColumn:NO];
			
		
		currentSortedColumn = 1;
		
		
		updateTarget = NULL;
		updateAction = NULL;
		
		flagImage = [[NSImage alloc] initByReferencingFile:
			[[NSBundle bundleForClass:[self class]] pathForResource:@"flagged" ofType:@"tiff"]];
		
		NSImage* _flagtitle = [[[NSImage alloc] initByReferencingFile:
			[[NSBundle bundleForClass:[self class]] pathForResource:@"flagged_header" ofType:@"tiff"]] autorelease];
		
		
		[[[fugoFinderOutlineView tableColumnWithIdentifier:@"C7"] headerCell] setImage:_flagtitle];
		
		
		
		//
		
		counterAttributes   = [NSDictionary dictionaryWithObjectsAndKeys:
			[NSFont boldSystemFontOfSize: 11.0], NSFontAttributeName,
			[NSColor whiteColor], NSForegroundColorAttributeName,
			NULL];
		[counterAttributes retain];
	
		
	}
	
	
    return self;
}



-(void)dealloc
{
	//[fugoFinderArray release];
	[fugoFinderArray_original release];
	[fugoFinderArray_unique release];
	[fugoFinderArray_unique_filtered release];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	
	[flagImage release];

	[super dealloc];
	
}



-(FUGO_FINDER)convertStringToFUGO_FINDER:(NSString*) string
{
	
	//	NSLog(@"convertStringToFUGO");
	
	
	FUGO_FINDER returnValue = { NSMakeRange(NSNotFound,0), @"",@"",@"",@"",@"",@""};
	
	
	NSMutableArray* array = [FugoAnalyzer fugoRangeArrayInString:string 
										with:[textStorage recognitionSetting]];
	int lastIndex = [array count]-1;
	
	if( lastIndex < 0 )
	{
		returnValue.fugoString = string;
		
		
	}else
	{
		NSRange __range = [FugoAnalyzer findMembernameRangeIncludingAltNameFor:lastIndex of:array in:string
				with:[ textStorage recognitionSetting] ];

		NSString* __str = [string substringWithRange:__range];
		
		returnValue.fugoString = [FugoAnalyzer extractMemberNameFromMemberNameIncludingAltName:__str
			with:[ textStorage recognitionSetting]];
		returnValue.fugoPrefix = [FugoAnalyzer extractAltNameFromMemberNameIncludingAltName:__str
			with:[ textStorage recognitionSetting]];
		returnValue.fugoSuffix = [string substringWithRange:NSRangeFromString( [array lastObject] ) ];
		
		
		
	}
	
	returnValue.fugoID = UNIQUEID;

	
	return returnValue;
}


-(IBAction)selectorClicked:(id)sender
{
	/*
		if( [sender selectedSegment] == 0 )
		{
			[fugoFinderArray release];
			fugoFinderArray = [[NSArray alloc] initWithArray:fugoFinderArray_original];
		}else if( [sender selectedSegment] == 1 )
		{
			[fugoFinderArray release];
			fugoFinderArray = [[NSArray alloc] initWithArray:fugoFinderArray_unique];
	
		}*/
	
	
	[fugoFinderTable reloadData];

}

-(NSArray*)uniqueFugoFinderArray:(NSMutableArray*)originalArray
{
/*
 
 (1)符号をキーにして、dictionary化 objectValue は　fugoDic の羅列array
 
(2) 同じ符号を持つグループをＡとする。 fugoDicArrayWithSameSuffix
 
(3) Ａから一つａを取り出す
 
 ａの部材名をチェック。最後から１文字ずつ、他のａ’から共通文字列数を探す。
 
(4)	最後の１文字が他のどれとも合致しない　→　独立　次のａに移る。
(5)	合致する　→　非独立
 最後の１文字が合致する他のａ’の個数ｘを求める。
(6)
 　　　　ｘが減る直前までおしりから合致する文字数を増やしていく。
 
 (7)
 　　　　部材名決定。共通する他のａ’を削除する。
 
 次のａ’も同じ処理。
 すべてのＡが終わったら、次のグループＢ
 
 */
	
// (0)	
	[progressBar setDoubleValue:0.0];
	[progressBar setIndeterminate:NO];

	
// (1)
	NSMutableDictionary* workdic = [[[NSMutableDictionary alloc] init] autorelease];
	NSMutableDictionary* uniqueSuffixDic = [[[NSMutableDictionary alloc] init] autorelease];
	NSMutableArray* uniqueFugoFinderArray = [[[NSMutableArray alloc] init] autorelease];
	

	[progressStatus setStringValue:@"Counting..."];
	
	unsigned hoge;
	for( hoge = 0; hoge < [originalArray count]; hoge++ )
	{
		NSDictionary* fugoDic = [originalArray objectAtIndex:hoge];
		FUGO_FINDER entry = FUGO_FINDERFromDictionary(fugoDic);
		
		
		NSMutableArray* __eMArray = [NSMutableArray array];
		id __existingArray = [workdic objectForKey:entry.fugoSuffix];
		if( __existingArray != NULL )
		[__eMArray addObjectsFromArray:__existingArray  ];
		[__eMArray addObject:fugoDic ];
		
		[workdic setObject:__eMArray forKey:entry.fugoSuffix];
		
		[uniqueSuffixDic setObject:fugoDic forKey:entry.fugoSuffix];
	}
	
	NSEnumerator* enumerator = [uniqueSuffixDic keyEnumerator];
	NSString* suffix;
	
	
	unsigned progressBarCounter = 0;
	
	
	while( suffix = [enumerator nextObject] )
	{
		[progressBar setDoubleValue: ( (double)progressBarCounter / (double)[uniqueSuffixDic count]  ) ];
		progressBarCounter++;
		
//(2)
		NSMutableArray* fugoDicArrayWithSameSuffix = [workdic objectForKey:suffix];
		

		
		
		unsigned piyo;

		
		for( piyo = 0; piyo < [fugoDicArrayWithSameSuffix count]; piyo++ )
		{
//(3)
			FUGO_FINDER entry = FUGO_FINDERFromDictionary([fugoDicArrayWithSameSuffix objectAtIndex:piyo]);
		
			[progressStatus setStringValue:entry.fugoString];
//(4)
			NSRange longestCommonSuffixRange  = 
			[self does:(NSString*)entry.fugoString 
							haveBrotherIn:(NSArray*)fugoDicArrayWithSameSuffix];
	
//(5)
				
			if( longestCommonSuffixRange.length > 0 )
			{

						
				NSString*  commonSuffix = [entry.fugoString substringWithRange:longestCommonSuffixRange];
			
				//NSLog(@"range %@ %@",NSStringFromRange(longestCommonSuffixRange),commonSuffix );

				
				//NSLog(@"found common %@ among %d",commonSuffix, numberOfBrothers);
//(7)
				// now puke is the longest length, with entry.fugoSuffix or suffix
				//         among numberOfBrothers having commonSuffix
				
				// remove entries with common suffix from fugoDicArrayWithSameSuffix
				for( hoge = 0; hoge < [fugoDicArrayWithSameSuffix count]; hoge++ )
				{
					NSDictionary* fugoDic = [fugoDicArrayWithSameSuffix objectAtIndex:hoge];
					FUGO_FINDER entry = FUGO_FINDERFromDictionary(fugoDic);
				
					if( [[entry.fugoString lowercaseString] hasSuffix:
						[commonSuffix lowercaseString ]] )
					{
						[fugoDicArrayWithSameSuffix removeObjectAtIndex:hoge];
						
						hoge --;
					}
				}
				
				//add as new
				entry.fugoString = 
					[commonSuffix  omitSpacesAtBothEnds];

				entry.fugoComment = @"";
				entry.fugoID =  UNIQUEID;
				entry.fugoFlag = @"";
			
				[fugoDicArrayWithSameSuffix addObject:dictionaryFromFUGO_FINDER(entry) ];
				
				

			}// end if	


		}// for loop end: next entry with same number(suffix)
		
		//now fugoDicArrayWithSameSuffix has unique fugo
		[uniqueFugoFinderArray addObjectsFromArray:fugoDicArrayWithSameSuffix];

	}//next suffix
	
	[progressStatus setStringValue:@""];

	
	
	// sort
	NSArray *anArray;
	anArray = [uniqueFugoFinderArray sortedArrayUsingFunction:suffixSort context:NULL];


	
	return anArray;
}

-(NSRange)does:(NSString*)targetStr haveBrotherIn:(NSArray*)fugoDicArray

	// find if there is a brother with same suffix with length num
{
	BOOL compareByWord = [FugoAnalyzer compareByWordWith:[textStorage recognitionSetting]];
	
	
	unsigned count;
	unsigned lastCount = 0;
	
	unsigned hoge;
	unsigned positionFromLast;
	BOOL stopFlag = NO;
	
	NSMutableArray* brothers = [[[NSMutableArray alloc] init] autorelease];
	for( hoge = 0; hoge < [fugoDicArray count]; hoge++ )
	{
		FUGO_FINDER entry = FUGO_FINDERFromDictionary([fugoDicArray objectAtIndex:hoge]);		
		[brothers addObject:entry.fugoString];
	}
	
	if( 1 /*compareByWord == NO*/ ) 
	{
	
	
		for( positionFromLast = 0 ; positionFromLast < [targetStr length]  ; positionFromLast++ )
		{
			count = 0;
			
			
		
			NSString* moji = [targetStr substringWithRange:
				NSMakeRange([targetStr reverseLocation:positionFromLast], 1)];
					
			for( hoge = 0; hoge < [brothers count]; hoge++ )
			{
				NSString* anotherBrother = [brothers objectAtIndex:hoge];
				
				NSRange compareRange = NSMakeRange( [anotherBrother reverseLocation:positionFromLast], 1 );
				compareRange = NSIntersectionRange( [anotherBrother fullRange], compareRange);
				if( compareRange.length > 0 )
				{
				NSString* anotherBrothersMoji = [anotherBrother substringWithRange:compareRange];
				
				
				if( [[moji lowercaseString] isEqualToString:[anotherBrothersMoji lowercaseString]] )
					count++;
				}
			}
				
			if( count == 1 )
				break;
			
			if( ( positionFromLast != 0 )  && ( lastCount > count ) )
				break;
				
			// set criteria
			if( lastCount == 0  ) lastCount = count;
			
		}
		
	}
	else // comapreByWord == YES
	{
		//NSTextView* tv = [[[NSTextView alloc] initWithFrame:NSMakeRect(0,0,100,100)] autorelease];
		//[tv insertText:targetStr];//
		
		NSAttributedString* targetAttrStr = [[NSAttributedString alloc] initWithString:targetStr] ;
		
		
		
		
		for( positionFromLast = 0 ; positionFromLast < [targetStr length]  ;   )
		{
			count = 0;
			
			NSRange __oneRange = NSMakeRange([targetStr reverseLocation:positionFromLast], 1);
			
			
			
			
			
			NSRange wordRange;
			
			wordRange.location = [targetAttrStr nextWordFromIndex:__oneRange.location forward:NO];
			wordRange.length = positionFromLast - wordRange.location +1;
			
			
			
			
			NSString* moji = [targetStr substringWithRange:wordRange];
			[moji omitSpacesAtBothEnds];
			
			//NSLog(@"compare %@",moji);
			
			for( hoge = 0; hoge < [brothers count]; hoge++ )
			{
				
				
				NSString* anotherBrother = [brothers objectAtIndex:hoge];
				
				NSAttributedString* compAttrStr = [[[NSAttributedString alloc] initWithString:anotherBrother] autorelease];
				
				
				
				NSRange compareWordRange;
				NSRange __compareOneRange = NSMakeRange( [anotherBrother reverseLocation:positionFromLast], 1 );
				__compareOneRange = NSIntersectionRange( [anotherBrother fullRange], __compareOneRange);
				
				compareWordRange.location = [compAttrStr nextWordFromIndex:__compareOneRange.location forward:NO];
				compareWordRange.length = positionFromLast - compareWordRange.location +1;
				
				
				if( compareWordRange.length == wordRange.length )
				{
					NSString* anotherBrothersMoji = [anotherBrother substringWithRange:compareWordRange];
					[anotherBrothersMoji omitSpacesAtBothEnds];
					
					if( [[moji lowercaseString] isEqualToString:[anotherBrothersMoji lowercaseString]] )
						count++;
				}
			}
			
			if( count == 1 )
				break;
			
			if( ( positionFromLast != 0 )  && ( lastCount > count ) )
				break;
			
			// set criteria
			if( lastCount == 0  ) lastCount = count;
			
			
			positionFromLast += wordRange.length;
			
		}
		
		[targetAttrStr release];
	}
		
		
		
	
	NSRange returnRange = {0,0};
	
	
	if( positionFromLast == 0 ) // doesnt have a brother
		returnRange.length = 0;

	else if( lastCount == count ) // the array entries are all the same
		returnRange = [targetStr fullRange];

	else // has a brother, but not all
		returnRange = NSMakeRange( [targetStr reverseLocation:positionFromLast -1] , positionFromLast  );
	

//	NSLog(@"BROTHER:%@, %@",[fugoDicArray description], [targetStr substringWithRange:returnRange]);
	

	
	return returnRange;
}



/*
-(unsigned)does:(NSString*)targetStr haveBrotherIn:(NSArray*)fugoDicArray suffixLength:(unsigned)num;
// find if there is a brother with same suffix with length num
{
	unsigned count = 0;
	unsigned hoge;
	
	
	int checkLength = [targetStr length]-num;
	if ( checkLength < 0 )	return 0;
	
	NSString* lastLetter = [targetStr substringFromIndex:checkLength ];
	
	for( hoge = 0; hoge < [fugoDicArray count]; hoge++ )
	{
		FUGO_FINDER entry = FUGO_FINDERFromDictionary([fugoDicArray objectAtIndex:hoge]);
		
		if( [entry.fugoString hasSuffix:lastLetter] ) 
			count++;
	}
	return count;
}
*/

/*

-(void)runSheetOn:(NSWindow*)parentWindow
{
	
	// build table
	
	[self buildTable:self];
	
	
	
	// set up and start sheet
	NSView* sheetview = [self view];	
	NSWindow* fugoFinderWindow = [[NSWindow alloc] initWithContentRect:[sheetview frame]
								 styleMask:NSResizableWindowMask
								   backing:NSBackingStoreBuffered
									 defer:YES];
	
	[fugoFinderWindow setContentView:sheetview];
	[fugoFinderWindow setMinSize:NSMakeSize(300,200)];
	
	[[NSApplication sharedApplication] beginSheet:fugoFinderWindow
								   modalForWindow:parentWindow
									modalDelegate:self
								   didEndSelector:NULL
									  contextInfo:NULL];
}
*/
-(IBAction)buttonClicked:(id)sender
{
	
	
	
	NSLog(@"here");

	NSTableColumn* tc;
	
	if( [sender tag] == 0 )
	{
		[self add:sender];
		
	}else if( [sender tag] == -1 )
	{
			[self remove:sender];	
		
	}else if( [sender tag] == 10 )
	{
		[self buildTable:sender];	
	}else if( [sender tag] == 11 )
	{
		[self updateTable:sender];	
	}
	else if( 100 < [sender tag] )
	{
	
	if( [sender tag] == 101 )
		tc = [fugoFinderOutlineView tableColumnWithIdentifier:@"C1"];
	else if( [sender tag] == 102 )
		tc = [fugoFinderOutlineView tableColumnWithIdentifier:@"C2"];
	else if( [sender tag] == 103 )
		tc = [fugoFinderOutlineView tableColumnWithIdentifier:@"C3"];
	
	
	[self outlineView:fugoFinderOutlineView didClickTableColumn:tc];
	}

	
}

-(void)setTextStorage:(NSTextStorage*)ts
{
	[textStorage release];
	textStorage = ts;
	[textStorage retain];
	
	

}



-(NSString*)buildTable:(id)sender
{
	
	if( sender != nil ) return @"";
	
	
	
	[fugoFinderArray_original release];
	fugoFinderArray_original = [[NSArray alloc] init];
	
	
	[fugoFinderArray_unique release];
	fugoFinderArray_unique = [[NSArray alloc] init];
	
	
	[fugoFinderArray_unique_filtered release];
	fugoFinderArray_unique_filtered = [[NSArray alloc] init];
	
	
	
	
	
	
	
	
	
	
	
	[textStorage setRecognitionSetting : [RecognitionDictionaryController defaultRecognitionDictionaryFor:[textStorage string]] ];
	
	
	fugoFinderArrayBuilt = YES;
	
	[progressIndicator startAnimation:self];
	[progressBar setHidden:NO];
	[progressBar setIndeterminate:YES];
	[progressBar startAnimation:self];

	
	
	// preparation
	
	NSAttributedString* buffer = 
		[[[NSAttributedString alloc] initWithAttributedString:textStorage] autorelease];
	

	
	
	
	
	
	[progressStatus setStringValue:@"Extracting..."];
	
	NSString* bufferStr = [buffer string];
	
	NSMutableArray* tempArray = [FugoAnalyzer fugoRangeArrayInString:bufferStr  
													with:[textStorage recognitionSetting]];
	
	NSMutableArray* returnArray = [[[NSMutableArray alloc] init] autorelease];
		
	[progressStatus setStringValue:@"Analyzing..."];

	unsigned hoge;
	for( hoge = 0 ; hoge < [tempArray count]; hoge++ )
	{
		NSRange membernameRange = 
		[FugoAnalyzer findMembernameRangeIncludingAltNameFor:hoge of:tempArray in:bufferStr
			with:[textStorage recognitionSetting]];
		
		NSString* rawStr = [bufferStr substringWithRange:membernameRange];
		
		
		NSString* memberName;
		NSString* altName;
		NSString* fugoString;
		
		memberName = [FugoAnalyzer extractMemberNameFromMemberNameIncludingAltName:rawStr 
			with:[textStorage recognitionSetting]];
		altName = [FugoAnalyzer extractAltNameFromMemberNameIncludingAltName:rawStr 
			with:[textStorage recognitionSetting]];
		fugoString = [bufferStr substringWithRange:NSRangeFromString( [tempArray objectAtIndex:hoge] ) ];
		
		//
		memberName = [memberName omitSpacesAtBothEnds];
		
		if( ! [memberName isEqualToString:@""] )
		{
		FUGO_FINDER entry;

		
		entry.range = membernameRange;
		entry.fugoString = memberName;
		entry.fugoSuffix = [fugoString omitSpacesAtBothEnds];
		entry.fugoPrefix = [altName omitSpacesAtBothEnds];
		
		entry.fugoComment = @"";
		entry.fugoID = UNIQUEID;
		entry.fugoFlag = @"";
		
		
		[returnArray addObject: dictionaryFromFUGO_FINDER(entry)];
		}
	}
	
	

	
	
	
	
	
	
	// finish
	[progressStatus setStringValue:@"Sorting..."];

	
	[fugoFinderTable setIndicatorImage:[NSImage imageNamed: @"NSAscendingSortIndicator"  ] 
						 inTableColumn:[fugoFinderTable tableColumnWithIdentifier:@"C3"]];
	
	
	[fugoFinderArray_originalToBeUpdated release];
	fugoFinderArray_originalToBeUpdated = [[NSMutableArray alloc ] initWithArray:returnArray];

	

	
	
	
	
	
	
	
	
	NSArray* array = [self uniqueFugoFinderArray:returnArray];


	[fugoFinderArray_uniqueToBeUpdated release];
	fugoFinderArray_uniqueToBeUpdated = [[NSMutableArray alloc ] initWithArray:array];
	
	
	// now fugoFinderArray_originalToBeUpdated has updated all objects,
	// fugoFinderArray_uniqueToBeUpdated has unique items
	

	//(1) remove same items from fugoFinderArray_uniqueToBeUpdated
	
	if( [fugoFinderArray_uniqueToBeUpdated count] > 0 )
	{
	hoge = [fugoFinderArray_uniqueToBeUpdated count] -1;
	while(1)
	{
		id item = [fugoFinderArray_uniqueToBeUpdated objectAtIndex:hoge];
		FUGO_FINDER entryToBeUpdated = FUGO_FINDERFromDictionary(item);
		
		unsigned piyo;
		for( piyo = 0; piyo < [fugoFinderArray_unique count] ; piyo++)
		{
			id item2 = [fugoFinderArray_unique objectAtIndex:piyo];
			FUGO_FINDER entry = FUGO_FINDERFromDictionary(item2);
			
			
			//NSLog(@"%@,%@", entryToBeUpdated.fugoString , entry.fugoString );
			
			if( [ [entryToBeUpdated.fugoString lowercaseString] hasSuffix:
				[entry.fugoString lowercaseString]] && 
				[ entryToBeUpdated.fugoSuffix isEqualToString:entry.fugoSuffix] )
			{
				[fugoFinderArray_uniqueToBeUpdated removeObjectAtIndex:hoge];
				//NSLog(@"remove %@", entryToBeUpdated.fugoString );
				break;
			}

			
		}
		if( hoge == 0 ) break;
		hoge--;
	}
	}
	
			 

	 
	//(2) add items without number in fugoFinderArray_unique
	
	for( hoge = 0; hoge < [fugoFinderArray_unique count]; hoge++)
	{
		id item = [fugoFinderArray_unique objectAtIndex:hoge];
		FUGO_FINDER entry = FUGO_FINDERFromDictionary(item);
		if( [entry.fugoSuffix isEqualToString:@"" ] && ! [entry.fugoString isEqualToString:@"" ])
		{
			NSLog(@"processing %@", entry.fugoString);
			
			NSString* key = [OGRegularExpression regularizeString:entry.fugoString];
			OGRegularExpression    *regex = [OGRegularExpression regularExpressionWithString:key options:OgreIgnoreCaseOption];
			NSEnumerator    *enumerator = [regex matchEnumeratorInAttributedString:textStorage];	// 
			
			OGRegularExpressionMatch    *match;	// 
			while ((match = [enumerator nextObject]) != nil)
			{	// 
				NSLog(@"matched");
				FUGO_FINDER newEntry;
				newEntry.range = [match rangeOfMatchedString];
				newEntry.fugoPrefix = @"";
				newEntry.fugoString = entry.fugoString;
				newEntry.fugoSuffix = @"";
				
				newEntry.fugoFlag = @"";
				newEntry.fugoComment =  @"";
				newEntry.fugoID = UNIQUEID;
			
				[fugoFinderArray_originalToBeUpdated addObject:dictionaryFromFUGO_FINDER(newEntry)];
			}
		}
			
	}
	 
	//(3) combine fugoFinderArray_unique
	if( fugoFinderArrayBuilt == YES )
	{// mutable ###
		
		NSMutableArray* _uniqueArray = [NSMutableArray arrayWithArray: fugoFinderArray_unique];
		[_uniqueArray addObjectsFromArray: fugoFinderArray_uniqueToBeUpdated];
		
		[fugoFinderArray_unique release];
		fugoFinderArray_unique = [[NSArray alloc] initWithArray: _uniqueArray];
		
		//[fugoFinderArray_unique addObjectsFromArray:fugoFinderArray_uniqueToBeUpdated];
		
		
	}


	
	//(4) finish
	NSArray* anArray =
		[fugoFinderArray_unique sortedArrayUsingFunction:suffixSort
												 context:NULL
													hint:[fugoFinderArray_unique sortedArrayHint]];
	
	[fugoFinderArray_unique release];
	fugoFinderArray_unique = [[NSArray alloc ] initWithArray:anArray]; 
	
	
	[fugoFinderArray_unique_filtered release];
	fugoFinderArray_unique_filtered = [[NSArray alloc] initWithArray:anArray];
	
	
	
	[fugoFinderArray_original release];
	// mutable ###
	fugoFinderArray_original = [NSArray arrayWithArray: fugoFinderArray_originalToBeUpdated ];
	[fugoFinderArray_original retain];
	
	
	//(5) make index dictionary
	[self createDictionaryWithKeyFugoDic];
	
	
	//(6) update range in unique array
	for( hoge = 0; hoge < [fugoFinderArray_unique count]; hoge++ )
	{
		NSDictionary* dic = [fugoFinderArray_unique objectAtIndex:hoge];
		FUGO_FINDER entry = FUGO_FINDERFromDictionary(dic);
		
		//get first child
		id firstChild = [self itemAtIndex:0 inGroupOfFugoFinder:entry];
		
		if( firstChild != NULL )
		{
			FUGO_FINDER childentry = FUGO_FINDERFromDictionary( firstChild );
			
			entry.range = childentry.range;
			entry.range.length = [[self groupOfFugoFinderDictionary:dic ] count  ];
		}else
		{
			entry.range = NSMakeRange(NSNotFound, 0 );
		}
		
		
		
		dic = dictionaryFromFUGO_FINDER(entry);
		
		// mutable ###
		
		NSMutableArray* _uniqueArray = [NSMutableArray arrayWithArray: fugoFinderArray_unique];
		[_uniqueArray replaceObjectAtIndex:hoge withObject:dic];
		
		[fugoFinderArray_unique release];
		fugoFinderArray_unique = [[NSArray alloc] initWithArray: _uniqueArray];
		

		
		
//		[fugoFinderArray_unique replaceObjectAtIndex:hoge withObject:dic];
		
	}
	
	
	//(6.5) make index dictionary AGAIN
	[self createDictionaryWithKeyFugoDic];

	



	

	

	
	
	
	
	// finish
	//[self updateTable];
	//[fugoFinderOutlineView reloadData];
	
	[progressIndicator stopAnimation:self];
	[progressBar stopAnimation:self];
	[progressBar setHidden:YES];
	

	
	
	
	return [self description];
		
}

-(NSString*)description
{
	NSMutableString* str = [NSMutableString string];
	unsigned hoge;
	for( hoge = 0; hoge < [fugoFinderArray_unique count]; hoge++ )
	{
		FUGO_FINDER item = FUGO_FINDERFromDictionary( [fugoFinderArray_unique objectAtIndex:hoge] );
		
		NSString* _oneStr = [NSString stringWithFormat:@"%@\t%@\n",item.fugoSuffix, item.fugoString];
		
		[str appendString:_oneStr];
	}
	
	NSLog(@"DESCRIPTION %@",str);
	
	return str;
}


-(BOOL)checkIfThisCanBeFugo:(NSString*)targetStr criteria:(NSArray*)forbiddenMemberNames
{	
 
	if( [targetStr isEqualToString:@"" ] ) return NO;
	
	//NSLog(@"%@",[forbiddenMemberNames description]);
	
	BOOL flag = YES;
	unsigned hoge;
	for( hoge= 0; hoge < [forbiddenMemberNames count]; hoge++ )
	{
		NSString* str = [forbiddenMemberNames objectAtIndex:hoge];
		//NSLog(@"ok? %@",str);
		
		
		if( [targetStr rangeOfRegularExpressionString:str].length != 0 )
		{
			flag = NO;
			break;
		}
	}
 
	NSLog(@"okhere?");
	return flag;
}





/*
- (void)tableView:(NSTableView*)tableView 
  willDisplayCell:(id)cell 
   forTableColumn:(NSTableColumn*)tableColumn 
			  row:(int)row
{
	
	if([[tableColumn identifier] isEqualToString:@"C2"])
	{
		// @"...省略される文。部材１００"	
		FUGO_FINDER entry = FUGO_FINDERFromDictionary( [fugoFinderArray objectAtIndex:row ] );
		
		unsigned location = entry.location;
		if( location > 50 ) location -= 50;
		else location = 0;
		
		unsigned length = entry.location - location;
		NSRange contextRange = NSMakeRange( location, length );
		
		
		// prepare string
		NSDictionary* attributes = [NSDictionary dictionaryWithObjectsAndKeys:
			[NSFont userFontOfSize:12.0],NSFontAttributeName,
			[NSColor lightGrayColor], NSForegroundColorAttributeName,
			NULL];
		
		// get context string
		NSMutableString* str = [[NSMutableString alloc] initWithString:
			[[textStorage string] substringWithRange:contextRange ]];
		[str autorelease];
		[str chomp];
		
		NSAttributedString* as = [[NSAttributedString alloc] initWithString:str attributes:attributes ];
		[as autorelease];
		
		// set position
		NSSize columnSize = [tableView rectOfColumn:1].size;
		
		int x = [tableView frameOfCellAtColumn:1 row:row].origin.x + columnSize.width - [as size].width;
		int y = [tableView frameOfCellAtColumn:1 row:row].origin.y;
		
		//draw context string
		[tableView lockFocus];
		[as drawAtPoint:NSMakePoint(x,y)];
		[tableView unlockFocus];
		
		[cell setLineBreakMode:NSLineBreakByTruncatingHead];
	}
	
}*/


///////////////


-(NSArray*)groupOfFugoFinderDictionary:(NSDictionary*)item
{	
	FUGO_FINDER entry = FUGO_FINDERFromDictionary(item);
	
	id groupIndexArray = [fugoSuffixDictionary objectForKey: entry.fugoID ];
	
	NSMutableArray* __array = [NSMutableArray array];
		
	unsigned hoge;
	for( hoge = 0; hoge < [groupIndexArray count]; hoge++ )
	{
		unsigned getIndex = [[groupIndexArray objectAtIndex:hoge] intValue];
		id getObjectFromIndex = [fugoFinderArray_original objectAtIndex:getIndex];
		
		[__array addObject:getObjectFromIndex ];
		
	}
	
	return [NSArray arrayWithArray:__array];
}





-(void)createDictionaryWithKeyFugoDic
{
	// fugoSuffixDictionaryは、それぞれ fugoID をキーにして、NSArrayがはいてちる。
	// NSArrayは、グループとなるべきfugoFinderArray_originalのインデックスが入っている。
	//　fugoSuffixDictionaryは、表示の際に呼び出される。
	NSLog(@"createDictionaryWithKeyFugoDic");
	
	NSMutableDictionary* tempDic = [[[NSMutableDictionary alloc] init] autorelease];
	
	// create index dictionary with key fugoSuffix
	
	NSMutableArray* temp_fugoFinderArray_original = [NSMutableArray arrayWithArray:fugoFinderArray_original];
	
	
	unsigned piyo;
	for( piyo = 0; piyo < [fugoFinderArray_unique count]; piyo++ )
	{
		NSMutableArray* children = [[[NSMutableArray alloc] init] autorelease];
		
		NSDictionary* uniqueFugoDic = [fugoFinderArray_unique objectAtIndex:piyo ];
		FUGO_FINDER		uniqueFugo = FUGO_FINDERFromDictionary(uniqueFugoDic);
		
	
		unsigned hoge;
		for( hoge = 0; hoge < [temp_fugoFinderArray_original count]; hoge++ )
		{
			NSDictionary* fugoDic = [temp_fugoFinderArray_original objectAtIndex:hoge];
			if( [fugoDic count] != 0 )				
			{
				NSString* fugoDicSuffix = [fugoDic objectForKey:@"fugoSuffix"];
				NSString* fugoDicString = [fugoDic objectForKey:@"fugoString"];

				if( [[uniqueFugo.fugoSuffix lowercaseString] isEqualToString:
					[fugoDicSuffix lowercaseString]] &&
					
					[[fugoDicString lowercaseString] hasSuffix:
					[uniqueFugo.fugoString lowercaseString]] )
				{
					[children addObject:  [NSNumber numberWithInt:hoge] ];
					
					[temp_fugoFinderArray_original replaceObjectAtIndex:hoge withObject:[NSDictionary dictionary]];
				}
			}
		}


		[tempDic setObject:[NSArray arrayWithArray:children]
					forKey: uniqueFugo.fugoID ];
	}
	
	[fugoSuffixDictionary release];
	fugoSuffixDictionary = [[NSDictionary alloc] initWithDictionary:tempDic];

		
}



-(NSDictionary*)itemAtIndex:(int)index inGroupOfFugoFinder:(FUGO_FINDER)indexEntry
{
	
	NSArray* childrenGroup = [self groupOfFugoFinderDictionary:dictionaryFromFUGO_FINDER(indexEntry)];
	
	if( index < [childrenGroup count] )
		return [childrenGroup objectAtIndex:index];
	else 
		return NULL;
	

}

////
//parent object is NSNumber
//children is dictionaries


//////


-(BOOL)thisItemHasUniqueSuffix:(FUGO_FINDER)entry
{
	int counter = 0;

	unsigned hoge;
	for( hoge = 0; hoge < [fugoFinderArray_unique count]; hoge++ )
	{
		//FUGO_FINDER checkRow = FUGO_FINDERFromDictionary( [fugoFinderArray_unique objectAtIndex:hoge] );
		
		NSString* checkFugoSuffix = [[fugoFinderArray_unique objectAtIndex:hoge] objectForKey:@"fugoSuffix"];
		
		if( ! [ checkFugoSuffix isEqualToString:@""] && 
			( [ checkFugoSuffix isEqualToString:entry.fugoSuffix]  ) )
		{
			
			counter++;
			
		}
		
	}
	if( counter > 1 ) return NO;
	
	return YES;
}

- (IBAction)add:(id)sender
{
	

	
	FUGO_FINDER newEntry;
	newEntry.range = NSMakeRange(NSNotFound,0);
	newEntry.fugoPrefix = @"";
	newEntry.fugoString = @"Untitled";
	newEntry.fugoSuffix = @"";
	
	
	newEntry.fugoFlag = @"";
	newEntry.fugoComment = @"";
	newEntry.fugoID = UNIQUEID; 
	
	
	NSMutableArray* _uniqueArray = [NSMutableArray arrayWithArray: fugoFinderArray_unique];
	[_uniqueArray addObject:dictionaryFromFUGO_FINDER(newEntry)];
	
	[fugoFinderArray_unique release];
	fugoFinderArray_unique = [[NSArray alloc] initWithArray: _uniqueArray];
	
	
	
	//[fugoFinderArray_unique addObject:dictionaryFromFUGO_FINDER(newEntry)];
	
	
	[self updateTable];

	[fugoFinderOutlineView reloadData];
	
}

- (IBAction)remove:(id)sender
{
	
	
	if([fugoFinderOutlineView numberOfSelectedRows] == 0 )	return;
	
	
	//finish editing
	if( [fugoFinderOutlineView editedRow] != -1 )	return;
	
	
	NSIndexSet* index = [fugoFinderOutlineView selectedRowIndexes];
	
	unsigned row = [fugoFinderOutlineView numberOfRows] -1;
	NSLog(@"nor %d",row);
	
	while(1)
	{
		
		if( [index containsIndex:row] )
		{
							NSLog(@"deleting %d",row);
			id item = [fugoFinderOutlineView itemAtRow:row];
			int itemIndex = [fugoFinderArray_unique indexOfObjectIdenticalTo: item];
			if( itemIndex != NSNotFound )
			{
				NSMutableArray* _uniqueArray = [NSMutableArray arrayWithArray: fugoFinderArray_unique];
				[_uniqueArray removeObjectAtIndex:itemIndex ];
				
				[fugoFinderArray_unique release];
				fugoFinderArray_unique = [[NSArray alloc] initWithArray: _uniqueArray];

				
				//[fugoFinderArray_unique removeObjectAtIndex:itemIndex];
			}

		}
		
		if( row == 0 ) break;
		
		row--;
		
	}
	
	[fugoFinderOutlineView selectRowIndexes:[NSIndexSet indexSet] byExtendingSelection:NO];
	
	
	[self updateTable];

	[fugoFinderOutlineView reloadData];
}

- (void)addThisString:(NSString*)addThisString
{
	

	
	//もうすこしせいりできる■
	//string wo ninshiki				
	FUGO_FINDER aNewRow = [self convertStringToFUGO_FINDER:addThisString];

	
	
	//check  uniqueness
	unsigned hoge;
	for( hoge = 0; hoge < [fugoFinderArray_unique count]; hoge++ )
	{
		FUGO_FINDER checkRow = FUGO_FINDERFromDictionary( [fugoFinderArray_unique objectAtIndex:hoge] );
		if( ! [checkRow.fugoSuffix isEqualToString:@""] && 
			( [checkRow.fugoSuffix isEqualToString:aNewRow.fugoSuffix] ||
			  [checkRow.fugoString isEqualToString:aNewRow.fugoString] )  )
		{
			
			NSBeep();
			if( [checkRow.fugoSuffix isEqualToString:aNewRow.fugoSuffix] )
				NSBeginAlertSheet(@"Confirmation", @"OK",
								  NULL, NULL, [view window] , NULL
								  //			  NULL,NULL, [fugoTable window], NULL
								  ,  NULL, NULL ,NULL ,[ NSString stringWithCString: 
									  "The numbering is not unique."]);
			break;
			
		}
		
	}
	
	// 
	
	NSMutableArray* _uniqueArray = [NSMutableArray arrayWithArray: fugoFinderArray_unique];
				[_uniqueArray addObject:dictionaryFromFUGO_FINDER(aNewRow) ];
				
				[fugoFinderArray_unique release];
				fugoFinderArray_unique = [[NSArray alloc] initWithArray: _uniqueArray];
				
	
	
	//[fugoFinderArray_unique addObject:dictionaryFromFUGO_FINDER(aNewRow)];
	
	[self updateTable];
	
	[fugoFinderOutlineView reloadData];	
	
	
}


/*
 
 
 昔のコードより。
 
 -(IBAction)copyWrongCombinationSearchString:(id)sender
 //ローラ繊維(?!１２)(?=[0-9|０-９]+[a-z|A-Z|ａ-ｚ|Ａ-Ｚ]*)|ローラ繊維(?=１２[a-z|A-Z|ａ-ｚ|Ａ-Ｚ]+)
 {
	 
	 
	 NSMutableString* value = [[NSMutableString alloc] init];
	 
	 unsigned hoge;
	 for( hoge = 0; hoge < [fugoArray count]; hoge++ )
	 {
		 FUGO checkRow = FUGO_FINDERFromDictionary( [fugoArray objectAtIndex:hoge] );
		 NSMutableString* template = [[[NSMutableString alloc] initWithString:PREF_wrongCombination() ] autorelease];
		 //replaceOccurrencesOfRegularExpressionString:withString:options:range:
		 
		 
		 
		 
		 if( ![checkRow.fugoSuffix isEqualToString:@"" ] )
			 [value appendString:fillTemplate(template, checkRow)];
		 
	 }
	 [value deleteCharactersInRange:NSMakeRange([value length]-1,1)];
	 NSPasteboard* pb = [NSPasteboard generalPasteboard];
	 
	 [pb declareTypes:[NSArray arrayWithObjects:NSStringPboardType, nil] owner:self];
	 
	 
	 
	 [pb setString:value forType:NSStringPboardType];
	 
	 
	 NSLog(@"ok? %@",value);
	 
 }
 

*/


-(NSMutableArray*)fugoFinderArray_unique
{
	return fugoFinderArray_unique;
}

-(void)setFugoFinderArray_unique:(NSArray*)array
{
	[fugoFinderArray_unique release];
	fugoFinderArray_unique = [[NSArray alloc] initWithArray:array];
	
	
	[fugoFinderArray_unique_filtered release];
	fugoFinderArray_unique_filtered = [[NSArray alloc] initWithArray:array];
	
	
	
	[fugoFinderOutlineView reloadData];

}

-(void)updateTable
{
	NSLog(@"call?");

	// notify
	if( updateTarget != NULL && updateAction != NULL )
	{
		[updateTarget performSelector:updateAction];
	}
	
	

////////////////
		[fugoFinderArray_unique_filtered release];
		fugoFinderArray_unique_filtered = [[NSArray alloc] initWithArray:fugoFinderArray_unique];
		
		[fugoFinderOutlineView reloadData];
		
		return;


}

-(int)count
{
	
	return [fugoFinderArray_unique count];
}


-(void)setFugoTableUpdateTarget:(id)target andAction:(id)action
{
	NSLog(@"set method");

	updateTarget = target;
	updateAction = action;
}



- (IBAction)searchFieldInputed:(id)sender;
{
	NSString* str = [sender stringValue];

	if( [str isEqualToString:@""] )
	{
		[fugoFinderArray_unique_filtered release];
		fugoFinderArray_unique_filtered = [[NSArray alloc] initWithArray:fugoFinderArray_unique];
		
		[fugoFinderOutlineView reloadData];

		return;
	}
	
	unsigned hoge;
	
	[fugoFinderOutlineView deselectAll:self];
	
	
	NSMutableArray* _mutableArray = [NSMutableArray array];
	
	for( hoge = 0; hoge < [fugoFinderArray_unique count]; hoge++ )
	{
		FUGO_FINDER entry = FUGO_FINDERFromDictionary([fugoFinderArray_unique objectAtIndex:hoge]);
		
		if( [self filter:entry withSearchField:sender] )
		{

			[_mutableArray addObject:[fugoFinderArray_unique objectAtIndex:hoge] ];
			
		}
		
	}
	
	[fugoFinderArray_unique_filtered release];
	fugoFinderArray_unique_filtered = [[NSArray alloc] initWithArray:_mutableArray];

	
	
	[fugoFinderOutlineView reloadData];
	
	//[[fugoFinderOutlineView window] makeFirstResponder:sender];
}



-(BOOL)filter:(FUGO_FINDER)entry  withSearchField:(id)sender
{
	NSString* str = [[sender stringValue] lowercaseString];
	
	if( [str isEqualToString:@"" ] ) return YES;
	
	if( ( [[entry.fugoString lowercaseString]  rangeOfString:str].length != 0 )
		|| ( [[entry.fugoSuffix lowercaseString] rangeOfString:str].length != 0 )
		||	( [[entry.fugoPrefix lowercaseString]  rangeOfString:str].length != 0 ) )
	{
		return YES;
		
	}

	else return NO;
	
}


@end
