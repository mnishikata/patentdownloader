//
//  FugoAnalyzer.m
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/03/31.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "FugoAnalyzer.h"
#import "FugoFinder.h"
#import "RecognitionDictionaryController.h"
#import "FugoCompletionTextStorage.h"


#define LONGEST_PREFIX_AND_SUFFIX_LENGTH 80

@implementation FugoAnalyzer




+(BOOL)compareByWordWith:(NSDictionary*)item
{
	return FUGO_compareByWord( item );
}


+(NSMutableArray*)fugoRangeArrayInString:(NSString*)buffer  with:(NSDictionary*)item
{


	NSMutableArray* tempArray = [[[NSMutableArray alloc] init] autorelease];
	NSString* aKey = [NSString stringWithString:FUGO_numberRecog( item )];

	NSString* forbiddenPrefix = [NSString stringWithString:FUGO_numberRecogForbiddenPrefix( item )];
	NSString* forbiddenSuffix = [NSString stringWithString:FUGO_numberRecogForbiddenSuffix( item )];

	unsigned options = OgreIgnoreCaseOption;


	
	OGRegularExpression    *regex = [OGRegularExpression regularExpressionWithString:aKey options:options];
	NSEnumerator    *enumerator = [regex matchEnumeratorInString:buffer];	// 
	
	OGRegularExpressionMatch    *match;	// 
	while ((match = [enumerator nextObject]) != nil)
	{	// 
		
		NSString* prefixString;
		NSString* suffixString;
		
		NSRange matchedRange = [match rangeOfMatchedString];//
		NSString* matchedString  = [buffer substringWithRange:matchedRange];
		
		if( matchedRange.location != 0 )
		{			
			NSRange prefixStringRange;

			if(  matchedRange.location < LONGEST_PREFIX_AND_SUFFIX_LENGTH ) 
				prefixStringRange = NSMakeRange(0, matchedRange.location );
				
			else
				prefixStringRange = NSMakeRange( matchedRange.location - LONGEST_PREFIX_AND_SUFFIX_LENGTH, 
											  LONGEST_PREFIX_AND_SUFFIX_LENGTH);
			
				
			prefixString = [buffer substringWithRange:prefixStringRange];
		}
		else 
			prefixString = @"";
		
		if(  NSMaxRange(matchedRange) < [buffer length] )
		{
			
			NSRange trimRange_A = NSMakeRange( 0, [buffer length]);
			NSRange trimRange_B = NSMakeRange(NSMaxRange(matchedRange),
											  LONGEST_PREFIX_AND_SUFFIX_LENGTH);
			
			NSRange suffixStringRange = NSIntersectionRange(trimRange_A,trimRange_B);
			
			suffixString = [buffer substringWithRange:suffixStringRange];
			
			
			
		}
		else 
			suffixString  = @"";

		
		//prefixString = [prefixString omitSpacesAtBothEnds];
		//suffixString = [suffixString omitSpacesAtBothEnds];
		
		
		if( [ prefixString rangeOfRegularExpressionString:forbiddenPrefix options:OgreIgnoreCaseOption].length == 0 &&
			[ suffixString rangeOfRegularExpressionString:forbiddenSuffix options:OgreIgnoreCaseOption].length == 0 )
		{
			[tempArray addObject:NSStringFromRange(matchedRange)];
			
			////NSLog(@"%@",[buffer substringWithRange:matchedRange]);
		}
		
	}
		

	return tempArray;
}

+(NSRange)findMembernameRangeIncludingAltNameFor:(int)index of:(NSMutableArray*)fugoArray
											  in:(NSString*)buffer with:(NSDictionary*)item

//return membername including alt
{
	NSString* delimiter = [NSString stringWithString:FUGO_memberNameExtractor( item )];
	
	NSRange targetFugoRange;
	NSRange previousFugoRange = {0,0};
	
	targetFugoRange = NSRangeFromString( [fugoArray objectAtIndex:index] );
	
	if( index != 0 )
	previousFugoRange = NSRangeFromString( [fugoArray objectAtIndex:index-1] );
	

	
	
	NSRange checkRange = NSMakeRange(NSMaxRange(previousFugoRange), targetFugoRange.location - NSMaxRange(previousFugoRange)   );
	
	NSString* str = [buffer substringWithRange:checkRange];
	NSRange rangeToBeExtracted = [str rangeOfRegularExpressionString:delimiter options:OgreIgnoreCaseOption];
	
	rangeToBeExtracted.location += checkRange.location;
	
	//Check wheather str is @", "
	
	NSString* _str = [buffer substringWithRange:rangeToBeExtracted];
	if( [_str rangeOfRegularExpressionString:  FUGO_numberConnector( item ) ].length != 0 )
	{
		if( index == 0 ) return NSMakeRange(0,0);
		
		NSRange previousNumberRange = NSRangeFromString( [fugoArray objectAtIndex:index-1] );
		//if previousNumberRange is connected to rangeToBeExtracted, return range for previousNumber
		
		if( NSMaxRange(previousNumberRange) == rangeToBeExtracted.location )
			return [self findMembernameRangeIncludingAltNameFor:index-1 of:fugoArray
															 in:buffer  with:(NSDictionary*)item];
		
	}
	
	//
	
	return rangeToBeExtracted;
	
}

+(NSString*)extractMemberNameFromMemberNameIncludingAltName:(NSString*)string  with:(NSDictionary*)item
{
	NSString* str = [NSString stringWithString:FUGO_stringRecog( item )];
	
	NSRange range = 
	[string rangeOfRegularExpressionString:str options:OgreIgnoreCaseOption];
	
	if( range.length == 0 )	return @"";
	
	return [string substringWithRange:range];
	
}

+(NSString*)extractAltNameFromMemberNameIncludingAltName:(NSString*)string with:(NSDictionary*)item
{
	NSString* str = [NSString stringWithString:FUGO_altExtractor( item )];
	
	NSRange range = 
		[string rangeOfRegularExpressionString:str options:OgreIgnoreCaseOption];
	
	if( range.length == 0 )	return @"";
	
	return [string substringWithRange:range];
}

+(void)drawUnderlineIn:(NSTextView*)textView for:(NSArray*)fugoFinderArray_unique
{
	// seach keywords and draw underline
	
	/*
	  0 = none; 0x8000 = by word; styles: 1 = single, 2 = thick, 9 = double; patterns: 0x100 = dotted, 0x200 = dash, 0x300 = dash dot, 0x400 = dash dot dot. Default value 0.
	 
	 */
	
	NSDictionary* TEMP_UNDERLINE = [NSDictionary dictionaryWithObjectsAndKeys:[NSColor blueColor], 
		NSUnderlineColorAttributeName, [NSNumber numberWithInt:  2  ], NSUnderlineStyleAttributeName , nil];
	
	unsigned hoge;
	for( hoge = 0; hoge < [fugoFinderArray_unique count]; hoge++ )
	{
		id piyo = [fugoFinderArray_unique objectAtIndex:hoge];
		FUGO_FINDER item = FUGO_FINDERFromDictionary(piyo);
		
		NSString* strToSearch =  item.fugoString;
		
		
		OGRegularExpression   *regex;
		regex = [OGRegularExpression regularExpressionWithString:strToSearch 
														 options:OgreIgnoreCaseOption];
		NSEnumerator    *enumerator = [regex matchEnumeratorInAttributedString:[textView textStorage]];	// 
		
		OGRegularExpressionMatch    *match;	// 
		
		while ((match = [enumerator nextObject]) != nil)
		{	// 
			NSRange matchedRange = [match rangeOfMatchedString];//
			
			[[textView layoutManager] addTemporaryAttributes:TEMP_UNDERLINE forCharacterRange:matchedRange];
			
			
			[[textView textStorage] addAttribute:NSToolTipAttributeName
										   value:item.fugoPrefix range:matchedRange];
			
			
		}
		
	}
	

}

@end
