//
//  FugoAnalyzer.h
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/03/31.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <OgreKit/OgreKit.h>


@interface FugoAnalyzer : NSObject {


}


+(BOOL)compareByWordWith:(NSDictionary*)item;

+(NSMutableArray*)fugoRangeArrayInString:(NSString*)buffer with:(NSDictionary*)item;


+(NSRange)findMembernameRangeIncludingAltNameFor:(int)index of:(NSMutableArray*)fugoArray in:(NSString*)buffer with:(NSDictionary*)item;


+(NSString*)extractMemberNameFromMemberNameIncludingAltName:(NSString*)string with:(NSDictionary*)item;


+(NSString*)extractAltNameFromMemberNameIncludingAltName:(NSString*)string with:(NSDictionary*)item;

+(void)drawUnderlineIn:(NSTextView*)textView for:(NSArray*)fugoFinderArray_unique;



@end
