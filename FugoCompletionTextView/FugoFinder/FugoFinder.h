/* FugoFinder */

#import <Cocoa/Cocoa.h>
#import <OgreKit/OgreKit.h>

#import "NSString (extension).h"
#import "ThreadWorker.h"
#import "FugoAnalyzer.h"
#import "RBSplitView.h"
#import "NSTextView (coordinate extension).h"


typedef struct
{
	NSRange		range;
	NSString*	fugoPrefix;
	NSString*	fugoString;
	NSString*	fugoSuffix;
	
	
	NSString*	fugoFlag;
	NSString*	fugoComment;
	NSString*	fugoID;

	
}FUGO_FINDER;

 int prefixSort(id num1, id num2, void *context);
 int prefixDecSort(id num1, id num2, void *context);
 int stringSort(id num1, id num2, void *context);
 int stringDecSort(id num1, id num2, void *context);
 int suffixSort(id num1, id num2, void *context) ;
 int suffixDecSort(id num1, id num2, void *context) ;

int rangeSort(id num1, id num2, void *context);
int rangeDecSort(id num1, id num2, void *context) ;
int counterSort(id num1, id num2, void *context);
int counterDecSort(id num1, id num2, void *context) ;
int commentSort(id num1, id num2, void *context);
int commentDecSort(id num1, id num2, void *context);
int flagSort(id num1, id num2, void *context) ;
int flagDecSort(id num1, id num2, void *context);

extern NSDictionary* dictionaryFromFUGO_FINDER( FUGO_FINDER aFugo_row );
extern FUGO_FINDER FUGO_FINDERFromDictionary( NSDictionary* aDic );

@interface FugoFinder : NSObject
{
 
	IBOutlet id fugoFinderOutlineView;

    IBOutlet id fugoFinderTable;
	
	
    IBOutlet id view;
    IBOutlet id progressIndicator;
    IBOutlet id progressStatus;
    IBOutlet id progressBar;


	
	id	updateTarget;
	id	updateAction;
	
	NSDictionary* counterAttributes;
	
	
	NSArray* fugoFinderArray_original; //contains all
	NSArray* fugoFinderArray_unique;	//contains only unique string
	
	NSDictionary* fugoSuffixDictionary; // organised with key:fugoDictionary
	
	NSArray* fugoFinderArray_unique_filtered;	//contains only unique string filtered with searchfield

	
	NSMutableArray* fugoFinderArray_originalToBeUpdated; 
	NSMutableArray* fugoFinderArray_uniqueToBeUpdated;
	
	id	textStorage;
	BOOL			fugoFinderArrayBuilt;
	
	NSImage*	flagImage;
	
	ThreadWorker* tw;
	
	
	int	currentSortedColumn;
	
	
	FugoAnalyzer* fugoAnalyzer;
	
//depricated	������������
	//NSMutableArray* fugoFinderArray;
	
	
	

	
}

- (id)init;

-(void)dealloc;

-(FUGO_FINDER)convertStringToFUGO_FINDER:(NSString*) string;


-(IBAction)selectorClicked:(id)sender;

-(NSArray*)uniqueFugoFinderArray:(NSMutableArray*)originalArray;

-(NSRange)does:(NSString*)targetStr haveBrotherIn:(NSArray*)fugoDicArray;
-(IBAction)buttonClicked:(id)sender;

-(void)setTextStorage:(NSTextStorage*)ts;


-(IBAction)buildTable:(id)sender;

-(IBAction)updateTable:(id)sender;


- (id)makeAllList:(id)buffer;

-(void)makeAllListFinished:(NSArray*)array;

-(id)makeUniqueList:(id)array;

-(NSString*)description;

-(void)makeUniqueListFinished:(NSArray*)array;

-(void)underlineMembernames;

-(BOOL)checkIfThisCanBeFugo:(NSString*)targetStr criteria:(NSArray*)forbiddenMemberNames;

-(NSArray*)groupOfFugoFinderDictionary:(NSDictionary*)item;





-(void)createDictionaryWithKeyFugoDic;



-(NSDictionary*)itemAtIndex:(int)index inGroupOfFugoFinder:(FUGO_FINDER)indexEntry;

-(BOOL)thisItemHasUniqueSuffix:(FUGO_FINDER)entry;

- (IBAction)add:(id)sender;

- (IBAction)remove:(id)sender;

- (void)addThisString:(NSString*)addThisString;


-(NSMutableArray*)fugoFinderArray_unique;

-(void)setFugoFinderArray_unique:(NSArray*)array;


-(void)updateTable;

-(int)count;

-(void)setFugoTableUpdateTarget:(id)target andAction:(id)action;


- (IBAction)searchFieldInputed:(id)sender;


-(BOOL)filter:(FUGO_FINDER)entry  withSearchField:(id)sender;


@end

@interface FugoFinder (UI)

#import "FugoFinder.h"

#define TableRowType @"fugo"
#define TableRowTypes [NSArray arrayWithObjects:@"fugo",NSStringPboardType, nil]

#define COUNTER_RADIUS 7.0

#define UNIQUEID [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970]];

//#define LONGEST_FUGO_TEMPLATE	[ NSString stringWithCString:"[^0-9|�O-�X|,�A�@�|�`�捀�y�z(�i�}\n]+[0-9|�O-�X]+[a-z|A-Z|��-��|�`-�y]*"]





-(NSView*)view;




- (int)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item ;
- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item ;

- (id)outlineView:(NSOutlineView *)outlineView child:(int)index ofItem:(id)item ;

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item ;



- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item;

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldSelectItem:(id)item;

- (BOOL)outlineView:(NSOutlineView *)outView writeItems:(NSArray*)items toPasteboard:(NSPasteboard*)pboard;

- (unsigned int)outlineView:(NSOutlineView*)outView validateDrop:(id <NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(int)index;

- (BOOL)outlineView:(NSOutlineView*)outView acceptDrop:(id <NSDraggingInfo>)info item:(id)item childIndex:(int)index;


- (void)outlineView:(NSOutlineView *)outlineView willDisplayCell:(id)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item;

- (void)outlineView:(NSOutlineView *)outlineView setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item;

- (void)splitView:(RBSplitView*)sender changedFrameOfSubview:(RBSplitSubview*)subview  from:(NSRect)fromRect  to:(NSRect)toRect;


- (unsigned int)splitView:(RBSplitView*)sender dividerForPoint:(NSPoint)point inSubview:(RBSplitSubview*)subview;

- (NSRect)splitView:(RBSplitView*)sender cursorRect:(NSRect)rect forDivider:(unsigned int)divider;




- (void)outlineView:(NSOutlineView *)outlineView didClickTableColumn:(NSTableColumn *)tableColumn ;




@end
