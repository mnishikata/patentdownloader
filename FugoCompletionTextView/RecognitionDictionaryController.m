//
//  RecognitionDictionaryController.m
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/06/02.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "RecognitionDictionaryController.h"

#define APPLICATION_SUPPORT_DIRECTORY 	[NSHomeDirectory() stringByAppendingPathComponent: @"Library/Application Support/Kojo/"]



#define BEGINLINE @"---------- %@ begin ----------\n"
#define ENDLINE @"\n---------- %@ end ----------"

static 	NSMutableArray* recognitionDictionaryArray;

NSString* FUGO_language( NSDictionary* item )
{
	
	return emptyOrValue( [ item objectForKey:@"language"] );
}


NSString* FUGO_paragraphDelimiter( NSDictionary* item )
{
	
	return emptyOrValue( [ item objectForKey:@"paragraphDelimiter"] );
}


NSString* FUGO_numberRecog( NSDictionary* item )
{
	
	return emptyOrValue( [ item objectForKey:@"numberRecog"] );
}




NSString* FUGO_numberRecogForbiddenPrefix(  NSDictionary* item  )
{
	
	return emptyOrValue( [ item objectForKey:@"numberRecogForbiddenPrefix"] );
}



NSString* FUGO_numberRecogForbiddenSuffix( NSDictionary* item )
{
	
	return emptyOrValue( [ item objectForKey:@"numberRecogForbiddenSuffix"] );
}


NSString* FUGO_numberConnector( NSDictionary* item )
{
	
	return emptyOrValue( [ item objectForKey:@"numberConnector"] );
}


NSString* FUGO_memberNameExtractor( NSDictionary* item )
{
	
	return emptyOrValue( [ item objectForKey:@"memberNameExtractor"] );
}




NSString* FUGO_altExtractor( NSDictionary* item )
{
	////NSLog(@"HERE2");
	
	return emptyOrValue( [ item objectForKey:@"altExtractor"] );
}




NSString* FUGO_stringRecog( NSDictionary* item )
{
	////NSLog(@"HERE6 %@",[ selectedRecognitionDictionary().dictionary objectForKey:@"stringRecog"]);
	
	return emptyOrValue( [ item objectForKey:@"stringRecog"] );
}


BOOL FUGO_compareByWord( NSDictionary* item )
{
	NSString* str =  emptyOrValue( [ item objectForKey:@"compareByWord"] );
	
//	//NSLog(@"COMPARE BY WORD %@",str);
	
	if( [str isEqualToString:@"YES"] ) return YES;
	return NO;
}

////






@implementation RecognitionDictionaryController

+(NSDictionary*)defaultRecognitionDictionaryFor:( NSString*) contents 
// (1) select assigned dictionary, or
// (2) find appropriate dictionary, or
{
	
	NSDictionary* recog;
	
	NSMutableArray* recognitionDictionaryArray = [self recognitionDictionaries];
	
	NSUserDefaults* ud = [NSUserDefaults standardUserDefaults];
	
	//(1)
	NSString* selectedDictionary = [ud stringForKey:@"selectedDictionary"];
	
	recog  = [recognitionDictionaryArray objectAtIndex:0]; // default
	
	
	
	if( selectedDictionary != NULL )
	{
		
		
		unsigned hoge;
		
		
		for( hoge = 0 ; hoge < [recognitionDictionaryArray count]; hoge++)
		{
			NSDictionary* oneDic = [recognitionDictionaryArray objectAtIndex:hoge];
			if( [[oneDic objectForKey:@"title"] isEqualToString:selectedDictionary ] )
			{
				recog = oneDic;
				break;
			}
		}
		
		
	}
	
	
	//(2)
	if( ![contents isEqualToString:@"" ] ) 
	{
		unsigned hoge;
		for( hoge = 0 ; hoge < [recognitionDictionaryArray count]; hoge++)
		{
			NSDictionary* oneDic = [recognitionDictionaryArray objectAtIndex:hoge];
			NSString* identifier = emptyOrValue( [oneDic objectForKey:@"identifier"] );
			//NSLog(@"idenftifier %@", identifier);
			if(  ![identifier isEqualToString:@"" ] )
			{
				if(  [contents rangeOfRegularExpressionString:identifier].length != 0   )
				{
					recog = oneDic;
					break;
				}
			}
		}
	}
	
	
	
	//NSLog(@"default dic %@",[recog  objectForKey:@"title"] );
	
	if( recog  == NULL )
		recog  = [NSDictionary dictionary];
	return recog;
}



+(NSMutableArray*)recognitionDictionaries
{
	
	
	NSMutableArray* returnArray = [[[NSMutableArray alloc] init] autorelease];
	
	//set default first
	
	// search resource bundle
	NSArray* paths =  [[NSBundle bundleForClass:[self class]]
						pathsForResourcesOfType:@"kojodic" inDirectory:@""];
	//NSLog(@"path description %@",[paths description]);
	
	unsigned hoge;
	for( hoge = 0 ; hoge < [paths count]; hoge++ )
	{
		NSDictionary* recog = [self loadRecognitionSettingFromFile:
			[paths objectAtIndex:hoge]];
		
		
		[returnArray addObject:recog ];
	}
	
	
	//seach aplication support folder
	NSArray* contents = [[NSFileManager defaultManager] subpathsAtPath:APPLICATION_SUPPORT_DIRECTORY];
	
	for( hoge = 0 ; hoge < [contents count]; hoge++ )
	{
		NSString* item = [contents objectAtIndex:hoge];
		
		if( [item hasSuffix:@".kojodic"] )
		{
			NSDictionary* recog = [self loadRecognitionSettingFromFile:[APPLICATION_SUPPORT_DIRECTORY stringByAppendingPathComponent: item]];
			
			[returnArray addObject:recog ];
		}
	}
	

	
	return returnArray;
}


#pragma mark -
#pragma mark Private

+(NSDictionary*)loadRecognitionSettingFromFile:(NSString*)path
{
	NSAttributedString* atts =
	[ [[NSAttributedString alloc] initWithPath:path documentAttributes:NULL] autorelease];
	
	NSString* moto = [atts string];
	
	NSString* toc = [self extract:@"TOC" from:moto];
	NSArray* tocArray = [toc componentsSeparatedByString:@"\n"];
	
	
	// start
	NSMutableDictionary* contents = [[[NSMutableDictionary alloc] init] autorelease];
	unsigned hoge;
	for( hoge = 0; hoge < [tocArray count]; hoge++)
	{
		NSString* key = [tocArray objectAtIndex:hoge];
		[contents setObject:[self extract:key from:moto] forKey:key];
	}
	
	// now contentArray has proper strings with key--title.
	
	NSDictionary* recog;
	
	if( [contents objectForKey:@"title"] != NULL )
		recog  = contents;
	else 
		recog  = NULL;

	
	return recog;
}

+(NSString*)extract:(NSString*)identifier from:(NSString*)oya
{
	NSString* beginLine = [NSString stringWithFormat:BEGINLINE,identifier];
	NSString* endLine = [NSString stringWithFormat:ENDLINE,identifier];
	
	NSArray* cutOya = [oya componentsSeparatedByString:beginLine];
	
	if( [cutOya count] != 2 ) return @"";
	
	return [[[cutOya objectAtIndex:1]  componentsSeparatedByString:endLine] objectAtIndex:0];
	
}






@end
