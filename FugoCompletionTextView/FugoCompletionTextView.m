//
//  FugoCompletionTextView.m
//  KojoWriter
//
//  Created by Masatoshi Nishikata on 13/02/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "FugoCompletionTextView.h"

#define LONGEST_FUGO_COMPLETION_LENGTH 100

NSMutableString* fillTemplate(NSString* template, FUGO_FINDER entry)
{
	// return NULL if %1 exists and trying to fill %1 string, and so on.
	
	if( [template rangeOfString:@"%1"].length != 0 && [entry.fugoString isEqualToString:@""] )
		return NULL;
	
	if( [template rangeOfString:@"%2"].length != 0 && [entry.fugoSuffix isEqualToString:@""] )
		return NULL;

	if( [template rangeOfString:@"%3"].length != 0 && [entry.fugoPrefix isEqualToString:@""] )
		return NULL;
	
	
	
	[template replaceOccurrencesOfRegularExpressionString:@"%1"
											   withString:entry.fugoString
												  options:OgreNoneOption range:NSMakeRange(0,[template length]) ];
	
	[template replaceOccurrencesOfRegularExpressionString:@"%2"
											   withString:entry.fugoSuffix
												  options:OgreNoneOption range:NSMakeRange(0,[template length]) ];
	
	[template replaceOccurrencesOfRegularExpressionString:@"%3"
											   withString:entry.fugoPrefix
												  options:OgreNoneOption range:NSMakeRange(0,[template length]) ];

	[template replaceOccurrencesOfRegularExpressionString:@"%4"
											   withString:[entry.fugoSuffix fullwidthString]
												  options:OgreNoneOption range:NSMakeRange(0,[template length]) ];

	[template replaceOccurrencesOfRegularExpressionString:@"%5"
											   withString:[entry.fugoSuffix halfwidthString]
												  options:OgreNoneOption range:NSMakeRange(0,[template length]) ];
	
	
	return template;
}



@implementation FugoCompletionTextView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.

		[self awakeFromNib];
		
    }
    return self;
}



-(void)dealloc
{
	[super dealloc];
	
}


- (void)awakeFromNib
{
	FugoCompletionTextStorage* ts = [[[FugoCompletionTextStorage alloc] init] autorelease];
	id delegate = [[self textStorage] delegate];
	[[self layoutManager] replaceTextStorage:ts];

	if (delegate != NULL ) [[self textStorage] setDelegate:delegate];
	
	if ([[self superclass] instancesRespondToSelector:@selector(awakeFromNib)]) {
		[super awakeFromNib];
	}
}


-(NSView*)fugoView
{
		//NSLog(@"X2");
	return [[self textStorage] fugoView];
	//NSLog(@"X2 ok");
}


-(IBAction)copyFugo:(id)sender
{
	NSString* str = [[[self textStorage] attributedSubstringFromRange:[self selectedRange]] string];
	[[self textStorage] addFugo:str];
}





@end


