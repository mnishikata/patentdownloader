//
//  StyleToolbarControl.m
//  SampleApp
//
//  Created by Masatoshi Nishikata on 06/04/07.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//

#import "CustomToolbarControl.h"


@implementation CustomToolbarControl

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
		
		//NSLog(@"INIT");
		
		immediateAction = NULL;
		immediateTarget = NULL;
		
		/*
		progre = [[NSProgressIndicator alloc] initWithFrame:NSMakeRect(0,0,16,16)];
		
		
		[progre setStyle:NSProgressIndicatorSpinningStyle];
		[progre setControlSize:NSSmallControlSize];
		[progre setMinValue:0];
		[progre setMaxValue:100];
		[progre setUsesThreadedAnimation:YES];
	[progre setDisplayedWhenStopped:NO];
		[progre startAnimation:self];
		
		[self addSubview:progre];
		 */

    }
    return self;
}



- (void)mouseDown:(NSEvent *)theEvent
{
	/*
	int keyCode = [theEvent modifierFlags];
	if( keyCode == 524576 || keyCode == 721186)
	{
	
		[[[self cell] target] buttonClicked:NULL];
	}
	else
	[super mouseDown:theEvent];
	 */
	
	//NSLog(@"clicked");
	
	if( immediateTarget != NULL && immediateAction != NULL)
	{
		[[self cell] setHighlighted:YES];
		[self display];
		[immediateTarget performSelector:immediateAction];
		[[self cell] setHighlighted:NO];
		[self display];
	}else
	[super mouseDown:theEvent];
}
-(void)setImmediateAction:(id)action
{
	immediateAction = action;
}
-(void)setImmediateTarget:(id)target
{
	immediateTarget = target;
}

-(void)dealloc
{

	[super dealloc];
}
@end
